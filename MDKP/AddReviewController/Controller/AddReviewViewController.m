//
//  AddReviewViewController.m
//  MDKP
//
//  Created by Андрей on 26.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "AddReviewViewController.h"
#import "AddReviewView.h"
#import "AddReviewModel.h"

@interface AddReviewViewController () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) AddReviewView *addReviewView;
@property (nonatomic, strong) AddReviewModel *model;
@property (nonatomic, strong) NSMutableDictionary *review;

@end

@implementation AddReviewViewController

- (void)loadView {
    self.addReviewView = [[AddReviewView alloc] init];
    self.view = self.addReviewView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[AddReviewModel alloc] init];
    self.review = [NSMutableDictionary dictionary];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCheck:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    
    [self.addReviewView.applyButton addTarget:self action:@selector(addReview:)forControlEvents:UIControlEventTouchUpInside];
}

- (void)addReview:(UIButton *)sender {
    [self.addReviewView.indicatorView startAnimating];
    [self.review setValue:self.addReviewView.textReview.text forKey:@"text"];
    [self.review setValue:[@(self.adId) stringValue] forKey:@"adId"];
    [self.review setValue:[self.model getAuthorId] forKey:@"authorId"];
    __weak typeof(self) weakSelf = self;
    [self.model addReview:self.review withCompletion:^{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Отзывы успешно добавлен!"
                                     message:@"Теперь он появится в списке отзывов!"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [weakSelf dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [alert addAction:okButton];
        [weakSelf presentViewController:alert animated:YES completion:nil];
    }];
    [self.addReviewView.indicatorView stopAnimating];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (void)tapCheck:(UITapGestureRecognizer *)sender {
    if (!CGRectContainsPoint(self.addReviewView.modalView.frame, [sender locationInView:self.view])){
        [self.view removeGestureRecognizer:sender];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}


@end
