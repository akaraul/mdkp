//
//  AddReviewModel.h
//  MDKP
//
//  Created by Андрей on 26.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddReviewModel : NSObject

- (void)addReview:(NSDictionary *)review withCompletion:(void (^)())completion;
- (NSString *)getAuthorId;

@end
