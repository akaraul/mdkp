//
//  AddReviewModel.m
//  MDKP
//
//  Created by Андрей on 26.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "AddReviewModel.h"
#import "APIService.h"
#import "AppDelegate.h"

@interface AddReviewModel ()

@property (nonatomic, strong) NSPersistentContainer *persistentContainer;

@end

@implementation AddReviewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}

- (void)addReview:(NSDictionary *)review withCompletion:(void (^)())completion{
    [[APIService sharedService] addAdvertReview:review withComplition:^(BOOL success, NSError *error) {
        if (success) {
            if (completion) completion();
        }
    }];
}

- (NSString *)getAuthorId {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"USERINFO"];
    NSArray *res = [context executeFetchRequest:request error:nil];
    return [[res firstObject] valueForKey:@"userId"];
}

@end
