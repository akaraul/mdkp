//
//  AddReviewView.m
//  MDKP
//
//  Created by Андрей on 26.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "AddReviewView.h"

@implementation AddReviewView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    self.backgroundColor = [UIColor clearColor];
    self.opaque = NO;

    _applyButton = [[UIButton alloc] init];
    _applyButton.backgroundColor = colorGreen;
    _applyButton.layer.cornerRadius = 10.0f;
    [_applyButton setTitle:@"Отправить" forState:UIControlStateNormal];
    [_applyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    _modalView = [[UIView alloc] init];
    _modalView.backgroundColor = [UIColor whiteColor];
    _modalView.layer.borderWidth = 1.0f;
    //_modalView.layer.cornerRadius = 20.0f;
    _modalView.layer.masksToBounds = NO;
    _modalView.layer.shadowRadius = 30;
    _modalView.layer.shadowOpacity = 0.9;
    [_modalView addSubview:_applyButton];
    
    _textReview = [[UITextView alloc] init];
    [_textReview setFont:[UIFont systemFontOfSize:16]];
    _textReview.backgroundColor = [UIColor colorWithWhite:0.96 alpha:1];
    [_modalView addSubview:_textReview];
    
    _header = [[UILabel alloc] init];
    _header.text = @"Оставьте свой отзыв";
    _header.backgroundColor = colorGreen;
    _header.textAlignment = NSTextAlignmentCenter;
    [_modalView addSubview:_header];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _indicatorView.hidesWhenStopped = YES;
    _indicatorView.color = [UIColor blueColor];
    [_modalView addSubview:_indicatorView];
    
    [self addSubview:_modalView];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat offset = 10;
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGSize sizeModal = CGSizeMake(width*0.9f, 310);
    CGSize sizeButton =  CGSizeMake(width*0.5, 40.0f);

    
    self.modalView.frame = CGRectMake(width/2-sizeModal.width/2,
                                      height/2-sizeModal.height/2,
                                      sizeModal.width,
                                      sizeModal.height);
    
    self.indicatorView.center = self.modalView.center;
    
    self.textReview.frame = CGRectMake(offset, 60, sizeModal.width- offset*2, 120);
    self.header.frame = CGRectMake(0, 0, sizeModal.width, 25);
    
    
    self.applyButton.frame = CGRectMake(sizeModal.width/2 - sizeButton.width/2 ,
                                        sizeModal.height - 20.0f - sizeButton.height,
                                        sizeButton.width,
                                        sizeButton.height);
    
}

@end
