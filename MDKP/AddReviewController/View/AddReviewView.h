//
//  AddReviewView.h
//  MDKP
//
//  Created by Андрей on 26.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddReviewView : UIView

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property (nonatomic, strong) UIView *modalView;
@property (nonatomic, strong) UITextView *textReview;
@property (nonatomic, strong) UILabel *header;

@property (nonatomic, strong) UIButton *applyButton;

@end
