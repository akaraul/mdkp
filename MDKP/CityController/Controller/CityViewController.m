//
//  CityViewController.m
//  MDKP
//
//  Created by Андрей on 28.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "CityViewController.h"
#import "CityView.h"
#import <CoreLocation/CoreLocation.h>
#import "CoreData/CoreData.h"
#import "AppDelegate.h"
#import "CITIES+CoreDataClass.h"

@interface CityViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) CityView *cityView;

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSArray *searchResult;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSPersistentContainer *persistentContainer;

@end

@implementation CityViewController

- (void)loadView {
    self.cityView = [[CityView alloc] init];
    _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    self.view = self.cityView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"CITIES"];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [request setSortDescriptors:@[sort]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc]
                                     initWithFetchRequest:request
                                     managedObjectContext:context
                                     sectionNameKeyPath:nil
                                     cacheName:nil];
    [self.fetchedResultsController performFetch:nil];
    self.searchResult = self.fetchedResultsController.fetchedObjects;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    
    self.cityView.result.delegate = self;
    self.cityView.result.dataSource = self;
    self.cityView.searchBar.delegate = self;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"closeIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(closeView:)];
}

- (void)closeView:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

#pragma SearchBar delegate

- (void)searchForText:(NSString*)searchText {
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[cd] %@",searchText];
    self.searchResult = [self.fetchedResultsController.fetchedObjects filteredArrayUsingPredicate:resultPredicate];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.view endEditing:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self searchForText:searchText];
    [self.cityView.result reloadData];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    if ([searchBar.text isEqualToString:@""]) self.searchResult=nil;
    else  [self searchForText:searchBar.text];
    //[searchBar setShowsCancelButton:YES animated:YES];
    [self.cityView.result reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.searchResult = self.fetchedResultsController.fetchedObjects;
    self.cityView.searchBar.text = @"";
    [self.view endEditing:YES];
   // [searchBar setShowsCancelButton:NO animated:YES];
    [self.cityView.result reloadData];
}

#pragma UITableviewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.searchResult count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCity"];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    if ([indexPath row] == 0) {
        cell.textLabel.text = @"Мое местоположение";
        return cell;
    }
    CITIES *cellObject = [self.searchResult objectAtIndex:indexPath.row-1];
    cell.textLabel.text = cellObject.name;
    cell.detailTextLabel.hidden = YES;
    cell.detailTextLabel.text = cellObject.code;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath row] == 0) {
        [self getLocation];
    }
    else {
        [self.delegate cityDidSelected:[tableView cellForRowAtIndexPath:indexPath].textLabel.text];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma getLocation

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    [self.locationManager stopUpdatingLocation];
}

- (void)getLocation {
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [self.cityView.indicatorView startAnimating];
    
    [self.locationManager startUpdatingLocation];
    
    CLGeocoder *geo = [[CLGeocoder alloc] init];
    
    [geo reverseGeocodeLocation:self.locationManager.location completionHandler:^(NSArray* placemarks, NSError* error){
        if ([placemarks count] > 0)
        {
            CLPlacemark *city= [placemarks objectAtIndex:0];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:nil
                                             message:[NSString stringWithFormat:@"Ваш город %@?",city.locality]
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"Да"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                [self.delegate cityDidSelected:city.locality];
                                                [self dismissViewControllerAnimated:YES completion:nil];
                                            }];
                
                [alert addAction:yesButton];
                
                UIAlertAction* noButton = [UIAlertAction
                                           actionWithTitle:@"Нет"
                                           style:UIAlertActionStyleDefault
                                           handler:nil];
                
                [alert addAction:noButton];
                
                [self presentViewController:alert animated:YES completion:nil];
                
            });
        }
        [self.cityView.indicatorView stopAnimating];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }];
}

@end
