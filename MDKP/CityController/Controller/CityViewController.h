//
//  CityViewController.h
//  MDKP
//
//  Created by Андрей on 28.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  CityViewControllerDelegate;

@interface CityViewController : UIViewController
@property (nonatomic, weak) id<CityViewControllerDelegate> delegate;

@end

@protocol CityViewControllerDelegate <NSObject>

- (void)cityDidSelected:(NSString *)city;

@end
