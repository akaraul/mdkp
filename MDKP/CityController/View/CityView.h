//
//  CityView.h
//  MDKP
//
//  Created by Андрей on 28.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityView : UIView

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UITableView *result;
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end
