//
//  CityView.m
//  MDKP
//
//  Created by Андрей on 28.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "CityView.h"

@implementation CityView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    
    _searchBar = [[UISearchBar alloc] init];
    _searchBar.placeholder = @"Название города";
    [_searchBar setValue:@"Отмена" forKey:@"_cancelButtonText"];
    _searchBar.searchBarStyle = UISearchBarStyleMinimal;
    UITextField *txfSearchField = [_searchBar valueForKey:@"_searchField"];
    txfSearchField.borderStyle = UITextBorderStyleNone;
    txfSearchField.backgroundColor = [UIColor whiteColor];
    txfSearchField.leftView = nil;
    [self addSubview:_searchBar];
    
    _result = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _result.separatorStyle = UITableViewCellSeparatorStyleNone;
    _result.showsVerticalScrollIndicator = NO;
    [self addSubview:_result];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _indicatorView.hidesWhenStopped = YES;
    _indicatorView.color = [UIColor grayColor];
    [self addSubview:_indicatorView];
    
    [self addBotLayerToTheView:self.searchBar];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    
    self.searchBar.frame = CGRectMake(offset, height*0.2, width-offset*2, 50.0f);
    
    self.result.frame = CGRectMake(offset,
                                   self.searchBar.frame.origin.y + self.searchBar.frame.size.height + offset*4,
                                   self.searchBar.frame.size.width,
                                   height - self.result.frame.origin.y);
    
    self.indicatorView.center = self.center;
    
}

- (void)addBotLayerToTheView:(UIView *)view {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CALayer *botBorder = [CALayer layer];
    botBorder.frame = CGRectMake(0.0, 49.0, screenRect.size.width-20, 1.0);
    botBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                  alpha:1.0f].CGColor;
    [view.layer addSublayer:botBorder];
}


@end
