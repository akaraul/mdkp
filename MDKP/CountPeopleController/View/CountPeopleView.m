//
//  CountPeopleView.m
//  MDKP
//
//  Created by Андрей on 28.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "CountPeopleView.h"

@implementation CountPeopleView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    UIImage *incrementImageFromFile = [UIImage imageNamed:@"plusIcon"];
    UIImage *incrementImage = [incrementImageFromFile imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *decrementImageFromFile = [UIImage imageNamed:@"minusIcon"];
    UIImage *decrementImage = [decrementImageFromFile imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    self.backgroundColor = [UIColor whiteColor];
    
    _countPeopleTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _countPeopleTable.scrollEnabled = NO;
    _countPeopleTable.allowsSelection = NO;
    _countPeopleTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_countPeopleTable];
    
    _save = [[UIButton alloc] init];
    [_save setTitle:@"Сохранить" forState:UIControlStateNormal];
    _save.backgroundColor = colorGreen;
    _save.layer.cornerRadius = 10.0f;
    
    _animals = [[UISwitch alloc] init];
    _animals.transform = CGAffineTransformMakeScale(1.1, 1.1);
    _animals.selected = NO;
    
    _stepAdult = [[SAStepperControl alloc] init];
    _stepAdult.value = 1;
    _stepAdult.tintColor = [UIColor clearColor];
    _stepAdult.transform = CGAffineTransformMakeScale(1.3, 1.3);
    [_stepAdult setDecrementImage:decrementImage forState:UIControlStateNormal];
    [_stepAdult setIncrementImage:incrementImage forState:UIControlStateNormal];;
    
    _stepBaby = [[SAStepperControl alloc] init];
    _stepBaby.value = 0;
    _stepBaby.tintColor = [UIColor clearColor];
    _stepBaby.transform = CGAffineTransformMakeScale(1.3, 1.3);
    [_stepBaby setDecrementImage:decrementImage forState:UIControlStateNormal];
    [_stepBaby setIncrementImage:incrementImage forState:UIControlStateNormal];;
    
    _stepChildren = [[SAStepperControl alloc] init];
    _stepChildren.value = 0;
    _stepChildren.tintColor = [UIColor clearColor];
    _stepChildren.transform = CGAffineTransformMakeScale(1.3, 1.3);
    [_stepChildren setDecrementImage:decrementImage forState:UIControlStateNormal];
    [_stepChildren setIncrementImage:incrementImage forState:UIControlStateNormal];;
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGFloat widthButton = width*0.8;
    
    self.countPeopleTable.frame = CGRectMake(0,
                                             0,
                                             width,
                                             height - self.countPeopleTable.frame.origin.y - 90);
    
    self.save.frame = CGRectMake(width/2 - widthButton/2,
                                 15.0f,
                                 widthButton,
                                 50.0f);
    
    self.animals.frame = CGRectMake(width - 105.0f, 35.0f, 50.0f, 50.0f);
    self.stepChildren.frame = CGRectMake(width - 140, 35, 80, 80);
    self.stepBaby.frame = self.stepChildren.frame;
    self.stepAdult.frame = self.stepChildren.frame;
    
    
}


@end
