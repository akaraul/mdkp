//
//  CountPeopleView.h
//  MDKP
//
//  Created by Андрей on 28.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SAStepperControl.h>

@interface CountPeopleView : UIView

@property (nonatomic, strong) UIButton *save;

@property (nonatomic, strong) UITableView *countPeopleTable;

@property (nonatomic, strong) UISwitch *animals;

@property (nonatomic, strong) SAStepperControl *stepAdult;
@property (nonatomic, strong) SAStepperControl *stepChildren;
@property (nonatomic, strong) SAStepperControl *stepBaby;

@end
