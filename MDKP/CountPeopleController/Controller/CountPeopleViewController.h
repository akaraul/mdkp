//
//  CountPeopleViewController.h
//  MDKP
//
//  Created by Андрей on 28.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  CountPeopleViewControllerDelegate;

@interface CountPeopleViewController : UIViewController
@property (nonatomic, weak) id<CountPeopleViewControllerDelegate> delegate;

@end

@protocol CountPeopleViewControllerDelegate <NSObject>

- (void)countPeopleDidSelected:(NSInteger)count;

@end
