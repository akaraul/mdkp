//
//  CountPeopleViewController.m
//  MDKP
//
//  Created by Андрей on 28.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "CountPeopleViewController.h"
#import "CountPeopleView.h"

@interface CountPeopleViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) CountPeopleView *peopleView;

@end

@implementation CountPeopleViewController

- (void)loadView {
    self.peopleView = [[CountPeopleView alloc] init];
    self.view = self.peopleView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    
    self.peopleView.countPeopleTable.delegate = self;
    self.peopleView.countPeopleTable.dataSource = self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"closeIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(closeView:)];
    
    [self.peopleView.save addTarget:self action:@selector(saveParametrs:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.barStyle = UIBarStyleDefault;
    CGFloat toolbarHeight = 80;
    CGRect rootViewBounds = self.parentViewController.view.bounds;
    CGFloat rootViewHeight = CGRectGetHeight(rootViewBounds);
    CGFloat rootViewWidth = CGRectGetWidth(rootViewBounds);
    CGRect rectArea = CGRectMake(0, rootViewHeight - toolbarHeight, rootViewWidth, toolbarHeight);
    [toolbar setFrame:rectArea];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.peopleView.save];
    
    [toolbar setItems:[NSArray arrayWithObjects:barButtonItem,nil]];
    [self.navigationController.view addSubview:toolbar];
}

- (void)saveParametrs:(UIButton *)sender {
    NSInteger count = self.peopleView.stepBaby.value + self.peopleView.stepAdult.value + self.peopleView.stepChildren.value;
    [self.delegate countPeopleDidSelected:count];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)closeView:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma UITableviewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    cell.textLabel.font=[UIFont systemFontOfSize:24.0];
    cell.detailTextLabel.font=[UIFont systemFontOfSize:16.0];
        switch ([indexPath row]) {
            case 0:
                cell.textLabel.text = @"Взрослые";
                [cell.contentView addSubview:self.peopleView.stepAdult];
                break;
            case 1:
                cell.textLabel.text = @"Дети";
                cell.detailTextLabel.text = @"От 2 до 12 лет";
                [cell.contentView addSubview:self.peopleView.stepChildren];
                break;
            case 2:
                cell.textLabel.text = @"Младенцы";
                cell.detailTextLabel.text = @"Младше 2 лет";
                [cell.contentView addSubview:self.peopleView.stepBaby];
                break;
            case 3:
                cell.textLabel.text = @"Питомцы";
                [cell.contentView addSubview:self.peopleView.animals];
                break;
        }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0f;
}


@end
