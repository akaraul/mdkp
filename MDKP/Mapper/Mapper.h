//
//  Mapper.h
//  MDKP
//
//  Created by Андрей on 11.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreData/CoreData.h"

@interface Mapper : NSObject

+ (void)loadUserInfoInDatabase:(NSData *)userData;
+ (void)loadAdInfoInDatabase:(NSInteger)adId and:(NSDictionary *)adInfo;
+ (void)loadComfortsInContext:(NSManagedObjectContext *)context;
+ (void)mapCityInContext:(NSManagedObjectContext *)context;

@end
