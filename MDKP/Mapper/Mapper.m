//
//  Mapper.m
//  MDKP
//
//  Created by Андрей on 11.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "Mapper.h"
#import "AppDelegate.h"
#import "APIService.h"
#import "ADS+CoreDataClass.h"

@implementation Mapper

+ (void)loadUserInfoInDatabase:(NSData *)userData {
    NSDictionary *mapUserInfo = @{
                          @"response.userInfo.userId" : @"userId",
                          @"response.userInfo.name" :   @"name",
                          @"response.userInfo.surname" : @"surname",
                          @"response.userInfo.phone" : @"phone",
                          @"response.userInfo.city" : @"city",
                          @"response.userInfo.about" : @"about",
                          @"response.userInfo.education" : @"education",
                          @"response.userInfo.avatar_path" : @"avatar_path",
                          @"response.userInfo.login" : @"login",
                          @"response.userInfo.password" : @"password",
                          @"response.userInfo.gender" : @"gender",
                          @"response.userInfo.birthday" : @"birthday",
                          };
    
    NSPersistentContainer *persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    NSManagedObjectContext *context = persistentContainer.viewContext;
        NSDictionary *userInfo =[NSJSONSerialization JSONObjectWithData:userData options:0 error:nil];
        NSManagedObject *entity = [NSEntityDescription insertNewObjectForEntityForName:@"USERINFO"
                                                                inManagedObjectContext:context];
        for (id key in mapUserInfo) {
            id value = ([userInfo valueForKeyPath:key] == [NSNull null] ? nil : [userInfo valueForKeyPath:key]);
            [entity setValue:value forKey:[mapUserInfo objectForKey:key]];
        }
        for (id ad in [userInfo valueForKeyPath:@"response.ads"]) {
            NSManagedObject *adsEntity = [NSEntityDescription insertNewObjectForEntityForName:@"ADS"
                                                                    inManagedObjectContext:context];
            [adsEntity setValue:[ad valueForKey:@"adsId"] forKey:@"adsId"];
            [adsEntity setValue:[ad valueForKey:@"name"] forKey:@"name"];
            [adsEntity setValue:[ad valueForKey:@"price"] forKey:@"price"];
            [adsEntity setValue:[ad valueForKey:@"type"] forKey:@"type"];
            [adsEntity setValue:[ad valueForKey:@"photo"] forKey:@"photo"];
            [adsEntity setValue:[ad valueForKey:@"active"] forKey:@"active"];
        }
        NSString *path = [NSString stringWithFormat:@"https://easyhomeapp.000webhostapp.com%@",[userInfo valueForKeyPath:@"response.userInfo.avatar_path"]];
        NSURL *url = [NSURL URLWithString:path];
        NSData *data = [NSData dataWithContentsOfURL:url];
        [entity setValue:data forKey:@"avatarImage"];
        [context save:nil];

}

+ (void)loadAdInfoInDatabase:(NSInteger)adId and:(NSDictionary *)adInfo {
    NSPersistentContainer *persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    NSManagedObjectContext *context = persistentContainer.viewContext;
    NSManagedObject *entity = [NSEntityDescription insertNewObjectForEntityForName:@"ADS"
                                                            inManagedObjectContext:context];
        [entity setValue:[@(adId) stringValue] forKey:@"adsId"];
        [entity setValue:[adInfo valueForKeyPath:@"advertInfo.name"] forKey:@"name"];
        [entity setValue:[adInfo valueForKeyPath:@"advertInfo.price"] forKey:@"price"];
        [entity setValue:[[adInfo valueForKeyPath:@"advertInfo.type"] stringValue] forKey:@"type"];
        NSString *path_photo = [NSString stringWithFormat:@"/photo/ads/%li_0.jpg",adId];
        [entity setValue:path_photo forKey:@"photo"];
        [context save:nil];
}

+ (void)loadComfortsInContext:(NSManagedObjectContext *)context {
    [[APIService sharedService] getAllComfortsWithComplition:^(BOOL success, NSData *data, NSError *error) {
        if (success) {
            NSArray *comforts = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"response"];
            for (id comfort in comforts) {
                NSManagedObject *entity = [NSEntityDescription insertNewObjectForEntityForName:@"COMFORT"
                                                                        inManagedObjectContext:context];
                [entity setValue:[comfort valueForKey:@"comfortId"] forKey:@"comfortId"];
                [entity setValue:[comfort valueForKey:@"name"] forKey:@"name"];
            }
                    [context save:nil];
        }
    }];
}

+ (void)mapCityInContext:(NSManagedObjectContext *)context {
    NSString *path= [[NSBundle mainBundle] pathForResource:@"city" ofType:@"json"];
    NSData *output = [NSData dataWithContentsOfFile:path];
    NSArray *cities =[NSJSONSerialization JSONObjectWithData:output options:0 error:nil];
    
    for (id city in cities)
    {
        if ([[city objectForKey:@"country_code"]  isEqual: @"RU"]) {
            NSManagedObject *entity = [NSEntityDescription insertNewObjectForEntityForName:@"CITIES"
                                                                    inManagedObjectContext:context];
            [entity setValue:[city objectForKey:@"code"] forKey:@"code"];
            [entity setValue:[city objectForKey:@"name"] forKey:@"name"];
        }
    }
    [context save:nil];
    
}

@end
