//
//  AddAdvertModel.h
//  MDKP
//
//  Created by Андрей on 18.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^myCompletion)(BOOL);

@interface AddAdvertModel : NSObject

- (void)uploadAdInfo:(NSDictionary *)adInfo images:(NSArray *)images withCompletion:(myCompletion)completion;

- (NSString *)getTitleType:(NSInteger)value;

- (NSString *)getOwnerId;

- (NSString *)getTitleComforts:(NSArray *)ids;

@end
