//
//  AddAdvertModel.m
//  MDKP
//
//  Created by Андрей on 18.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "AddAdvertModel.h"
#import "APIService.h"
#import "AppDelegate.h"
#import "UIKit/UIKit.h"

@interface AddAdvertModel ()

@property (nonatomic, strong) NSPersistentContainer *persistentContainer;

@end

@implementation AddAdvertModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}

- (void)uploadAdInfo:(NSDictionary *)adInfo images:(NSArray *)images withCompletion:(myCompletion)completion {
    NSMutableArray *dataImage = [NSMutableArray array];
    for (id img in images) {
        [dataImage addObject:UIImageJPEGRepresentation(img, 1)];
    }
    [[APIService sharedService] addAdvert:adInfo images:dataImage withComplition:^(BOOL success, NSError *error) {
            completion(success);
    }];
}

- (NSString *)getOwnerId {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"USERINFO"];
    NSArray *res = [context executeFetchRequest:request error:nil];
    return [[res firstObject] valueForKey:@"userId"];
}

- (NSString *)getTitleType:(NSInteger)value {
    if (value == 1) return @"Жилье целиком";
    if (value == 2) return @"Отдельная комната";
    if (value == 3) return @"Общая комната";
    return @"Не выбрано";
}

- (NSString *)getTitleComforts:(NSArray *)ids {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"COMFORT"];
    
    NSString *result = @"";
    for (id comfortId in ids) {
        [request setPredicate:[NSPredicate predicateWithFormat:@"comfortId == %@", comfortId]];
        NSArray *res = [context executeFetchRequest:request error:nil];
      result = [result stringByAppendingString:[NSString stringWithFormat:@"%@, ",[[res firstObject] valueForKey:@"name"]]];
    }
    if ([result  isEqual: @""]) result = @"Не выбрано";
    else result = [result substringToIndex:[result length] - 2];
    return result;
}

@end
