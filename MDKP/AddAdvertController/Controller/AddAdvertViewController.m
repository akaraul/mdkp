//
//  AddAdvertViewController.m
//  MDKP
//
//  Created by Андрей on 02.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "AddAdvertViewController.h"
#import "AddAdvertView.h"
#import "AppDelegate.h"
#import "GMImagePickerController.h"
#import "CityViewController.h"
#import "TypeHomeViewController.h"
#import "PhotoCollectionViewCell.h"
#import "ComfortsViewController.h"
#import "AddAdvertModel.h"

NSString* const PhotosCellRuseIdentifier = @"PhotoCellRuseIdentifier";

@interface AddAdvertViewController () <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, GMImagePickerControllerDelegate, UICollectionViewDataSource,UICollectionViewDelegate, CityViewControllerDelegate, TypeHomeControllerDelegate, ComfortsViewControllerDelegate, UITextFieldDelegate>

@property (nonatomic,strong) AddAdvertView *addAdvertView;
@property (nonatomic, strong) AddAdvertModel *model;
@property (nonatomic, assign) BOOL isPhotoSelected;

@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableDictionary *adInfo;

@end

@implementation AddAdvertViewController

- (void)loadView {
    self.addAdvertView = [[AddAdvertView alloc] init];
    self.view = self.addAdvertView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.addAdvertView.nameAd.delegate = self;
    self.addAdvertView.minPeriod.delegate = self;
    self.addAdvertView.guests.delegate = self;
    self.addAdvertView.rooms.delegate = self;
    self.addAdvertView.price.delegate = self;
    self.addAdvertView.aboutAd.delegate = self;
    
    self.model = [[AddAdvertModel alloc] init];
    
    self.photos = [NSMutableArray array];
    self.adInfo = [NSMutableDictionary dictionary];
    
    self.navigationItem.title = @"Новое объявление";
    self.addAdvertView.infoAdvert.delegate = self;
    self.addAdvertView.infoAdvert.dataSource = self;
    
    self.addAdvertView.photos.delegate = self;
    self.addAdvertView.photos.dataSource = self;
    [self.addAdvertView.photos registerClass:[PhotoCollectionViewCell class] forCellWithReuseIdentifier:PhotosCellRuseIdentifier];
    
    self.isPhotoSelected = NO;
    
    [self.addAdvertView.addPhotos addTarget:self action:@selector(addPhotos:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.addAdvertView.addAdvert addTarget:self action:@selector(addAdvert:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menuIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(openMenu:)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [gestureRecognizer setCancelsTouchesInView:NO];
    [self.addAdvertView addGestureRecognizer:gestureRecognizer];
}

- (void)addAdvert:(UIButton *)sender {
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    [self.adInfo setValue:[self.model getOwnerId] forKey:@"ownerId"];
    [self.adInfo setValue:self.addAdvertView.beds.text forKey:@"beds"];
    [self.adInfo setValue:self.addAdvertView.bathrooms.text  forKey:@"bathrooms"];
    [self.adInfo setValue:self.addAdvertView.nameAd.text forKey:@"name"];
    [self.adInfo setValue:self.addAdvertView.minPeriod.text forKey:@"minPeriod"];
    [self.adInfo setValue:self.addAdvertView.guests.text forKey:@"guests"];
    [self.adInfo setValue:self.addAdvertView.rooms.text forKey:@"rooms"];
    [self.adInfo setValue:self.addAdvertView.aboutAd.text forKey:@"about"];
    [self.adInfo setValue:self.addAdvertView.price.text forKey:@"price"];
    
    [result setValue:[self.adInfo valueForKey:@"comforts"] forKey:@"comforts"];
    [self.adInfo removeObjectForKey:@"comforts"];
    [result setValue:@(self.photos.count) forKey:@"countPhotos"];
    [result setValue:self.adInfo forKey:@"advertInfo"];
    NSLog(@"%@",result);
    [self.addAdvertView.indicatorView startAnimating];
    [self.model uploadAdInfo:result images:self.photos withCompletion:^(BOOL success){
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Объявление добавлено!"
                                     message:@"Теперь оно появится в поиске!"
                                     preferredStyle:UIAlertControllerStyleAlert];
        if (!success) {
            [alert setTitle:@"Ошибка!"];
            [alert setMessage:@"Объявление не было добавлено! Проверьте подключение к сети!"];
        }
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:nil];
        
        [alert addAction:okButton];
        [self.addAdvertView.indicatorView stopAnimating];
        [self presentViewController:alert animated:YES completion:nil];

    }];
    //[self.addAdvertView.indicatorView stopAnimating];
}

- (void)addPhotos:(UIButton *)sender {
    GMImagePickerController *picker = [[GMImagePickerController alloc] init];
    picker.delegate = self;
    picker.displayAlbumsNumberOfAssets = YES;
    picker.customSmartCollections = nil;
    picker.showCameraButton = YES;
    picker.autoSelectCameraImages = YES;
    picker.mediaTypes = @[@(PHAssetMediaTypeImage)];
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)assetsPickerController:(GMImagePickerController *)picker didFinishPickingAssets:(NSArray *)assetArray{
    self.isPhotoSelected = YES;
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self getPhotosFromPhassets:assetArray];
    [self.addAdvertView.photos reloadData];
    if ([self.addAdvertView.infoAdvert numberOfRowsInSection:6] == 1) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:6];
        [self.addAdvertView.infoAdvert insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        [self.addAdvertView.infoAdvert scrollToRowAtIndexPath:indexPath
                                             atScrollPosition:UITableViewScrollPositionTop
                                                     animated:YES];
    }
    [self.addAdvertView.photos reloadData];
}

- (void)getPhotosFromPhassets:(NSArray *)assetArray {
    PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    requestOptions.synchronous = true;
    __block UIImage *ima;
    for (PHAsset *asset in assetArray) {
        [[PHImageManager defaultManager] requestImageForAsset:asset
                           targetSize:PHImageManagerMaximumSize
                          contentMode:PHImageContentModeDefault
                              options:requestOptions
                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                            ima = image;
                            [self.photos addObject:ima];
                        }];
    }
}

- (void)deletePhoto:(UIButton *)sender {
    [self.photos removeObjectAtIndex:sender.tag];
    [self.addAdvertView.photos reloadData];
    if (self.photos.count == 0) {
        self.isPhotoSelected = NO;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:6];
        [self.addAdvertView.infoAdvert deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }
}

- (void)openMenu:(UIBarButtonItem *)sender {
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.addAdvertView.topOffset = self.topLayoutGuide.length;
}

#pragma Hide keyboard

- (void)keyboardWasShown:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGRect keyboardRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    UIEdgeInsets contentInset = self.addAdvertView.infoAdvert.contentInset;
    contentInset.bottom = keyboardRect.size.height + 7.0;
    self.addAdvertView.infoAdvert.contentInset = contentInset;
}

- (void)keyboardWillBeHidden:(NSNotification*)notification {
    self.addAdvertView.infoAdvert.contentInset = UIEdgeInsetsZero;
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

#pragma Select delegates

- (void)cityDidSelected:(NSString *)city {
    [self.adInfo setValue:city forKey:@"city"];
    [self.addAdvertView.infoAdvert reloadData];
}

- (void)typeHomeDidSelected:(NSArray *)typeHome {
    [self.adInfo setValue:typeHome[0] forKey:@"type"];
    [self.addAdvertView.infoAdvert reloadData];
}

- (void)comfortsDidSelected:(NSArray *)comforts {
    [self.adInfo setValue:comforts forKey:@"comforts"];
    [self.addAdvertView.infoAdvert reloadData];
}

#pragma UITableviewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 8;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 3) return 6;
    if (section == 6 && self.isPhotoSelected) return 2;
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
        switch ([indexPath section]) {
            case 0:
                cell.detailTextLabel.text = [self.model getTitleType:[[self.adInfo valueForKey:@"type"] integerValue]];
                cell.textLabel.text = @"Выберите тип";
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            case 1:
                cell.detailTextLabel.text = [self.adInfo valueForKey:@"city"] ?: @"Не выбрано";
                cell.textLabel.text = @"Выберите город";
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            case 2:
                cell.detailTextLabel.text = [self.model getTitleComforts:[self.adInfo objectForKey:@"comforts"]];
                cell.textLabel.text = @"Доступные удобства";
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            case 3:
                switch ([indexPath row]) {
                    case 0:
                        [cell.contentView addSubview:self.addAdvertView.nameAd];
                        break;
                    case 1:
                        [cell.contentView addSubview:self.addAdvertView.minPeriod];
                        break;
                    case 2:
                        [cell.contentView addSubview:self.addAdvertView.guests];
                        break;
                    case 3:
                        [cell.contentView addSubview:self.addAdvertView.rooms];
                        break;
                    case 4:
                        [cell.contentView addSubview:self.addAdvertView.bathrooms];
                        break;
                    case 5:
                        [cell.contentView addSubview:self.addAdvertView.beds];
                        break;
                }
                break;
            case 4:
                [cell.contentView addSubview:self.addAdvertView.aboutAd];
                break;
            case 5:
                [cell.contentView addSubview:self.addAdvertView.price];
                break;
            case 6:
                switch ([indexPath row]) {
                    case 0:
                        [cell.contentView addSubview:self.addAdvertView.addPhotos];
                        break;
                    case 1:
                        [cell.contentView addSubview:self.addAdvertView.photos];
                        break;
                }
                break;
            case 7:
                cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [cell.contentView addSubview:self.addAdvertView.addAdvert];
                break;
        }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath section] == 4) return 120;
    if ([indexPath section]==6 && [indexPath row] == 1) return 70;
    if ([indexPath section] == 7) return 50;
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath section] == 1) {
        CityViewController *cityController = [[CityViewController alloc] init];
        cityController.delegate = self;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cityController];
        [self presentViewController:nav animated:YES completion:nil];
    }
    if ([indexPath section] == 0) {
        TypeHomeViewController *typeController = [[TypeHomeViewController alloc] init];
        typeController.delegate = self;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:typeController];
        [self presentViewController:nav animated:YES completion:nil];
    }
    if ([indexPath section] == 2) {
        ComfortsViewController *comfortsController = [[ComfortsViewController alloc] init];
        comfortsController.delegate = self;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:comfortsController];
        [self presentViewController:nav animated:YES completion:nil];
    }
}

#pragma UITextViewDelegate
- (void) textViewDidChange:(UITextView *)theTextView {
    if(![self.addAdvertView.aboutAd hasText]) {
        [self.addAdvertView.aboutAd addSubview:self.addAdvertView.placeholderTextView];
    } else if ([[self.addAdvertView.aboutAd subviews] containsObject:self.addAdvertView.placeholderTextView]) {
        [self.addAdvertView.placeholderTextView removeFromSuperview];
    }
}

- (void)textViewDidEndEditing:(UITextView *)theTextView {
    if (![self.addAdvertView.aboutAd hasText]) {
        [self.addAdvertView.aboutAd addSubview:self.addAdvertView.placeholderTextView];
    }
}

#pragma UICollectionViewDelegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.photos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
        PhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PhotosCellRuseIdentifier forIndexPath:indexPath];
    cell.imageView.image = [self.photos objectAtIndex:[indexPath item]];
    cell.deleteButton.tag = [indexPath item];
    [cell.deleteButton addTarget:self action:@selector(deletePhoto:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

#pragma UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType==UIReturnKeyNext) {
        UIView *next = [self.addAdvertView viewWithTag:textField.tag+1];
        [next becomeFirstResponder];
    } else if (textField.returnKeyType==UIReturnKeyDone) {
        [textField resignFirstResponder];
    }
    return YES;
}

@end
