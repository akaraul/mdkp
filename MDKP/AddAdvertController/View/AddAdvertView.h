//
//  AddAdvertView.h
//  MDKP
//
//  Created by Андрей on 02.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAdvertView : UIView

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property (nonatomic, strong) UIButton *addAdvert;
@property (nonatomic, strong) UITableView *infoAdvert;

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic, strong) UICollectionView *photos;

@property (nonatomic, strong) UITextField *nameAd;
@property (nonatomic, strong) UITextField *minPeriod;
@property (nonatomic, strong) UITextField *guests;
@property (nonatomic, strong) UITextField *beds;
@property (nonatomic, strong) UITextField *bathrooms;
@property (nonatomic, strong) UITextField *rooms;
@property (nonatomic, strong) UITextField *price;

@property (nonatomic, strong) UITextView *aboutAd;
@property (nonatomic, strong) UILabel *placeholderTextView;

@property (nonatomic, strong) UIButton *addPhotos;

@property (nonatomic, assign) CGFloat topOffset;

@end
