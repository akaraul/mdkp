//
//  PhotoCollectionViewCell.m
//  MDKP
//
//  Created by Андрей on 30.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "PhotoCollectionViewCell.h"

@implementation PhotoCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        _imageView = [[UIImageView alloc] init];
        [self addSubview:_imageView];
        
        _deleteButton = [[UIButton alloc] init];
        _deleteButton.layer.cornerRadius = 12.5f;
        _deleteButton.backgroundColor = [UIColor colorWithWhite:1.0f
                                                          alpha:0.7f];
        [_deleteButton setImage:[UIImage imageNamed:@"closeIcon"] forState:UIControlStateNormal];
        [self addSubview:_deleteButton];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.imageView.frame = self.bounds;
    
    self.deleteButton.frame = CGRectMake(self.bounds.size.width-25, 2, 23, 23);
}

@end
