//
//  AddAdvertView.m
//  MDKP
//
//  Created by Андрей on 02.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "AddAdvertView.h"

@implementation AddAdvertView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    self.backgroundColor = [UIColor whiteColor];
    
    _flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [_flowLayout  setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    _flowLayout.minimumLineSpacing = 10;
    
    _photos = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_flowLayout];
    _photos.backgroundColor = [UIColor whiteColor];
    [_photos setShowsHorizontalScrollIndicator:NO];
    
    _addAdvert = [[UIButton alloc] init];
    _addAdvert.backgroundColor = colorGreen;
    _addAdvert.layer.cornerRadius = 10.0f;
    [_addAdvert setTitle:@"Добавить объявление" forState:UIControlStateNormal];
    [self addSubview:_addAdvert];
    
    _nameAd = [[UITextField alloc] init];
    _nameAd.tag = 0;
    _nameAd.returnKeyType = UIReturnKeyNext;
    _nameAd.placeholder = @"Название";
    
    _guests = [[UITextField alloc] init];
    _guests.tag = 2;
    _guests.returnKeyType = UIReturnKeyNext;
    _guests.placeholder = @"Количество гостей";
    _guests.keyboardType = UIKeyboardTypeNumberPad;
    
    _rooms = [[UITextField alloc] init];
    _rooms.placeholder = @"Количество комнат";
    _rooms.tag = 3;
    _rooms.returnKeyType = UIReturnKeyNext;
    _rooms.keyboardType = UIKeyboardTypeNumberPad;
    
    _minPeriod = [[UITextField alloc] init];
    _minPeriod.placeholder = @"Минимальный срок";
    _minPeriod.tag = 1;
    _minPeriod.returnKeyType = UIReturnKeyNext;
    _minPeriod.keyboardType = UIKeyboardTypeNumberPad;
    
    _beds = [[UITextField alloc] init];
    _beds.placeholder = @"Кровати";
    _beds.keyboardType = UIKeyboardTypeNumberPad;
    
    _bathrooms = [[UITextField alloc] init];
    _bathrooms.placeholder = @"Ванные комнаты";
    _bathrooms.keyboardType = UIKeyboardTypeNumberPad;
    
    _placeholderTextView = [[UILabel alloc] init];
    _placeholderTextView.text = @"Подробная информация";
    [_placeholderTextView setBackgroundColor:[UIColor clearColor]];
    //[_placeholderTextView setFont:[challengeDescription font]];
    [_placeholderTextView setTextColor:[UIColor lightGrayColor]];
    
    _aboutAd = [[UITextView alloc] init];
    _aboutAd.tag = 4;
    [_aboutAd setFont:[UIFont systemFontOfSize:16]];
    _aboutAd.returnKeyType = UIReturnKeyNext;
    [_aboutAd addSubview:_placeholderTextView];
    
    _infoAdvert = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    //_infoAdvert.allowsSelection = NO;
    [self addSubview:_infoAdvert];
    
    _price = [[UITextField alloc] init];
    _price.placeholder = @"Цена в сутки";
    _price.tag = 5;
    _price.returnKeyType = UIReturnKeyDone;
    _price.keyboardType = UIKeyboardTypeDecimalPad;
    
    _addPhotos = [[UIButton alloc] init];
    [_addPhotos setImage:[UIImage imageNamed:@"addPhotoIcon"] forState:UIControlStateNormal];
    
    _addAdvert = [[UIButton alloc] init];
    _addAdvert.backgroundColor = colorGreen;
    _addAdvert.layer.cornerRadius = 10.0f;
    [_addAdvert setTitle:@"Добавить" forState:UIControlStateNormal];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _indicatorView.hidesWhenStopped = YES;
    _indicatorView.color = [UIColor blueColor];
    _indicatorView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    [self addSubview:_indicatorView];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGRect sizeTextField = CGRectMake(offset, 0, width-offset*2, 60);
    
    self.indicatorView.center = self.center;
    self.infoAdvert.frame = CGRectMake(0, self.topOffset, width, height - self.topOffset);
    self.aboutAd.frame = CGRectMake(offset, 0, width, 120);
    self.placeholderTextView.frame = CGRectMake(0, 0, width, 40);
    self.addPhotos.frame = sizeTextField;
    self.nameAd.frame = sizeTextField;
    self.minPeriod.frame = sizeTextField;
    self.rooms.frame = sizeTextField;
    self.guests.frame = sizeTextField;
    self.bathrooms.frame = sizeTextField;
    self.beds.frame = sizeTextField;
    self.price.frame = sizeTextField;
    self.addAdvert.frame = CGRectMake(offset, 0, width-offset*2, 50);
    
    self.flowLayout.itemSize = CGSizeMake(60, 60);
    self.photos.frame = CGRectMake(offset, 0, width-offset*2, 70);
    
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}


@end
