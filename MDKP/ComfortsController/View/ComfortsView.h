//
//  ComfortsView.h
//  MDKP
//
//  Created by Андрей on 01.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComfortsView : UIView

@property (nonatomic, strong) UIButton *save;

@property (nonatomic, strong) UITableView *comfortsTable;

@end
