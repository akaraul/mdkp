//
//  ComfortsView.m
//  MDKP
//
//  Created by Андрей on 01.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "ComfortsView.h"

@implementation ComfortsView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    self.backgroundColor = [UIColor whiteColor];
    
    _save = [[UIButton alloc] init];
    [_save setTitle:@"Сохранить" forState:UIControlStateNormal];
    _save.backgroundColor = colorGreen;
    _save.layer.cornerRadius = 10.0f;
    [self addSubview:_save];
    
    _comfortsTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _comfortsTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_comfortsTable];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.bounds.size.width;
    CGFloat widthButton = width*0.8;
    
    self.save.frame = CGRectMake(width/2 - widthButton/2,
                                 15.0f,
                                 widthButton,
                                 50.0f);
    
    self.comfortsTable.frame = CGRectMake(0, 0, width, self.bounds.size.height - 80);
}

@end
