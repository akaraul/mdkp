//
//  ComfortsModel.m
//  MDKP
//
//  Created by Андрей on 19.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "ComfortsModel.h"

@implementation ComfortsModel

- (UIImage *)getComfortImageById:(NSInteger)comfortId {
    UIImage *icon;
    switch (comfortId) {
        case 1:
            return icon = [UIImage imageNamed:@"wifiIcon"];
        case 2:
            return icon = [UIImage imageNamed:@"tvIcon"];
        case 3:
            return icon = [UIImage imageNamed:@"parkingIcon"];
        case 4:
            return icon = [UIImage imageNamed:@"nofireIcon"];
        case 5:
            return icon = [UIImage imageNamed:@"bathroomIcon"];
        case 6:
            return icon = [UIImage imageNamed:@"hairdryerIcon"];
        case 7:
            return icon = [UIImage imageNamed:@"ironIcon"];
        case 8:
            return icon = [UIImage imageNamed:@"conditionerIcon"];
        case 9:
            return icon = [UIImage imageNamed:@"radiatorIcon"];
        case 10:
            return icon = [UIImage imageNamed:@"washmachineIcon"];
        case 11:
            return icon = [UIImage imageNamed:@"firePlaceIcon"];
        case 12:
            return icon = [UIImage imageNamed:@"poolIcon"];
        case 13:
            return icon = [UIImage imageNamed:@"gymIcon"];
        case 14:
            return icon = [UIImage imageNamed:@"elevatorIcon"];
        case 15:
            return icon = [UIImage imageNamed:@"airmachineIcon"];
        case 16:
            return icon = [UIImage imageNamed:@"ethernetIcon"];
        case 17:
            return icon = [UIImage imageNamed:@"hdtvIcon"];
        case 18:
            return icon = [UIImage imageNamed:@"kitchenIcon"];
        case 19:
            return icon = [UIImage imageNamed:@"jacuzziIcon"];
    }
    return nil;
}

@end
