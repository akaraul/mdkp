//
//  ComfortsModel.h
//  MDKP
//
//  Created by Андрей on 19.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

@interface ComfortsModel : NSObject

- (UIImage *)getComfortImageById:(NSInteger)comfortId;

@end
