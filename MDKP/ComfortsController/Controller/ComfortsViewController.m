//
//  ComfortsViewController.m
//  MDKP
//
//  Created by Андрей on 01.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "ComfortsViewController.h"
#import "ComfortsView.h"
#import "ComfortsModel.h"
#import "CoreData/CoreData.h"
#import "AppDelegate.h"
#import "COMFORT+CoreDataClass.h"

@interface ComfortsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) ComfortsView *comfortsView;
@property (nonatomic, strong) ComfortsModel *model;
@property (nonatomic, strong) NSMutableArray *selectedComforts;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSPersistentContainer *persistentContainer;


@end

@implementation ComfortsViewController

- (void)loadView {
    self.comfortsView = [[ComfortsView alloc] init];
    _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    self.view = self.comfortsView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[ComfortsModel alloc] init];
    
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"COMFORT"];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"comfortId" ascending:YES];
    [request setSortDescriptors:@[sort]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc]
                                     initWithFetchRequest:request
                                     managedObjectContext:context
                                     sectionNameKeyPath:nil
                                     cacheName:nil];
    [self.fetchedResultsController performFetch:nil];

    
    self.selectedComforts = [NSMutableArray array];
    
    self.navigationItem.title = @"Удобства";
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    
    self.comfortsView.comfortsTable.delegate = self;
    self.comfortsView.comfortsTable.dataSource = self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"closeIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(closeView:)];
    
    [self.comfortsView.save addTarget:self action:@selector(saveParametrs:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.barStyle = UIBarStyleDefault;
    CGFloat toolbarHeight = 80;
    CGRect rootViewBounds = self.parentViewController.view.bounds;
    CGFloat rootViewHeight = CGRectGetHeight(rootViewBounds);
    CGFloat rootViewWidth = CGRectGetWidth(rootViewBounds);
    CGRect rectArea = CGRectMake(0, rootViewHeight - toolbarHeight, rootViewWidth, toolbarHeight);
    [toolbar setFrame:rectArea];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.comfortsView.save];
    
    [toolbar setItems:[NSArray arrayWithObjects:barButtonItem,nil]];
    [self.navigationController.view addSubview:toolbar];
}

- (void)saveParametrs:(UIButton *)sender {
    [self.delegate comfortsDidSelected:self.selectedComforts];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)closeView:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma UITableviewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchedResultsController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Comfort"];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    COMFORT *cellObject = [self.fetchedResultsController.fetchedObjects objectAtIndex:indexPath.row];
    cell.textLabel.text = cellObject.name;
    cell.tag = [cellObject.comfortId integerValue];
    cell.imageView.image = [self.model getComfortImageById:cell.tag];
    if ([self.selectedComforts containsObject:@(cell.tag)]) cell.accessoryType = UITableViewCellAccessoryCheckmark;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark){
        cell.accessoryType = UITableViewCellAccessoryNone;
        [self.selectedComforts removeObject:@(cell.tag)];
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.selectedComforts addObject:@(cell.tag)];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0f;
}


@end
