//
//  ComfortsViewController.h
//  MDKP
//
//  Created by Андрей on 01.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  ComfortsViewControllerDelegate;

@interface ComfortsViewController : UIViewController
@property (nonatomic, weak) id<ComfortsViewControllerDelegate> delegate;

@end

@protocol ComfortsViewControllerDelegate <NSObject>

- (void)comfortsDidSelected:(NSArray *)comforts;

@end
