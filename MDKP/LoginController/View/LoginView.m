//
//  LoginView.m
//  MDKP
//
//  Created by Андрей on 04.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "LoginView.h"

@implementation LoginView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    
    self.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1];
    
    _labelLoginView = [[UILabel alloc] init];
    _labelLoginView.text = @"Вход";
    _labelLoginView.textAlignment = NSTextAlignmentCenter;
    _labelLoginView.font=[_labelLoginView.font fontWithSize:33];
    [self addSubview:_labelLoginView];
    
    _errorLabel = [[UILabel alloc] init];
    //_errorLabel.text = @"Ошибка ывпыпвгыпвгы";
    _errorLabel.textColor = [UIColor redColor];
    _errorLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_errorLabel];
    
    _loginTextField = [[UITextField alloc] init];
    _loginTextField.borderStyle = UITextBorderStyleRoundedRect;
    _loginTextField.placeholder = @"E-mail";
    _loginTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _loginTextField.keyboardType = UIKeyboardTypeEmailAddress;
    _loginTextField.returnKeyType = UIReturnKeyNext;
    _loginTextField.tag = 0;
    [self addSubview:_loginTextField];
    
    _passwordTextField = [[UITextField alloc] init];
    _passwordTextField.borderStyle = UITextBorderStyleRoundedRect;
    _passwordTextField.placeholder = @"Пароль";
    _passwordTextField.secureTextEntry = YES;
    _passwordTextField.tag = 1;
    _passwordTextField.returnKeyType = UIReturnKeyDone;
    [self addSubview:_passwordTextField];
    
    _loginButton = [[UIButton alloc] init];
    _loginButton.backgroundColor = colorGreen;
    _loginButton.layer.cornerRadius = 10.0f;
    _loginButton.tintColor = [UIColor whiteColor];
    [_loginButton setTitle:@"Войти" forState:UIControlStateNormal];
    [self addSubview:_loginButton];
    
    _regButton = [[UIButton alloc] init];
    [_regButton setTitleColor:colorGreen forState:UIControlStateNormal];
    _regButton.titleLabel.font = [UIFont systemFontOfSize:16.0];
    [_regButton setTitle:@"Вы здесь впервые? Регистрация" forState:UIControlStateNormal];
    [self addSubview:_regButton];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _indicatorView.hidesWhenStopped = YES;
    _indicatorView.color = [UIColor blueColor];
    [self addSubview:_indicatorView];
    
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGFloat heightElem = 40.0f;
    CGFloat heightButton = 45.0f;
    
    self.indicatorView.center = self.center;
    
    self.labelLoginView.frame = CGRectMake((width - 100.0f)/2, height*0.2f, 100.0f, heightElem);
    
    self.loginTextField.frame = CGRectMake(offset*3,
                                           self.labelLoginView.bounds.size.height + self.labelLoginView.frame.origin.y + offset*5,
                                           width - offset*6,
                                           heightElem);
    
    self.passwordTextField.frame = CGRectMake(self.loginTextField.frame.origin.x,
                                              self.loginTextField.frame.origin.y + self.loginTextField.bounds.size.height + offset*2,
                                              self.loginTextField.bounds.size.width,
                                              heightElem);
    
    self.errorLabel.frame = CGRectMake(self.loginTextField.frame.origin.x,
                                       self.passwordTextField.frame.origin.y + self.passwordTextField.bounds.size.height,
                                       self.loginTextField.bounds.size.width,
                                       heightElem);
    
    self.regButton.frame = CGRectMake(self.loginTextField.frame.origin.x,
                                      self.errorLabel.frame.origin.y + self.errorLabel.bounds.size.height,
                                      self.loginTextField.bounds.size.width,
                                      heightElem/2);
    
    self.loginButton.frame = CGRectMake(self.loginTextField.frame.origin.x,
                                        height - heightButton - offset*6,
                                        self.loginTextField.bounds.size.width,
                                        heightButton);

    
 
    
}

@end
