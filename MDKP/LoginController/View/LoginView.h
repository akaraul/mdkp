//
//  LoginView.h
//  MDKP
//
//  Created by Андрей on 04.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginView : UIView

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property (nonatomic,strong) UILabel *labelLoginView;
@property (nonatomic,strong) UILabel *errorLabel;
@property (nonatomic,strong) UITextField *loginTextField;
@property (nonatomic,strong) UITextField *passwordTextField;
@property (nonatomic,strong) UIButton *loginButton;
@property (nonatomic,strong) UIButton *regButton;

@end
