//
//  LoginViewController.m
//  MDKP
//
//  Created by Андрей on 04.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginView.h"
#import "MainViewController.h"
#import "MenuViewController.h"
#import "RegistrationViewController.h"
#import "AppDelegate.h"
#import "APIService.h"
#import <CommonCrypto/CommonDigest.h>

@interface LoginViewController () <UITextFieldDelegate>

@property (nonatomic,strong) LoginView *loginView;
@property (nonatomic, strong) NSMutableDictionary *authInfo;

@end

@implementation LoginViewController

- (void)loadView {
    self.loginView = [[LoginView alloc] init];
    self.view = self.loginView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.authInfo = [NSMutableDictionary dictionary];
    
    self.loginView.loginTextField.delegate =self;
    self.loginView.passwordTextField.delegate = self;
    
    [self.loginView.loginButton addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView.regButton addTarget:self action:@selector(regStart:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)regStart:(UIButton *)sender {
    RegistrationViewController *regController = [[RegistrationViewController alloc] init];
    [self presentViewController:regController animated:YES completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (BOOL)validation {
    NSString *errors = @"";
    
//    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//    if ([self.loginView.loginTextField.text isEqualToString:@""]) errors = [errors stringByAppendingString:@"Не указан e-mail"];
//    else if (![emailTest evaluateWithObject:self.loginView.loginTextField.text]) errors = [errors stringByAppendingString:@"Неверный формат e-mail"];
    
    if ([self.loginView.passwordTextField.text isEqualToString:@""]) errors = [errors stringByAppendingString:@"Не указан пароль"];
    
    
    if (![errors isEqualToString:@""]) {
        self.loginView.errorLabel.text = errors;
        return false;
    }
    return true;
}

- (void)login:(UIButton *)sender {
    if ([self validation]) {
        [self.authInfo setValue:self.loginView.loginTextField.text forKey:@"login"];
        [self.authInfo setValue:[self md5:self.loginView.passwordTextField.text] forKey:@"password"];
        [self.loginView.indicatorView startAnimating];
        [[APIService sharedService] loginUser:self.authInfo withComplition:^(BOOL success, NSError *error) {
            if (success) {
                AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                MainViewController *mainController = [[MainViewController alloc] init];
                MenuViewController *leftController = [[MenuViewController alloc] init];
                UINavigationController *centerController = [[UINavigationController alloc] initWithRootViewController:mainController];
                centerController.navigationBar.barTintColor = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
                [centerController.navigationBar
                 setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
                centerController.navigationBar.tintColor = [UIColor whiteColor];
                
                app.drawerController = [[MMDrawerController alloc] initWithCenterViewController:centerController leftDrawerViewController:leftController];
                app.drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModePanningCenterView;
                app.drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeAll;
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLoggedIn"];
                [self.loginView.indicatorView stopAnimating];
                [self presentViewController:app.drawerController animated:YES completion:nil];
            }
            else {
                [self.loginView.indicatorView stopAnimating];
                self.loginView.errorLabel.text = @"Неправильный логин или пароль";
            }
        }];
    }
//    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    
//    MainViewController *mainController = [[MainViewController alloc] init];
//    MenuViewController *leftController = [[MenuViewController alloc] init];
//    UINavigationController *centerController = [[UINavigationController alloc] initWithRootViewController:mainController];
//    centerController.navigationBar.barTintColor = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
//    [centerController.navigationBar
//     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
//    centerController.navigationBar.tintColor = [UIColor whiteColor];
//    
//    app.drawerController = [[MMDrawerController alloc] initWithCenterViewController:centerController leftDrawerViewController:leftController];
//    app.drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModePanningCenterView;
//    app.drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeAll;
//    
//    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLoggedIn"];
//    [self presentViewController:app.drawerController animated:YES completion:nil];
}

- (NSString *)md5:(NSString *)password {
    const char *ptr = [password UTF8String];
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(ptr, (CC_LONG)strlen(ptr), md5Buffer);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType==UIReturnKeyNext) {
        UIView *next = [[textField superview] viewWithTag:textField.tag+1];
        [next becomeFirstResponder];
    } else if (textField.returnKeyType==UIReturnKeyDone) {
        [textField resignFirstResponder];
    }
    return YES;
}

@end
