//
//  APIService.m
//  MDKP
//
//  Created by Андрей on 05.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "APIService.h"
#import "Mapper.h"
#import <UIKit/UIKit.h>

const NSString *site = @"https://easyhomeapp.000webhostapp.com/";

@interface APIService ()

@property (nonatomic, strong) NSURLSession *APIDataSession;
@property (nonatomic, assign) NSInteger requestsCount;

@end

@implementation APIService

+ (instancetype)sharedService {
    static APIService *sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [[APIService alloc] init];
    });
    return sharedService;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _requestsCount = 0;
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _APIDataSession = [NSURLSession sessionWithConfiguration:configuration];
    }
    return self;
}

- (void)addAdvert:(NSDictionary *)adInfo images:(NSArray *)images withComplition:(APIServiceCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/addAdvert",site]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:adInfo options:0 error:nil];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [self performRequest:request withCompletion:^(BOOL success, NSData *data, NSError *error) {
        if (success) {
        if (data == nil) {completion(NO, error); return;}
            NSInteger result = [[[NSJSONSerialization JSONObjectWithData:data
                                                                   options:0
                                                                     error:&error] objectForKey:@"response"] integerValue];
            if (result == 0) {completion(NO, error); return;}
                NSString *urlString = [ NSString stringWithFormat:@"%@api/loadAdPhoto",site];
                NSMutableArray *filenames = [NSMutableArray array];
                for (int i=0; i < images.count; i++) {
                    NSString *filename = [NSString stringWithFormat:@"%li_%i.jpg",(long)result,i];
                    [filenames addObject:filename];
                }
                [self uploadImages:images toUrl:urlString filenames:filenames withCompletion:^(BOOL success, NSError *error) {
                    if (success) {
                        [Mapper loadAdInfoInDatabase:result and:adInfo];
                        if (completion) completion(success,error);
                    }
                }];
            }
        else completion(NO, error);
    }];
}

- (void)getAllComfortsWithComplition:(APIServiceDataCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/getAllComforts",site]];
    [self performRequestWithURL:url completion:completion];
}

- (void)getAllReviews:(NSInteger)adId withComplition:(APIServiceDataCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/getAllReviews?adId=%ld",site,(long)adId]];
    [self performRequestWithURL:url completion:completion];
}

- (void)getOwnerContact:(NSInteger)userId withComplition:(APIServiceDataCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/getOwnerContacts?userId=%ld",site,(long)userId]];
    [self performRequestWithURL:url completion:completion];
}

- (void)changeAdActive:(NSInteger)adId active:(NSString *)active withComplition:(APIServiceDataCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/changeActive?adId=%ld&active=%@",site,(long)adId,active]];
    [self performRequestWithURL:url completion:completion];
}

- (void)deleteAd:(NSInteger)adId withComplition:(APIServiceDataCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/deleteAd?adId=%ld",site,(long)adId]];
    [self performRequestWithURL:url completion:completion];
}

- (void)searchResult:(NSDictionary *)options withComplition:(APIServiceDataCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/searchAds",site]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:options options:0 error:nil];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [self performRequest:request withCompletion:^(BOOL success, NSData *data, NSError *error) {
        if (success) {
            if (data == nil) {completion(NO,nil,error); return;}
            completion(success, data,error);
        }
        else completion(NO, nil ,error);
    }];
}

- (void)getUserInfoById:(NSInteger)userId withComplition:(APIServiceDataCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/getUserInfo?userId=%ld",site,(long)userId]];
    [self performRequestWithURL:url completion:completion];

}

- (void)getAdById:(NSInteger)adId withComplition:(APIServiceDataCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/getAdInfo?adId=%ld",site,(long)adId]];
    [self performRequestWithURL:url completion:completion];
}

- (void)updateUserInfo:(NSDictionary *)userInfo withComplition:(APIServiceCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/updateUserInfo",site]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [self performRequest:request withCompletion:^(BOOL success, NSData *data, NSError *error) {
        if (completion) {
            completion(success, error);
        }
        else completion(NO ,error);
    }];
}

- (void)addAdvertReview:(NSDictionary *)options withComplition:(APIServiceCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/addAdvertReview",site]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:options options:0 error:nil];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [self performRequest:request withCompletion:^(BOOL success, NSData *data, NSError *error) {
        if (completion) {
            completion(success, error);
        }
        else completion(NO ,error);
    }];

}

- (void)changeUserPassword:(NSDictionary *)userInfo withComplition:(APIServiceCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/changePassword",site]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [self performRequest:request withCompletion:^(BOOL success, NSData *data, NSError *error) {
        if (completion) {
            completion(success, error);
        }
        else completion(NO ,error);
    }];
}

- (void)updateUserAvatar:(NSData *)image byId:(NSInteger)userId withComplition:(APIServiceCompletion)completion {
    NSString *urlString = [ NSString stringWithFormat:@"%@api/loadUserAvatar",site];
    NSString *filename = [NSString stringWithFormat:@"%li.jpg",(long)userId];
    [self uploadImages:[NSArray arrayWithObject:image] toUrl:urlString filenames:[NSArray arrayWithObject:filename] withCompletion:^(BOOL success, NSError *error) {
        if (success) {
            completion(success,error);
        }
    }];
}

- (void)regUser:(NSDictionary *)userInfo withComplition:(APIServiceCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/regUser",site]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];

    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [self performRequest:request withCompletion:^(BOOL success, NSData *data, NSError *error) {
        if (success) {
            completion(success, error);
        }
        else completion(NO,error);
    }];
}

- (void)loginUser:(NSDictionary *)userInfo withComplition:(APIServiceCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/authorizeUser",site]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [self performRequest:request withCompletion:^(BOOL success, NSData *data, NSError *error) {
        if (data == nil) {completion(NO, error); return;}
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                    options:0
                                                      error:&error];
        if ([[result objectForKey:@"error"]  isEqual: @"0"]) {
            [Mapper loadUserInfoInDatabase:data];
            if (completion) {
                completion(success, error);
            }
        }
        else {
            if (completion) {
                completion(NO, error);
            }
        }
    }];
}

#pragma Private methods

- (void)performRequestWithURLString:(NSString *)URLString completion:(APIServiceDataCompletion)completion {
    [self performRequestWithURL:[NSURL URLWithString:URLString] completion:completion];
}

- (void)performRequestWithURL:(NSURL *)URL completion:(APIServiceDataCompletion)completion {
    [self performRequest:[[NSURLRequest alloc] initWithURL:URL] withCompletion:completion];
}

- (void)performRequest:(NSURLRequest *)request withCompletion:(APIServiceDataCompletion)completion {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.requestsCount++;
    });
    
    [[self.APIDataSession dataTaskWithRequest:request
                                  completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          if (data == nil) {completion(NO, data,error); return;}
                                          if (completion) {
                                              completion(!error, data, error);
                                          }
                                          self.requestsCount--;
                                      });
                                  }] resume];
}

- (void)uploadImages:(NSArray *)images toUrl:(NSString *)urlString filenames:(NSArray *)filenames withCompletion:(APIServiceCompletion)completion {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    for (int i=0; i<images.count; i++) {
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile[]\"; filename=\"%@\"\r\n", filenames[i]]] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:images[i]]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [self performRequest:request withCompletion:^(BOOL success, NSData *data, NSError *error) {
        if (completion) {
            completion(success, error);
        }
    }];

}

- (void)setRequestsCount:(NSInteger)requestsCount {
    _requestsCount = requestsCount;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:_requestsCount > 0];
}

@end
