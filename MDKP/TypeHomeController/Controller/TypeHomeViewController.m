//
//  TypeHomeViewController.m
//  MDKP
//
//  Created by Андрей on 01.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "TypeHomeViewController.h"
#import "TypeHomeView.h"

@interface TypeHomeViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) TypeHomeView *typeView;
@property (nonatomic, strong) NSMutableArray *selectedType;

@end

@implementation TypeHomeViewController

- (void)loadView {
    self.typeView = [[TypeHomeView alloc] init];
    self.view = self.typeView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.selectedType = [NSMutableArray array];
    
    self.navigationItem.title = @"Тип жилья";
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    
    self.typeView.typeTable.delegate = self;
    self.typeView.typeTable.dataSource = self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"closeIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(closeView:)];
    
    [self.typeView.save addTarget:self action:@selector(saveParametrs:) forControlEvents:UIControlEventTouchUpInside];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.barStyle = UIBarStyleDefault;
    CGFloat toolbarHeight = 80;
    CGRect rootViewBounds = self.parentViewController.view.bounds;
    CGFloat rootViewHeight = CGRectGetHeight(rootViewBounds);
    CGFloat rootViewWidth = CGRectGetWidth(rootViewBounds);
    CGRect rectArea = CGRectMake(0, rootViewHeight - toolbarHeight, rootViewWidth, toolbarHeight);
    [toolbar setFrame:rectArea];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.typeView.save];
    
    [toolbar setItems:[NSArray arrayWithObjects:barButtonItem,nil]];
    [self.navigationController.view addSubview:toolbar];
}

- (void)saveParametrs:(UIButton *)sender {
    if (self.typeView.allHome.selected) [self.selectedType addObject:@1];
    if (self.typeView.room.selected) [self.selectedType addObject:@2];
    if (self.typeView.sharedRoom.selected) [self.selectedType addObject:@3];
    [self.delegate typeHomeDidSelected:self.selectedType];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)closeView:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma UITableviewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    cell.textLabel.font = [UIFont systemFontOfSize:22.0];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:14.0];
    cell.detailTextLabel.numberOfLines = 3;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch ([indexPath row]) {
            case 0:
                cell.textLabel.text = @"Жилье целиком";
                cell.detailTextLabel.text = @"Получите жилье в свое полное\nраспоряжение.";
                [cell.contentView addSubview:self.typeView.allHome];
            break;
            case 1:
                cell.textLabel.text = @"Отдельная комната";
                cell.detailTextLabel.text = @"Получите собственную комнату\nв доме или квартире.";
                [cell.contentView addSubview:self.typeView.room];
            break;
            case 2:
                cell.textLabel.text = @"Общая комната";
                cell.detailTextLabel.text = @"Остановитесь в общем\nпространстве, с несколькими\nлюдьми в комнате.";
                [cell.contentView addSubview:self.typeView.sharedRoom];
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([indexPath row]) {
            case 0:
                self.typeView.allHome.selected = !self.typeView.allHome.selected;
            break;
            case 1:
                self.typeView.room.selected = !self.typeView.room.selected;
            break;
            case 2:
                self.typeView.sharedRoom.selected = !self.typeView.sharedRoom.selected;
            break;

    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 130.0f;
}

@end
