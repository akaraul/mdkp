//
//  TypeHomeViewController.h
//  MDKP
//
//  Created by Андрей on 01.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  TypeHomeControllerDelegate;

@interface TypeHomeViewController : UIViewController
@property (nonatomic, weak) id<TypeHomeControllerDelegate> delegate;

@end

@protocol TypeHomeControllerDelegate <NSObject>

- (void)typeHomeDidSelected:(NSArray *)typeHome;

@end
