//
//  TypeHomeView.m
//  MDKP
//
//  Created by Андрей on 01.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "TypeHomeView.h"

@implementation TypeHomeView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    self.backgroundColor = [UIColor whiteColor];
    
    _save = [[UIButton alloc] init];
    [_save setTitle:@"Сохранить" forState:UIControlStateNormal];
    _save.backgroundColor = colorGreen;
    _save.layer.cornerRadius = 10.0f;
    
    _typeTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _typeTable.scrollEnabled = NO;
    _typeTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_typeTable];
    
    _allHome = [[UIButton alloc] init];
    _allHome.layer.borderWidth = 1;
    _allHome.userInteractionEnabled = NO;
    [_allHome setImage:[UIImage imageNamed:@"checkBoxIcon"] forState:UIControlStateSelected];
    
    _room = [[UIButton alloc] init];
    _room.layer.borderWidth = 1;
    _room.userInteractionEnabled = NO;
    [_room setImage:[UIImage imageNamed:@"checkBoxIcon"] forState:UIControlStateSelected];
    
    _sharedRoom = [[UIButton alloc] init];
    _sharedRoom.layer.borderWidth = 1;
    _sharedRoom.userInteractionEnabled = NO;
    [_sharedRoom setImage:[UIImage imageNamed:@"checkBoxIcon"] forState:UIControlStateSelected];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.bounds.size.width;
    CGFloat widthButton = width*0.8;
    
    self.save.frame = CGRectMake(width/2 - widthButton/2,
                                 15.0f,
                                 widthButton,
                                 50.0f);
    
    self.typeTable.frame = self.bounds;
    
    self.allHome.frame = CGRectMake(width - 50, 50, 30, 30);
    self.room.frame = self.allHome.frame;
    self.sharedRoom.frame = self.allHome.frame;
    
}

@end
