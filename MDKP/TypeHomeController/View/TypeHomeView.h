//
//  TypeHomeView.h
//  MDKP
//
//  Created by Андрей on 01.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TypeHomeView : UIView

@property (nonatomic, strong) UIButton *save;

@property (nonatomic, strong) UITableView *typeTable;

@property (nonatomic, strong) UIButton *allHome;
@property (nonatomic, strong) UIButton *room;
@property (nonatomic, strong) UIButton *sharedRoom;

@end
