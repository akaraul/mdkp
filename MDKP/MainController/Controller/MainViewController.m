//
//  MainViewController.m
//  MDKP
//
//  Created by Андрей on 04.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "MainViewController.h"
#import "MainView.h"
#import "AppDelegate.h"
#import "SearchFiltersViewController.h"
#import "CityViewController.h"
#import "ResultTableViewCell.h"
#import "CountPeopleViewController.h"
#import "ResultModel.h"
#import "DetailViewController.h"
#import "HeaderView.h"
#import "APIService.h"

@interface MainViewController () <UITableViewDelegate, UITableViewDataSource, CityViewControllerDelegate, SearchFiltersViewControllerDelegate, CountPeopleViewControllerDelegate>

@property (nonatomic, strong) MainView *mainView;
@property (nonatomic, assign) BOOL findError;
@property (nonatomic, strong) NSMutableDictionary *searchParametrs;
@property (nonatomic, strong) HeaderView *stretchyHeaderView;
@property (nonatomic, strong) ResultModel *model;

@end

@implementation MainViewController

- (void)loadView {
    self.mainView = [[MainView alloc] init];
    self.view = self.mainView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[ResultModel alloc] init];
    
    self.searchParametrs = [NSMutableDictionary dictionary];
    [self.searchParametrs setValue:@"1" forKey:@"guests"];
    
    [self loadStretchyHeaderView];
    
    self.mainView.resTable.delegate = self;
    self.mainView.resTable.dataSource = self;
    
    [self.stretchyHeaderView.searchButton addTarget:self action:@selector(startSearch:) forControlEvents:UIControlEventTouchUpInside];
    [self.stretchyHeaderView.filterButton addTarget:self action:@selector(searchFilter:) forControlEvents:UIControlEventTouchUpInside];
    [self.stretchyHeaderView.menuButton addTarget:self action:@selector(openMenu:) forControlEvents:UIControlEventTouchUpInside];
    [self.stretchyHeaderView.place addTarget:self action:@selector(selectCity:) forControlEvents:UIControlEventTouchUpInside];
    [self.stretchyHeaderView.countPeople addTarget:self action:@selector(countPeople:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

#pragma NavBar hide

- (void)loadStretchyHeaderView {
    CGRect frame = CGRectMake(0, 0, self.mainView.resTable.frame.size.width, 220);
    HeaderView *headerView = [[HeaderView alloc] initWithFrame:frame];
    headerView.maximumContentHeight = 220;
    headerView.minimumContentHeight = 64;
    self.stretchyHeaderView = headerView;
    [self.mainView.resTable addSubview:headerView];
    [self.mainView.resTable setContentOffset:CGPointMake(0, -220) animated:YES];
}

#pragma Touch buttons

-(void)selectCity:(UIButton *)sender {
    CityViewController *cityController = [[CityViewController alloc] init];
    cityController.delegate = self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cityController];
    [self presentViewController:nav animated:YES completion:nil];
}

-(void)countPeople:(UIButton *)sender {
    CountPeopleViewController *peopleController = [[CountPeopleViewController alloc] init];
    peopleController.delegate = self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:peopleController];
    [self presentViewController:nav animated:YES completion:nil];
}

-(void)searchFilter:(UIButton *)sender {
    SearchFiltersViewController *filterController = [[SearchFiltersViewController alloc] init];
    filterController.delegate = self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:filterController];
    [self presentViewController:nav animated:YES completion:nil];
}

-(void)startSearch:(UIButton *)sender {
        NSLog(@"%@",self.searchParametrs);
    [self.model clearData];
    [self.mainView.indicatorView startAnimating];
    [[APIService sharedService] searchResult:self.searchParametrs withComplition:^(BOOL success, NSData *data, NSError *error) {
        if (success) {
                [self.model getSearchResults:data];
                [self.mainView.resTable reloadData];
                self.stretchyHeaderView.searchTextField.text = [NSString stringWithFormat:@"%@, %@",self.stretchyHeaderView.place.titleLabel.text, self.stretchyHeaderView.countPeople.titleLabel.text];
        }
        else {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Ошибка!"
                                         message:@"Проверьте подключение к сети!"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:nil];
            
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
        [self.mainView.indicatorView stopAnimating];
    }];
}

-(void)openMenu:(UIBarButtonItem *)sender {
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

#pragma Selected delegates

- (void)cityDidSelected:(NSString *)city {
    [self.searchParametrs setValue:city forKey:@"city"];
    [self.stretchyHeaderView.place setTitle:city forState:UIControlStateNormal];
}

- (void)countPeopleDidSelected:(NSInteger)count {
    [self.searchParametrs setValue:[@(count) stringValue] forKey:@"guests"];
    [self.stretchyHeaderView.countPeople setTitle:[NSString stringWithFormat:@"%li гость(-ей)",(long)count] forState:UIControlStateNormal];
}

- (void)searchParametrsDidSelected:(NSDictionary *)parametrs {
    [self.searchParametrs addEntriesFromDictionary:parametrs];
    NSLog(@"%@",self.searchParametrs);
}

#pragma UITableviewDelegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    self.findError = NO;
    NSInteger count = [self.model getCountAds];
    if (count == 0) {self.findError=YES; return 1;}
    return [self.model getCountAds];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.findError) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.textLabel.text = @"К сожалению, ничего не найдено :(";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
        ResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ResultTableViewCell reuseIdentifier]];
        if (!cell) {
            cell = [[ResultTableViewCell alloc] init];
        }
        [cell setItem:[self.model getAdByIndexPath:indexPath]];
        return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.findError) return;
    //[self.mainView.indicatorView setCenter:[tableView cellForRowAtIndexPath:indexPath].center];
    [self.mainView.indicatorView startAnimating];
    [self.model loadAdInfo:[tableView cellForRowAtIndexPath:indexPath].tag withCompletion:^(BOOL success){
        if (success) {
            DetailViewController *detailController = [[DetailViewController alloc] init];
            detailController.adInfo = [self.model getAdInfoDictionary];
            NSLog(@"%@",detailController.adInfo);
            [self.navigationController pushViewController:detailController animated:YES];
        }
        else {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Ошибка!"
                                         message:@"Проверьте подключение к сети!"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:nil];
            
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
        [self.mainView.indicatorView stopAnimating];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 300;
}



@end
