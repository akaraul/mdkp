//
//  MainView.h
//  MDKP
//
//  Created by Андрей on 04.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MARKRangeSlider.h"

@interface MainView : UIView

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;
@property (nonatomic, strong) UITableView *resTable;

@property (nonatomic, strong) UIView *priceView;
@property (nonatomic, strong) UILabel *priceHeader;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) MARKRangeSlider *priceSlider;


@end
