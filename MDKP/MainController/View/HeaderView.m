//
//  HeaderView.m
//  MDKP
//
//  Created by Андрей on 21.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "HeaderView.h"
#import <GSKGeometry.h>

static const CGFloat kAnimationDuration = 0.6;

@implementation HeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
        self.contentView.backgroundColor = colorGreen;
        [self setupSubviews];
        [self setSearchViewMode:HeaderViewModeFull animated:NO];
    }
    return self;
}

- (void)setupSubviews {
    UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    
    self.menuButton = [[UIButton alloc] init];
    [self.menuButton setImage:[UIImage imageNamed:@"menuIcon"] forState:UIControlStateNormal];
    [self.contentView addSubview:self.menuButton];
    
    self.filterButton = [[UIButton alloc] init];
    [self.filterButton setImage:[UIImage imageNamed:@"filterIcon"] forState:UIControlStateNormal];
    [self.contentView addSubview:self.filterButton];
    
    self.searchTextField = [[UITextField alloc] init];
    self.searchTextField.enabled = NO;
    self.searchTextField.textColor = colorGreen;
    [self addSubview:self.searchTextField];
    
    self.searchView = [[UIView alloc] init];
    self.backgroundColor = [UIColor clearColor];
    
    self.place = [[UIButton alloc] init];
    self.place.layer.cornerRadius = 15;
    self.place.backgroundColor = [UIColor whiteColor];
    [self.place setTitle:@"Выберите город" forState:UIControlStateNormal];
    [self.place setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    [self.searchView addSubview:self.place];
    
    self.countPeople = [[UIButton alloc] init];
    self.countPeople.layer.cornerRadius = 15;
    self.countPeople.backgroundColor = [UIColor whiteColor];
    [self.countPeople setTitle:@"1 гость" forState:UIControlStateNormal];
    [self.countPeople setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    [self.searchView addSubview:self.countPeople];
    
    self.searchButton = [[UIButton alloc] init];
    self.searchButton .backgroundColor = [UIColor greenColor];
    self.searchButton .layer.cornerRadius = 20.0f;
    [self.searchButton setTitle:@"Поиск" forState:UIControlStateNormal];
    [self.searchView addSubview:self.searchButton];
    
    [self addSubview:self.searchView];

  
    self.bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 1)];
    self.bottomLine.backgroundColor = [UIColor colorWithWhite:0.4 alpha:1];
    [self.contentView addSubview:self.bottomLine];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat widthButton = self.bounds.size.width - 40;
    
    self.menuButton.frame = CGRectMake(10, 20, 40, 40);
    
    self.filterButton.frame = CGRectMake(self.bounds.size.width - 50, 20, 40, 40);
    
    self.searchTextField.frame = CGRectMake(60, 20, self.bounds.size.width - 80, 40);
    
    self.searchView.frame = CGRectMake(20, 70, self.contentView.bounds.size.width - 40, 160);
    
    self.place.frame = CGRectMake(0, 0, widthButton, 40);
    
    self.countPeople.frame = CGRectMake(self.place.frame.origin.x, self.place.frame.origin.y + self.place.bounds.size.height + 10, widthButton, 40);
    
    self.searchButton.frame = CGRectMake(self.countPeople.frame.origin.x+10, self.countPeople.frame.origin.y + self.countPeople.bounds.size.height + 20, widthButton-20, 40);
}

- (void)didChangeStretchFactor:(CGFloat)stretchFactor {
    [super didChangeStretchFactor:stretchFactor];
    HeaderViewMode mode = self.contentView.bounds.size.height > self.maximumContentHeight-80 ? HeaderViewModeFull : HeaderViewModeShort;
    if (mode != self.mode) {
        [self setSearchViewMode:mode animated:YES];
    }
}

- (void)setSearchViewMode:(HeaderViewMode)mode animated:(BOOL)animated {
    _mode = mode;
    UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    [UIView animateWithDuration:animated ? kAnimationDuration : 0 animations:^{
        switch (mode) {
            case HeaderViewModeFull:
                self.searchView.alpha =  1;
                self.searchTextField.alpha = 0;
                self.filterButton.alpha = 1;
                self.contentView.backgroundColor = colorGreen;
                break;
                
            case HeaderViewModeShort:
                self.contentView.backgroundColor = [UIColor whiteColor];
                self.searchView.alpha =  0;
                self.searchTextField.alpha = 1;
                self.filterButton.alpha = 0;
                break;
        }
    }];
}

@end

