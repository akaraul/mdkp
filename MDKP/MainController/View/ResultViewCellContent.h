//
//  ResultViewCellContent.h
//  MDKP
//
//  Created by Андрей on 07.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewCellContent : UIView

@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIImageView *mainImage;

- (void)setItem:(id)item;

- (void)prepareForReuse;

@end
