//
//  ResultTableViewCell.m
//  MDKP
//
//  Created by Андрей on 07.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "ResultTableViewCell.h"

static CGFloat const ViewContentMargin = 8.0f;

@interface ResultTableViewCell ()

@property (nonatomic, assign) UIEdgeInsets contentInsets;

@end

@implementation ResultTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass(self);
}

- (instancetype)init {
    return [self initWithReuseIdentifier:[ResultTableViewCell reuseIdentifier]];
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    return [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.backgroundColor = [UIColor whiteColor];
        
        _contentInsets = UIEdgeInsetsMake(ViewContentMargin,
                                          ViewContentMargin,
                                          ViewContentMargin,
                                          ViewContentMargin);
        
        _cellContentView = [[ResultViewCellContent alloc] init];
        _cellContentView.layer.cornerRadius = 10.0f;
        _cellContentView.backgroundColor = [UIColor whiteColor];
        
        [self.contentView addSubview:_cellContentView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.cellContentView.frame = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
}

- (void)setItem:(id)item {
    self.tag = [[item valueForKey:@"adsId"] integerValue];
    [self.cellContentView setItem:item];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.cellContentView prepareForReuse];
}

@end
