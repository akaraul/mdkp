//
//  ResultTableViewCell.h
//  MDKP
//
//  Created by Андрей on 07.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultViewCellContent.h"

@interface ResultTableViewCell : UITableViewCell

@property (nonatomic, strong) ResultViewCellContent *cellContentView;

+ (NSString *)reuseIdentifier;

- (instancetype)init;
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;

- (void)setItem:(id)item;

@end
