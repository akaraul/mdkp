//
//  MainView.m
//  MDKP
//
//  Created by Андрей on 04.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "MainView.h"

@implementation MainView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    
    _priceView = [[UIView alloc] init];
    _priceView.backgroundColor = [UIColor whiteColor];
    
    _priceSlider = [[MARKRangeSlider alloc] initWithFrame:CGRectZero];
    [_priceSlider setMinValue:0 maxValue:100];
    [_priceSlider setLeftValue:0 rightValue:100];
    _priceSlider.minimumDistance = 30;
    [_priceView addSubview:_priceSlider];
    
    _priceLabel = [[UILabel alloc] init];
    _priceLabel.text = @"₽0 - ₽100";
    [_priceView addSubview:_priceLabel];
    
    _priceHeader = [[UILabel alloc] init];
    _priceHeader.text = @"Цена";
    [_priceHeader setFont:[UIFont boldSystemFontOfSize:20]];
    [_priceView addSubview:_priceHeader];
    
    _resTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _resTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    _resTable.showsVerticalScrollIndicator = NO;
    [self addSubview:_resTable];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _indicatorView.hidesWhenStopped = YES;
    _indicatorView.color = [UIColor blueColor];
    _indicatorView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    [self addSubview:_indicatorView];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    self.resTable.frame = self.bounds;
    self.indicatorView.center = self.center;
}

@end
