//
//  ResultViewCellContent.m
//  MDKP
//
//  Created by Андрей on 07.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "ResultViewCellContent.h"
#import <UIImageView+UIActivityIndicatorForSDWebImage.h>

@implementation ResultViewCellContent

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        _mainImage = [[UIImageView alloc] init];
        [self addSubview:_mainImage];
        
        _typeLabel = [[UILabel alloc] init];
        [self addSubview:_typeLabel];
        
        _priceLabel = [[UILabel alloc] init];
        [_priceLabel setFont:[UIFont boldSystemFontOfSize:16]];
        [self addSubview:_priceLabel];
        

    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    CGFloat width = self.bounds.size.width;
    
    self.mainImage.frame = CGRectMake(offset, offset, width-offset*2, 200.0f);
    self.priceLabel.frame = CGRectMake(offset,
                                      self.mainImage.frame.origin.y + self.mainImage.bounds.size.height + offset,
                                      width - offset*2,
                                     30.0f);
    self.typeLabel.frame = CGRectMake(offset,
                                      self.priceLabel.frame.origin.y + self.priceLabel.bounds.size.height,
                                      width - offset*2,
                                      20.0f);

}

- (void)setItem:(id)item {
    if ([[item valueForKey:@"active"] isEqual:@"0"]) self.alpha = 0.4f;
    NSArray *types = [NSArray arrayWithObjects:@"Жилье целиком",@"Отдельная комната",@"Общая комната",nil];
    NSString *imagePath = [NSString stringWithFormat:@"https://easyhomeapp.000webhostapp.com%@",[item valueForKey:@"photo"]];
    
    [self.mainImage setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageRefreshCached usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *nameAndPrice = [NSString stringWithFormat:@"%@ ₽ | %@",[item valueForKey:@"price"],[item valueForKey:@"name"]];
    self.priceLabel.text = [nameAndPrice uppercaseString];
    self.typeLabel.text = [types objectAtIndex:[[item valueForKey:@"type"] integerValue]-1];
    [self setNeedsLayout];
}

- (void)prepareForReuse {
    self.priceLabel.text = @"";
    self.typeLabel.text = @"";
    self.alpha = 1;
}



@end
