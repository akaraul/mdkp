//
//  HeaderView.h
//  MDKP
//
//  Created by Андрей on 21.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <GSKStretchyHeaderView.h>

typedef NS_ENUM(NSUInteger, HeaderViewMode) {
    HeaderViewModeFull,
    HeaderViewModeShort,
};

@interface HeaderView : GSKStretchyHeaderView

@property (nonatomic) UIButton *menuButton;
@property (nonatomic) UIButton *filterButton;
@property (nonatomic) UIView *searchView;
@property (nonatomic) UIView *bottomLine;
@property (nonatomic) UITextField *searchTextField;
@property (nonatomic) UIButton *place;
@property (nonatomic) UIButton *countPeople;
@property (nonatomic) UIButton *searchButton;

@property (nonatomic, readonly) HeaderViewMode mode;

@end

