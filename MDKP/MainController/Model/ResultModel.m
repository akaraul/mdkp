//
//  ResultModel.m
//  MDKP
//
//  Created by Андрей on 16.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "ResultModel.h"
#import "UIKit/UIKit.h"
#import "APIService.h"

@interface ResultModel ()

@property (nonatomic, strong) NSDictionary *adInfo;
@property (nonatomic, strong) NSMutableArray *searchResults;

@end

@implementation ResultModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _adInfo = [NSDictionary dictionary];
        _searchResults = [NSMutableArray array];
    }
    return self;
}

- (NSDictionary *)getAdInfoDictionary {
    return self.adInfo;
}

- (void)loadAdInfo:(NSInteger)adId withCompletion:(myCompletion)completion {
    [[APIService sharedService] getAdById:adId withComplition:^(BOOL success, NSData *data, NSError *error) {
        if (success) {
            _adInfo = [NSJSONSerialization JSONObjectWithData:data
                                                      options:0
                                                        error:&error];
            _adInfo = [_adInfo objectForKey:@"response"];
            completion(YES);
        }
        else completion(NO);
    }];
}

- (void)getSearchResults:(NSData *)data {
    NSDictionary *res = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"response"];
    for (NSDictionary * ad in res) {
        [self.searchResults addObject:ad];
    }
}

- (NSInteger)getCountAds {
    return self.searchResults.count;
}

- (NSDictionary *)getAdByIndexPath:(NSIndexPath *)indexPath {
    return [self.searchResults objectAtIndex:[indexPath row]];
}

- (void)clearData {
    [self.searchResults removeAllObjects];
}

@end
