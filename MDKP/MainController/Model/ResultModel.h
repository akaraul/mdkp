//
//  ResultModel.h
//  MDKP
//
//  Created by Андрей on 16.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^myCompletion)(BOOL);

@interface ResultModel : NSObject

- (void)loadAdInfo:(NSInteger)adId withCompletion:(myCompletion)completion;

- (NSDictionary *)getAdInfoDictionary;

- (void)clearData;
- (void)getSearchResults:(NSData *)data;
- (NSInteger)getCountAds;
- (NSDictionary *)getAdByIndexPath:(NSIndexPath *)indexPath;

@end
