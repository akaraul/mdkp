//
//  MyAdsModel.h
//  MDKP
//
//  Created by Андрей on 27.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^myCompletion)(BOOL);

@interface MyAdsModel : NSObject

-(void)getMyAds;

-(NSInteger)getCountMyAds;

-(void)deleteAd:(NSInteger)adId withComplition:(myCompletion)complition;
-(void)changeActiveAd:(NSInteger)adId active:(NSString *)active withComplition:(myCompletion)complition;

-(NSDictionary *)getMyAdsByIndexPath:(NSIndexPath *)indexPath;

@end
