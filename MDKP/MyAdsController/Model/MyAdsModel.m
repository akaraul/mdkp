//
//  MyAdsModel.m
//  MDKP
//
//  Created by Андрей on 27.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "MyAdsModel.h"
#import "CoreData/CoreData.h"
#import "AppDelegate.h"
#import "APIService.h"
#import "ADS+CoreDataClass.h"

@interface MyAdsModel ()

@property (nonatomic, strong) NSPersistentContainer *persistentContainer;
@property (nonatomic, strong) NSArray *myAds;

@end

@implementation MyAdsModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
        _myAds = [NSArray array];
    }
    return self;
}

-(void)deleteAd:(NSInteger)adId withComplition:(myCompletion)complition{
    [[APIService sharedService] deleteAd:adId withComplition:^(BOOL success, NSData *data, NSError *error) {
        if (success) {
            NSManagedObjectContext *context = self.persistentContainer.viewContext;
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"ADS"];
            fetchRequest.fetchLimit = 1;
            [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"adsId == %d",adId]];
            [context deleteObject:[[context executeFetchRequest:fetchRequest error:nil] firstObject]];
            [context save:nil];
        }
        complition(success);
    }];
}

-(void)changeActiveAd:(NSInteger)adId active:(NSString *)active withComplition:(myCompletion)complition{
    [[APIService sharedService] changeAdActive:adId active:active withComplition:^(BOOL success, NSData *data, NSError *error) {
        if (success) {
            NSManagedObjectContext *context = self.persistentContainer.viewContext;
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"ADS"];
            fetchRequest.fetchLimit = 1;
            [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"adsId == %d",adId]];
            ADS *update=[[context executeFetchRequest:fetchRequest error:nil] firstObject];
            [update setValue:active forKey:@"active"];
            [context save:nil];
        }
        complition(success);
    }];
}

-(void)getMyAds {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"ADS"];
    self.myAds = [context executeFetchRequest:request error:nil];
}

-(NSInteger)getCountMyAds {
    return self.myAds.count;
}

-(NSDictionary *)getMyAdsByIndexPath:(NSIndexPath *)indexPath {
    return [self.myAds objectAtIndex:[indexPath row]];
}

@end
