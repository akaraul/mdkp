//
//  MyAdsViewController.m
//  MDKP
//
//  Created by Андрей on 30.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "MyAdsViewController.h"
#import "MyAdsView.h"
#import "ResultTableViewCell.h"
#import "AppDelegate.h"
#import "MyAdsModel.h"

@interface MyAdsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) MyAdsView *myAdsView;
@property (nonatomic, strong) MyAdsModel *model;

@end

@implementation MyAdsViewController

- (void)loadView {
    self.myAdsView = [[MyAdsView alloc] init];
    self.view = self.myAdsView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[MyAdsModel alloc] init];
    [self.model getMyAds];
    
    self.myAdsView.resTable.delegate = self;
    self.myAdsView.resTable.dataSource = self;
    
    self.navigationItem.title = @"Мои объявления";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menuIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(openMenu:)];
}

- (void)openMenu:(UIBarButtonItem *)sender {
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

#pragma UITableviewDelegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.model getCountMyAds];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ResultTableViewCell reuseIdentifier]];
    if (!cell) {
        cell = [[ResultTableViewCell alloc] init];
    }
    [cell setItem:[self.model getMyAdsByIndexPath:indexPath]];
    return cell;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    ResultTableViewCell *currentCell = [tableView cellForRowAtIndexPath:indexPath];
    NSInteger adId = currentCell.tag;
    NSInteger active = 0;
    NSString *name = @"Сдано";
    if (currentCell.cellContentView.alpha == 0.4f){
        active = 1;
        name = @"Не сдано";
    }
    
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:name handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        [self.model changeActiveAd:adId active:[@(active) stringValue] withComplition:^(BOOL success) {
                                            UIAlertController * alert = [UIAlertController
                                                                         alertControllerWithTitle:nil
                                                                         message:nil
                                                                         preferredStyle:UIAlertControllerStyleAlert];

                                            if (success) {
                                                [tableView reloadData];
                                                [alert setTitle:@"Успешно!"];
                                                if (active == 0)
                                                [alert setMessage:@"Теперь объявление не видно в поиске!"];
                                                else [alert setMessage:@"Теперь объявление снова появится в поиске!"];
                                            }
                                            else {
                                                [alert setTitle:@"Ошибка!"];
                                                [alert setMessage:@"Проверьте подключение к сети!"];
                                            }
                                            UIAlertAction* okButton = [UIAlertAction
                                                                       actionWithTitle:@"Ok"
                                                                       style:UIAlertActionStyleDefault
                                                                       handler:nil];
                                            
                                            [alert addAction:okButton];
                                            [self presentViewController:alert animated:YES completion:nil];
                                        }];
                                    }];
    button.backgroundColor = [UIColor greenColor];
    
    UITableViewRowAction *button2 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Удалить" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                     {
                                         [self.model deleteAd:adId withComplition:^(BOOL success) {
                                             UIAlertController * alert = [UIAlertController
                                                                          alertControllerWithTitle:nil
                                                                          message:nil
                                                                          preferredStyle:UIAlertControllerStyleAlert];
                                             if (success) {
                                                 [alert setTitle:@"Успешно!"];
                                                 [alert setMessage:@"Объявление удалено!"];
                                                 [self.model getMyAds];
                                                 [tableView reloadData];
                                             }
                                             else {
                                                 [alert setTitle:@"Ошибка!"];
                                                 [alert setMessage:@"Проверьте подключение к сети!"];
                                             }
                                             UIAlertAction* okButton = [UIAlertAction
                                                                        actionWithTitle:@"Ok"
                                                                        style:UIAlertActionStyleDefault
                                                                        handler:nil];
                                             
                                             [alert addAction:okButton];
                                             [self presentViewController:alert animated:YES completion:nil];

                                         }];
                                     }];
    
    button2.backgroundColor = [UIColor redColor];
    return @[button, button2];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        //[self.dataArray removeObjectAtIndex:indexPath.row];
        [tableView reloadData]; // tell table to refresh now
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    DetailViewController *detailController = [[DetailViewController alloc] init];
//    [self.navigationController pushViewController:detailController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 300.0f;
}

@end
