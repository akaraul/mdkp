//
//  MyAdsView.m
//  MDKP
//
//  Created by Андрей on 30.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "MyAdsView.h"

@implementation MyAdsView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    
    _resTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _resTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    _resTable.allowsMultipleSelectionDuringEditing = NO;
    _resTable.allowsSelectionDuringEditing = NO;
    [self addSubview:_resTable];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.resTable.frame = self.bounds;
    
}


@end
