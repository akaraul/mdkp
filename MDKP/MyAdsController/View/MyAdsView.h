//
//  MyAdsView.h
//  MDKP
//
//  Created by Андрей on 30.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAdsView : UIView

@property (nonatomic, strong) UITableView *resTable;

@end
