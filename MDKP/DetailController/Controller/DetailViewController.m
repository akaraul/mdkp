//
//  DetailViewController.m
//  MDKP
//
//  Created by Андрей on 04.03.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "DetailViewController.h"
#import "DetailView.h"
#import "AppDelegate.h"
#import "DetailTableViewCell.h"
#import "UserInfoViewController.h"
#import "ImageCollectionViewCell.h"
#import "ComfortCollectionViewCell.h"
#import "MWPhotoBrowser.h"
#import "UINavigationBar+Awesome.h"
#import "DetailModel.h"
#import "ReviewViewController.h"
#import "AddReviewViewController.h"
#import "AppDelegate.h"
#import <UIImageView+UIActivityIndicatorForSDWebImage.h>

NSString* const PhotoCellRuseIdentifier = @"PhotoCellRuseIdentifier";
NSString* const ComfortCellRuseIdentifier = @"ComfortCellRuseIdentifier";

@interface DetailViewController () <UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource,UICollectionViewDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, MWPhotoBrowserDelegate>

@property (strong,nonatomic) DetailView *detailView;
@property (nonatomic, assign) BOOL isEndSlide;
@property (nonatomic, assign) BOOL noReviews;
@property (strong,nonatomic) NSTimer *timer;
@property (nonatomic,strong) NSMutableArray *bigPhoto;
@property (nonatomic,strong) NSMutableArray *photo;
@property (nonatomic, strong) DetailModel *model;

@end

@implementation DetailViewController

- (void)loadView {
    self.detailView = [[DetailView alloc] init];
    self.view = self.detailView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[SDImageCache sharedImageCache].shouldCacheImagesInMemory = NO;

    self.model = [[DetailModel alloc] init];
    [self setValuesForFields];

    self.detailView.reviewTable.delegate = self;
    self.detailView.reviewTable.dataSource = self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reviewIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(addReview:)];
    
    self.detailView.photos.dataSource = self;
    self.detailView.photos.delegate = self;
    [self.detailView.photos registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:PhotoCellRuseIdentifier];
    
    self.detailView.comforts.dataSource = self;
    self.detailView.comforts.delegate = self;
    [self.detailView.comforts registerClass:[ComfortCollectionViewCell class] forCellWithReuseIdentifier:ComfortCellRuseIdentifier];
    
    [self.detailView.ownerInfo addTarget:self action:@selector(getOwner:) forControlEvents:UIControlEventTouchUpInside];
    [self.detailView.watchAllReview addTarget:self action:@selector(getAllReviews:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.detailView.reviewTable addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];
    
    [self addTimer];
    
}

- (void)setValuesForFields {
    self.detailView.aboutHome.text = [self.adInfo valueForKeyPath:@"adInfo.about"];
    NSString *price = [NSString stringWithFormat:@"%@ ₽",[self.adInfo valueForKeyPath:@"adInfo.price"]];
    self.detailView.priceLabel.text= [self.detailView.priceLabel.text stringByAppendingString:price];
    self.detailView.typeLabel.text= [self.detailView.typeLabel.text stringByAppendingString:[self.model getTypeTitleByNumber:[[self.adInfo valueForKeyPath:@"adInfo.type"] integerValue]]];
    self.detailView.cityLabel.text= [self.detailView.cityLabel.text stringByAppendingString:[self.adInfo valueForKeyPath:@"adInfo.city"]];
    NSString *min = [NSString stringWithFormat:@"%@ день(-ней)",[self.adInfo valueForKeyPath:@"adInfo.minPeriod"]];
    self.detailView.minimumLabel.text= [self.detailView.minimumLabel.text stringByAppendingString:min];
    self.detailView.countPeople.text = [self.detailView.countPeople.text stringByAppendingString:[self.adInfo valueForKeyPath:@"adInfo.guests"]];
    self.detailView.countBed.text = [self.detailView.countBed.text stringByAppendingString:[self.adInfo valueForKeyPath:@"adInfo.beds"]];
    self.detailView.countBathroom.text = [self.detailView.countBathroom.text stringByAppendingString:[self.adInfo valueForKeyPath:@"adInfo.bathrooms"]];
    self.detailView.countRooms.text = [self.detailView.countRooms.text stringByAppendingString:[self.adInfo valueForKeyPath:@"adInfo.rooms"]];
    [self.detailView.ownerInfo setTitle:[NSString stringWithFormat:@"Хозяин: %@ %@",[self.adInfo valueForKeyPath:@"ownerInfo.name"],[self.adInfo valueForKeyPath:@"ownerInfo.surname"]] forState:UIControlStateNormal];
    
    self.photo = [self.model getPhotos:[self.adInfo valueForKey:@"photos"]];
    [self.model getComforts:[self.adInfo valueForKey:@"comforts"]];
    [self.model getReviews:[self.adInfo valueForKey:@"reviews"]];
    
    NSString *avatarPath = [NSString stringWithFormat:@"https://easyhomeapp.000webhostapp.com%@",[self.adInfo valueForKeyPath:@"ownerInfo.avatar_path"]];
    [self.detailView.ownerAvatar sd_setImageWithURL:[NSURL URLWithString:avatarPath] placeholderImage:nil options:SDWebImageCacheMemoryOnly];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.detailView.delegate = self;
    [self createToolBar];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBarHidden = NO;
}

-(void)createToolBar{
    [self.navigationController setToolbarHidden:NO animated:YES];
    CGFloat toolbarHeight = 60;
    CGRect rootViewBounds = self.parentViewController.view.bounds;
    CGFloat rootViewHeight = CGRectGetHeight(rootViewBounds);
    CGFloat rootViewWidth = CGRectGetWidth(rootViewBounds);
    CGRect rectArea = CGRectMake(0, rootViewHeight - toolbarHeight, rootViewWidth, toolbarHeight);
    [self.navigationController.toolbar setFrame:rectArea];
    CGFloat widthButton = rootViewWidth*0.7;
    UIButton *bookIt = [[UIButton alloc] initWithFrame:CGRectMake(0, 10, widthButton, 40)];
    [bookIt setTitle:@"Связаться с хозяином" forState:UIControlStateNormal];
    [bookIt addTarget:self action:@selector(getContacts:) forControlEvents:UIControlEventTouchUpInside];
    bookIt.backgroundColor = [UIColor redColor];
    bookIt.layer.cornerRadius = 10.0f;
    UIBarButtonItem *spaceButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:bookIt];
    self.toolbarItems = [NSArray arrayWithObjects:spaceButton,barButtonItem,spaceButton,nil];
}

- (void)getContacts:(UIButton *)sender {
    [self.detailView.indicatorView startAnimating];
    [self.model getContacts:[[self.adInfo valueForKeyPath:@"adInfo.ownerId"] integerValue] withCompletion:^{
        NSDictionary * contacts = [self.model getContactsDictionary];
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Контактные данные:"
                                     message:nil
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        NSString *phone = [NSString stringWithFormat:@"tel:%@",[contacts valueForKey:@"phone"]];
        UIAlertAction* phoneButton = [UIAlertAction
                                   actionWithTitle:[contacts valueForKey:@"phone"]
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone] options:@{} completionHandler:nil];
                                   }];
        
        NSString *mail = [NSString stringWithFormat:@"mailto:%@",[contacts valueForKey:@"login"]];
        UIAlertAction* emailButton = [UIAlertAction
                                      actionWithTitle:[contacts valueForKey:@"login"]
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mail] options:@{} completionHandler:nil];
                                      }];
        
        UIAlertAction* cancelButton = [UIAlertAction
                                      actionWithTitle:@"Закрыть"
                                      style:UIAlertActionStyleCancel
                                      handler:nil];
        
        [alert addAction:phoneButton];
        [alert addAction:emailButton];
        [alert addAction:cancelButton];
        [self presentViewController:alert animated:YES completion:nil];
        [self.detailView.indicatorView stopAnimating];
    }];
}

- (void)addReview:(UIButton *)sender {
    AddReviewViewController *addReviewController = [[AddReviewViewController alloc] init];
    addReviewController.adId = [[self.adInfo valueForKeyPath:@"adInfo.adsId"] integerValue];
    addReviewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:addReviewController animated:YES completion:nil];
}

- (void)getOwner:(UIButton *)sender {
    [self.detailView.indicatorView startAnimating];
    [self.model loadUserInfo:[[self.adInfo valueForKeyPath:@"adInfo.ownerId"] integerValue] withCompletion:^{
        UserInfoViewController *userInfoController = [[UserInfoViewController alloc] init];
        userInfoController.userInfo = [self.model getUserInfoDictionary];
        [self.navigationController pushViewController:userInfoController animated:YES];
        [self.detailView.indicatorView stopAnimating];
    }];

}

- (void)getAllReviews:(UIButton *)sender {
    [self.detailView.indicatorView startAnimating];
    [self.model getAllReviews:[[self.adInfo valueForKeyPath:@"adInfo.adsId"] integerValue]  withCompletion:^{
        ReviewViewController *reviewsController = [[ReviewViewController alloc] init];
        reviewsController.allReviews = [self.model getAllReviewsArray];
        [self.navigationController pushViewController:reviewsController animated:YES];
        [self.detailView.indicatorView stopAnimating];
    }];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.detailView.topOffset = self.topLayoutGuide.length;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (![scrollView isKindOfClass:[UICollectionView class]]) {
    UIColor * color = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    CGFloat offsetY = scrollView.contentOffset.y;
    CGFloat point = self.detailView.photos.frame.size.height - 64*2;
    if (offsetY > point) {
        CGFloat alpha = MIN(1, 1 - ((point + 64 - offsetY) / 64));
       [self.navigationController.navigationBar lt_setBackgroundColor:[color colorWithAlphaComponent:alpha]];
        self.navigationController.navigationBar.tintColor = [[UIColor whiteColor] colorWithAlphaComponent:alpha];
    } else {
        [self.navigationController.navigationBar lt_setBackgroundColor:[color colorWithAlphaComponent:0]];
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar lt_reset];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.detailView.delegate = nil;
    [self.navigationController setToolbarHidden:YES animated:YES];
}

#pragma AutoScroll image

- (void)nextPage {
    NSIndexPath *currentIndexPath = [[self.detailView.photos indexPathsForVisibleItems] lastObject];
    NSInteger nextItem;
    if (self.isEndSlide) nextItem = currentIndexPath.item - 1;
    else nextItem = currentIndexPath.item + 1;
    if (nextItem == self.photo.count || nextItem == -1) {
        self.isEndSlide = !self.isEndSlide;
        nextItem = nextItem == -1 ? 1 : self.photo.count-2;
    }
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:nextItem inSection:0];
    [self.detailView.photos scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
}
- (void)removeTimer {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)addTimer {
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(nextPage) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    self.timer = timer;
    self.isEndSlide = NO;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (scrollView == self.detailView.photos) [self removeTimer];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (scrollView == self.detailView.photos) [self addTimer];
}

#pragma UITableViewDelegate

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    CGRect frame = self.detailView.reviewTable.frame;
    frame.size = self.detailView.reviewTable.contentSize;
    self.detailView.reviewTable.frame = frame;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = [self.model getReviewsCount];
    if (count == 0) {self.noReviews = YES; return 1;}
    else return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.noReviews) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.textLabel.text = @"Отзывов пока нет :(";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        self.detailView.watchAllReview.hidden = YES;
        return cell;
    }
    DetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[DetailTableViewCell reuseIdentifier]];
    if (!cell) {
        cell = [[DetailTableViewCell alloc] init];
    }
    [cell.cellContentView setItem:[self.model getReviewByIndexPath:indexPath]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.noReviews) return 20;
    return 170.0f;
}

#pragma UICollectionViewDelegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(collectionView == self.detailView.photos) return self.photo.count;
    else return [self.model getComfortsCount];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    if(collectionView == self.detailView.photos)
    {
        ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PhotoCellRuseIdentifier forIndexPath:indexPath];
//        NSString *avatarPath = [NSString stringWithFormat:@"https://easyhomeapp.000webhostapp.com%@",[self.photo objectAtIndex:[indexPath item]]];
        [cell.imageView setImageWithURL:[self.photo objectAtIndex:[indexPath item]] placeholderImage:nil options:SDWebImageRefreshCached usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        //cell.imageView.image = [UIImage imageNamed:@"userAvatarIcon"];
                //cell.imageView.image = [self.photo objectAtIndex:[indexPath item]];
        return cell;
    }
    else
    {
        ComfortCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ComfortCellRuseIdentifier forIndexPath:indexPath];
        cell.imageView.image = [self.model getComfortImageByIndexPath:indexPath];
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if(collectionView != self.detailView.photos) return;
    self.bigPhoto = [NSMutableArray array];
    for (NSString * path in self.photo) {
        [self.bigPhoto addObject:[MWPhoto photoWithURL:[NSURL URLWithString:path]]];
    }
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = NO;
    browser.alwaysShowControls = NO;
    browser.enableGrid = NO;
    browser.enableSwipeToDismiss = YES;
    [browser setCurrentPhotoIndex:[indexPath item]];
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
 
    [self.navigationController pushViewController:browser animated:YES];
    //[self presentViewController:browser animated:YES completion:nil];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.bigPhoto.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.bigPhoto.count) {
        return [self.bigPhoto objectAtIndex:index];
    }
    return nil;
}

@end
