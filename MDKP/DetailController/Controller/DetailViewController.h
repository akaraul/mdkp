//
//  DetailViewController.h
//  MDKP
//
//  Created by Андрей on 04.03.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (nonatomic, strong) NSDictionary *adInfo;

@end
