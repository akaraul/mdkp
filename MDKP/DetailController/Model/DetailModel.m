//
//  DetailModel.m
//  MDKP
//
//  Created by Андрей on 16.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "DetailModel.h"
#import "APIService.h"

@interface DetailModel ()

@property (nonatomic, strong) NSDictionary *userInfo;
@property (nonatomic, strong) NSMutableArray *comforts;
@property (nonatomic, strong) NSMutableArray *reviews;
@property (nonatomic, strong) NSMutableArray *allReviews;
@property (nonatomic, strong) NSArray *typeValue;
@property (nonatomic, strong) NSMutableDictionary *contacts;

@end

@implementation DetailModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _userInfo = [NSDictionary dictionary];
        _comforts = [NSMutableArray array];
        _reviews = [NSMutableArray array];
        _allReviews = [NSMutableArray array];
        _contacts = [NSMutableDictionary dictionary];
        _typeValue = [NSArray arrayWithObjects:@"Не указано",@"Жилье целиком",@"Отдельная комната",@"Общая комната", nil];
    }
    return self;
}

- (NSString *)getTypeTitleByNumber:(NSInteger)number {
    return [self.typeValue objectAtIndex:number];
}

- (void)getReviews:(NSDictionary *)reviews {
    for (NSDictionary * review in reviews) {
        [self.reviews addObject:review];
    }
}

- (NSInteger)getReviewsCount {
    return self.reviews.count;
}

- (NSDictionary *)getReviewByIndexPath:(NSIndexPath *)indexPath {
    return [self.reviews objectAtIndex:[indexPath row]];
}

- (NSDictionary *)getUserInfoDictionary {
    return self.userInfo;
}

- (NSArray *)getAllReviewsArray {
    return  self.allReviews;
}

- (NSMutableArray *)getPhotos:(NSDictionary *)photos {
    NSMutableArray *photoArray = [NSMutableArray array];
    for (NSDictionary * photo in photos) {
        NSString *path = [NSString stringWithFormat:@"https://easyhomeapp.000webhostapp.com%@",[photo objectForKey:@"path_photo"]];
        //NSURL *url = [NSURL URLWithString:path];
        //NSData *data = [NSData dataWithContentsOfURL:url];
        //UIImage *img = [[UIImage alloc] initWithData:data];
        [photoArray addObject:path];
    }
    return photoArray;
}

- (void)getComforts:(NSDictionary *)comforts {
    for (NSDictionary * comfort in comforts) {
        [self.comforts addObject:[comfort objectForKey:@"comfortId"]];
    }
}

- (NSInteger)getComfortsCount {
    return self.comforts.count;
}

- (UIImage *)getComfortImageByIndexPath:(NSIndexPath *)indexPath {
    UIImage *icon;
    switch ([indexPath item]) {
        case 0:
            return icon = [UIImage imageNamed:@"wifiIcon"];
        case 1:
            return icon = [UIImage imageNamed:@"tvIcon"];
        case 2:
            return icon = [UIImage imageNamed:@"parkingIcon"];
        case 3:
            return icon = [UIImage imageNamed:@"nofireIcon"];
        case 4:
            return icon = [UIImage imageNamed:@"bathroomIcon"];
        case 5:
            return icon = [UIImage imageNamed:@"hairdryerIcon"];
        case 6:
            return icon = [UIImage imageNamed:@"ironIcon"];
        case 7:
            return icon = [UIImage imageNamed:@"conditionerIcon"];
        case 8:
            return icon = [UIImage imageNamed:@"radiatorIcon"];
        case 9:
            return icon = [UIImage imageNamed:@"washmachineIcon"];
        case 10:
            return icon = [UIImage imageNamed:@"firePlaceIcon"];
        case 11:
            return icon = [UIImage imageNamed:@"poolIcon"];
        case 12:
            return icon = [UIImage imageNamed:@"gymIcon"];
        case 13:
            return icon = [UIImage imageNamed:@"elevatorIcon"];
        case 14:
            return icon = [UIImage imageNamed:@"airmachineIcon"];
        case 15:
            return icon = [UIImage imageNamed:@"ethernetIcon"];
        case 16:
            return icon = [UIImage imageNamed:@"hdtvIcon"];
        case 17:
            return icon = [UIImage imageNamed:@"kitchenIcon"];
        case 18:
            return icon = [UIImage imageNamed:@"jacuzziIcon"];
    }
    return nil;
}

- (NSDictionary *)getContactsDictionary {
    return  self.contacts;
}

- (void)loadUserInfo:(NSInteger)userId withCompletion:(void (^)())completion {
    [[APIService sharedService] getUserInfoById:userId withComplition:^(BOOL success, NSData *data, NSError *error) {
        if (success) {
            _userInfo = [NSJSONSerialization JSONObjectWithData:data
                                                        options:0
                                                          error:&error];
            _userInfo = [_userInfo objectForKey:@"response"];
        }
        if (completion) {
            completion();
        }
    }];
}

- (void)getAllReviews:(NSInteger)adId withCompletion:(void (^)())completion {
    [[APIService sharedService] getAllReviews:adId withComplition:^(BOOL success, NSData *data, NSError *error) {
        if (success) {
            _allReviews = [[NSJSONSerialization JSONObjectWithData:data
                                                        options:0
                                                          error:&error] objectForKey:@"response"];
        }
        if (completion) {
            completion();
        }
    }];
}

- (void)getContacts:(NSInteger)userId withCompletion:(void (^)())completion {
    [[APIService sharedService] getOwnerContact:userId withComplition:^(BOOL success, NSData *data, NSError *error) {
        if (success) {
            _contacts = [[NSJSONSerialization JSONObjectWithData:data
                                                           options:0
                                                             error:&error] objectForKey:@"response"];
        }
        if (completion) {
            completion();
        }
    }];
}

@end
