//
//  DetailModel.h
//  MDKP
//
//  Created by Андрей on 16.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

typedef void(^myCompletion)(BOOL);

@interface DetailModel : NSObject

- (void)loadUserInfo:(NSInteger)userId withCompletion:(void (^)())completion;
- (void)getContacts:(NSInteger)userId withCompletion:(void (^)())completion;
- (void)getAllReviews:(NSInteger)adId withCompletion:(void (^)())completion;

- (NSDictionary *)getUserInfoDictionary;
- (NSDictionary *)getContactsDictionary;
- (NSArray *)getAllReviewsArray;

- (NSString *)getTypeTitleByNumber:(NSInteger)number;
- (NSMutableArray *)getPhotos:(NSDictionary *)photos;
- (void)getComforts:(NSDictionary *)comforts;
- (void)getReviews:(NSDictionary *)reviews;
- (NSDictionary *)getReviewByIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)getReviewsCount;
- (NSInteger)getComfortsCount;
- (UIImage *)getComfortImageByIndexPath:(NSIndexPath *)indexPath;

@end
