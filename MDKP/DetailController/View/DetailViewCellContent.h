//
//  DetailViewCellContent.h
//  MDKP
//
//  Created by Андрей on 10.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewCellContent : UIView

@property (nonatomic,strong) UIImageView *userAvatar;
@property (nonatomic,strong) UILabel *userName;
@property (nonatomic,strong) UILabel *textReview;
@property (nonatomic,strong) UILabel *dateReview;

- (void)setItem:(id)item;

- (void)prepareForReuse;

@end
