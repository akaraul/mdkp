//
//  DetailViewCellContent.m
//  MDKP
//
//  Created by Андрей on 10.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "DetailViewCellContent.h"
//#import <SDWebImage/UIImageView+WebCache.h>
#import <UIImageView+UIActivityIndicatorForSDWebImage.h>

@implementation DetailViewCellContent

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        _userAvatar = [[UIImageView alloc] init];
        _userAvatar.layer.cornerRadius = 30.0f;
        _userAvatar.backgroundColor = [UIColor grayColor];
        _userAvatar.layer.masksToBounds = YES;
        [self addSubview:_userAvatar];
        
        _userName = [[UILabel alloc] init];
        _userName.text = @"Имя пользователя";
        [self addSubview:_userName];
        
        _textReview = [[UILabel alloc] init];
        _textReview.numberOfLines = 0;
        [self addSubview:_textReview];
        
        _dateReview = [[UILabel alloc] init];
        [self addSubview:_dateReview];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    CGFloat width = self.bounds.size.width;
    
    self.userAvatar.frame = CGRectMake(offset, offset, 60.0f, 60.0f);
    self.userName.frame = CGRectMake(offset + 70.0f, 0 , width - 80.0f - offset, 40.0f);
    self.dateReview.frame = CGRectMake(self.userName.frame.origin.x,
                                       self.userName.frame.origin.y + 40.0f + offset,
                                       width - 80.0f - offset,
                                       20.0f);
    self.textReview.frame = CGRectMake(offset,
                                       self.userAvatar.frame.origin.y + 60.0f + offset,
                                       width - offset*2,
                                       [self getLabelHeight:self.textReview]);
}

- (void)setItem:(id)item {
    NSString *imagePath = [NSString stringWithFormat:@"https://easyhomeapp.000webhostapp.com%@",[item valueForKeyPath:@"authorInfo.avatar_path"]];
    [self.userAvatar sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageCacheMemoryOnly];
    self.dateReview.text = [item valueForKey:@"date"];
    self.textReview.text = [item valueForKey:@"text"];
    NSString *surname = [item valueForKeyPath:@"authorInfo.surname"];
    if (!surname) surname = @"";
    self.userName.text = [NSString stringWithFormat:@"%@ %@",[item valueForKeyPath:@"authorInfo.name"], surname];
    [self setNeedsLayout];
}

- (void)prepareForReuse {
    
}

- (CGFloat)getLabelHeight:(UILabel*)label {
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    return size.height;
}

@end
