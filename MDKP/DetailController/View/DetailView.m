//
//  DetailView.m
//  MDKP
//
//  Created by Андрей on 04.03.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "DetailView.h"

@implementation DetailView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    
    self.showsVerticalScrollIndicator = NO;
    
    _flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [_flowLayout  setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    _flowLayout.minimumLineSpacing = 0;
    
    _photos = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_flowLayout];
    _photos.backgroundColor = [UIColor grayColor];
    [_photos setPagingEnabled:YES];
    [_photos setShowsHorizontalScrollIndicator:NO];
    [self addSubview:_photos];
    
    _flowComfortLayout = [[UICollectionViewFlowLayout alloc] init];
    [_flowComfortLayout  setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    _flowComfortLayout.sectionInset = UIEdgeInsetsMake(0, 15.0f, 0, 15.0f);
    
    _comforts = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_flowComfortLayout];
    _comforts.backgroundColor = [UIColor whiteColor];
    [_comforts setShowsHorizontalScrollIndicator:NO];
    [self addSubview:_comforts];
    
    _reviewLabel = [[UILabel alloc] init];
    [_reviewLabel setFont:[UIFont boldSystemFontOfSize:20]];
    _reviewLabel.text = @"Последние отзывы";
    [self addSubview:_reviewLabel];
    
    _reviewTable = [[UITableView alloc] init];
    _reviewTable.backgroundColor = [UIColor whiteColor];
    _reviewTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_reviewTable];
    
    _ownerAvatar = [[UIImageView alloc] init];
    _ownerAvatar.backgroundColor = [UIColor grayColor];
    _ownerAvatar.layer.cornerRadius = 30.0f;
    _ownerAvatar.layer.masksToBounds = YES;
    
    _ownerInfo = [[UIButton alloc] init];
    _ownerInfo.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_ownerInfo setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_ownerInfo setTitle:@"Хозяин Andrey Karaulov" forState:UIControlStateNormal];
    [_ownerInfo addSubview:_ownerAvatar];
    [self addSubview:_ownerInfo];
    
    _aboutHomeLabel = [[UILabel alloc] init];
    [_aboutHomeLabel setFont:[UIFont boldSystemFontOfSize:20]];
    _aboutHomeLabel.text = @"Описание";
    [self addSubview:_aboutHomeLabel];
    
    _aboutHome = [[UILabel alloc] init];
    _aboutHome.numberOfLines = 0;
    [self addSubview:_aboutHome];
    
    _countView = [[UIView alloc] init];
    
    _countPeople = [[UILabel alloc] init];
    _countPeople.text = @"Гостей ";
    _countPeople.font=[_countPeople.font fontWithSize:14];
    [_countView addSubview:_countPeople];
    
    _imagePeople = [[UIImageView alloc] init];
    [_imagePeople setImage:[UIImage imageNamed:@"peopleIcon"]];
    [_countView addSubview:_imagePeople];
    
    _countRooms = [[UILabel alloc] init];
    _countRooms.text = @"Комнат ";
    _countRooms.font=[_countRooms.font fontWithSize:14];
    [_countView addSubview:_countRooms];
    
    _imageRoom = [[UIImageView alloc] init];
    [_imageRoom setImage:[UIImage imageNamed:@"roomIcon"]];
    [_countView addSubview:_imageRoom];
    
    _countBed = [[UILabel alloc] init];
    _countBed.text = @"Кровати ";
    _countBed.font=[_countBed.font fontWithSize:14];
    [_countView addSubview:_countBed];
    
    _imageBed = [[UIImageView alloc] init];
    [_imageBed setImage:[UIImage imageNamed:@"bedIcon"]];
    [_countView addSubview:_imageBed];
    
    _countBathroom = [[UILabel alloc] init];
    _countBathroom.text = @"Ванная ";
    _countBathroom.font=[_countBathroom.font fontWithSize:14];
    [_countView addSubview:_countBathroom];
    
    _imageBathroom = [[UIImageView alloc] init];
    [_imageBathroom setImage:[UIImage imageNamed:@"bathroomIcon"]];
    [_countView addSubview:_imageBathroom];
    [self addSubview:_countView];
    
    _minimumLabel = [[UILabel alloc] init];
    _minimumLabel.text = @"Мин. срок сдачи: ";
    [self addSubview:_minimumLabel];
    
    _comfortLabel = [[UILabel alloc] init];
    _comfortLabel.text = @"Удобства";
    [_comfortLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [self addSubview:_comfortLabel];
    
    _watchAllReview = [[UIButton alloc] init];
    [_watchAllReview setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_watchAllReview setTitle:@"Посмотреть все отзывы" forState:UIControlStateNormal];
    [self addSubview:_watchAllReview];
    
    _priceLabel = [[UILabel alloc] init];
    _priceLabel.text = @"Цена за сутки: ";
    [self addSubview:_priceLabel];
    
    _typeLabel = [[UILabel alloc] init];
    _typeLabel.text = @"Тип: ";
    [self addSubview:_typeLabel];
    
    _cityLabel = [[UILabel alloc] init];
    _cityLabel.text = @"Город: ";
    [self addSubview:_cityLabel];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _indicatorView.hidesWhenStopped = YES;
    _indicatorView.color = [UIColor blueColor];
    _indicatorView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    [self addSubview:_indicatorView];
    
    [self addTopLayerToTheView:self.comfortLabel];
    [self addTopLayerToTheView:self.aboutHomeLabel];
    [self addTopLayerToTheView:self.countView];
    [self addTopLayerToTheView:self.reviewLabel];
    [self addTopLayerToTheView:self.priceLabel];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat offset = 10.0f;
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGFloat heightLabel = 45.0f;
    
    self.indicatorView.center = self.center;
    
    self.flowLayout.itemSize = CGSizeMake(width, height/2.4f);
    
    self.photos.frame = CGRectMake(0, 0, width, height/2.4f);
    
    self.ownerInfo.frame = CGRectMake(offset,
                                      self.photos.frame.origin.y + self.photos.bounds.size.height,
                                      width-offset*2,
                                      65.0f);
    
    self.ownerAvatar.frame = CGRectMake(self.ownerInfo.bounds.size.width - 60.0f,
                                        5.0f,
                                        60.0f,
                                        60.0f);
    
    self.priceLabel.frame = CGRectMake(offset,
                                         self.ownerInfo.bounds.size.height + self.ownerInfo.frame.origin.y + offset,
                                         self.ownerInfo.bounds.size.width,
                                         30);
    
    self.typeLabel.frame = CGRectMake(offset,
                                         self.priceLabel.bounds.size.height + self.priceLabel.frame.origin.y,
                                         self.ownerInfo.bounds.size.width,
                                         30);
    
    self.cityLabel.frame = CGRectMake(offset,
                                         self.typeLabel.bounds.size.height + self.typeLabel.frame.origin.y,
                                         self.ownerInfo.bounds.size.width,
                                         30);
    
    self.minimumLabel.frame = CGRectMake(offset,
                                         self.cityLabel.bounds.size.height + self.cityLabel.frame.origin.y,
                                         self.ownerInfo.bounds.size.width,
                                         30);
    
    self.countView.frame = CGRectMake(offset,
                                      self.minimumLabel.bounds.size.height + self.minimumLabel.frame.origin.y,
                                      self.minimumLabel.bounds.size.width,
                                      70.0f);
    
    CGFloat iconSize = 40.0f;
    CGFloat offsetIcon = (self.countView.bounds.size.width - iconSize*4)/5.0f;
    
    self.imagePeople.frame = CGRectMake(offsetIcon, offset, iconSize, iconSize);
    self.countPeople.frame = CGRectMake(self.imagePeople.frame.origin.x - 12.0f,
                                        self.imagePeople.frame.origin.y + iconSize,
                                        64.0f, 30.0f);
    
    self.imageRoom.frame = CGRectMake(self.imagePeople.frame.origin.x + iconSize + offsetIcon, offset, iconSize, iconSize);
    self.countRooms.frame = CGRectMake(self.imageRoom.frame.origin.x - 17.0f,
                                        self.imageRoom.frame.origin.y + iconSize,
                                        74.0f, 30.0f);
    
    self.imageBed.frame = CGRectMake(self.imageRoom.frame.origin.x + iconSize + offsetIcon, offset, iconSize, iconSize);
    self.countBed.frame = CGRectMake(self.imageBed.frame.origin.x - 17.0f,
                                       self.imageBed.frame.origin.y + iconSize,
                                       74.0f, 30.0f);
    
    self.imageBathroom.frame = CGRectMake(self.imageBed.frame.origin.x + iconSize + offsetIcon, offset, iconSize, iconSize);
    self.countBathroom.frame = CGRectMake(self.imageBathroom.frame.origin.x - 12.0f,
                                     self.imageBathroom.frame.origin.y + iconSize,
                                     64.0f, 30.0f);
    
    self.aboutHomeLabel.frame = CGRectMake(offset,
                                           self.countView.bounds.size.height + self.countView.frame.origin.y + offset,
                                           self.ownerInfo.bounds.size.width,
                                           heightLabel);
    
    self.aboutHome.frame = CGRectMake(offset,
                                      self.aboutHomeLabel.bounds.size.height + self.aboutHomeLabel.frame.origin.y,
                                      self.ownerInfo.bounds.size.width,
                                      [self getLabelHeight:self.aboutHome]);
    
    self.comfortLabel.frame = CGRectMake(offset,
                                         self.aboutHome.bounds.size.height + self.aboutHome.frame.origin.y + offset,
                                         self.self.ownerInfo.bounds.size.width,
                                         heightLabel);
    
    self.flowComfortLayout.itemSize = CGSizeMake(50.0f, 50.0f);
    self.comforts.frame = CGRectMake(offset,
                                     self.comfortLabel.bounds.size.height + self.comfortLabel.frame.origin.y,
                                     self.self.ownerInfo.bounds.size.width,
                                     60.0f);
    
    self.reviewLabel.frame = CGRectMake(offset,
                                        self.comforts.frame.origin.y + self.comforts.bounds.size.height + offset,
                                        self.ownerInfo.bounds.size.width,
                                        heightLabel);
    
    self.reviewTable.frame = CGRectMake(offset,
                                        self.reviewLabel.frame.origin.y + self.reviewLabel.bounds.size.height + offset,
                                        self.ownerInfo.bounds.size.width,
                                        510.0f);
    
    self.watchAllReview.frame = CGRectMake(offset,
                                           self.reviewTable.frame.origin.y + self.reviewTable.bounds.size.height + offset,
                                           self.ownerInfo.bounds.size.width,
                                           30.0f);
    
    self.contentInset = UIEdgeInsetsMake(0,0, 80.0f + self.watchAllReview.bounds.size.height ,0);
    self.contentSize = CGSizeMake(self.bounds.size.width, self.watchAllReview.frame.origin.y);
}

- (void)addTopLayerToTheView:(UIView *)view {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(0.0, 0.0, screenRect.size.width - 20.0f, 1.0);
    topBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                     alpha:1.0f].CGColor;
    [view.layer addSublayer:topBorder];
}

- (CGFloat)getLabelHeight:(UILabel*)label {
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    return size.height;
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end
