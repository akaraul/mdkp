//
//  DetailView.h
//  MDKP
//
//  Created by Андрей on 04.03.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface DetailView : UIScrollView

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property(nonatomic,strong) UICollectionView *photos;
@property(nonatomic,strong) UICollectionView *comforts;
@property(nonatomic,strong) UICollectionViewFlowLayout *flowLayout;
@property(nonatomic,strong) UICollectionViewFlowLayout *flowComfortLayout;

@property (nonatomic, strong) UIButton *ownerInfo;
@property (nonatomic,strong) UIImageView *ownerAvatar;

@property (nonatomic,strong) UILabel *aboutHomeLabel;
@property (nonatomic,strong) UILabel *aboutHome;

@property (nonatomic,strong) UILabel *reviewLabel;
@property (nonatomic,strong) UITableView *reviewTable;
@property (nonatomic,strong) UIButton *watchAllReview;

@property (nonatomic, strong) UILabel *minimumLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UILabel *cityLabel;
@property (nonatomic, strong) UILabel *comfortLabel;

@property (nonatomic, strong) UIView *countView;
@property (nonatomic, strong) UILabel *countPeople;
@property (nonatomic, strong) UIImageView *imagePeople;
@property (nonatomic, strong) UILabel *countRooms;
@property (nonatomic, strong) UIImageView *imageRoom;
@property (nonatomic, strong) UILabel *countBed;
@property (nonatomic, strong) UIImageView *imageBed;
@property (nonatomic, strong) UILabel *countBathroom;
@property (nonatomic, strong) UIImageView *imageBathroom;

@property (nonatomic, assign) CGFloat topOffset;
@end
