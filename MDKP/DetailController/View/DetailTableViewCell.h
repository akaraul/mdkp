//
//  DetailTableViewCell.h
//  MDKP
//
//  Created by Андрей on 10.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewCellContent.h"

@interface DetailTableViewCell : UITableViewCell

@property (nonatomic, strong) DetailViewCellContent *cellContentView;

+ (NSString *)reuseIdentifier;

- (instancetype)init;
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;

- (void)setItem:(id)item;

@end
