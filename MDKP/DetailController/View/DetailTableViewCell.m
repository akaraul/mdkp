//
//  DetailTableViewCell.m
//  MDKP
//
//  Created by Андрей on 10.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "DetailTableViewCell.h"

@interface DetailTableViewCell ()

@property (nonatomic, assign) UIEdgeInsets contentInsets;

@end

@implementation DetailTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass(self);
}

- (instancetype)init {
    return [self initWithReuseIdentifier:[DetailTableViewCell reuseIdentifier]];
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    return [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.backgroundColor = [UIColor whiteColor];
        
        _contentInsets = UIEdgeInsetsMake(0,0,8.0f,0);
        
        _cellContentView = [[DetailViewCellContent alloc] init];
        _cellContentView.layer.cornerRadius = 10.0f;
        _cellContentView.backgroundColor = [UIColor colorWithWhite:0.96 alpha:1];
        
        [self.contentView addSubview:_cellContentView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.cellContentView.frame = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
}

- (void)setItem:(id)item {
    [self.cellContentView setItem:item];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.cellContentView prepareForReuse];
}


@end
