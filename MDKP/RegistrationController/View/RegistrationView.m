//
//  RegistrationView.m
//  MDKP
//
//  Created by Андрей on 04.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "RegistrationView.h"

@implementation RegistrationView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    self.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1];
    
    _regLabel = [[UILabel alloc] init];
    _regLabel.text = @"Регистрация";
    _regLabel.textAlignment = NSTextAlignmentCenter;
    _regLabel.font=[_regLabel.font fontWithSize:30];
    [self addSubview:_regLabel];
    
    _infoLabel = [[UILabel alloc] init];
    _infoLabel.numberOfLines = 2;
    _infoLabel.textAlignment = NSTextAlignmentCenter;
    _infoLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _infoLabel.text = @"Для регистрации заполните следующие поля:";
    [self addSubview:_infoLabel];
    
    _errorLabel = [[UILabel alloc] init];
    //_errorLabel.text = @"Ошибка ывпыпвгыпвгы";
    _errorLabel.textColor = [UIColor redColor];
    _errorLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_errorLabel];
    
    _closeButton = [[UIButton alloc] init];
    [_closeButton setImage:[UIImage imageNamed:@"closeIcon"] forState:UIControlStateNormal];
    [self addSubview:_closeButton];
    
    _regButton = [[UIButton alloc] init];
    _regButton.backgroundColor = colorGreen;
    _regButton.layer.cornerRadius = 10.0f;
    _regButton.tintColor = [UIColor whiteColor];
    [_regButton setTitle:@"Зарегистрироваться" forState:UIControlStateNormal];
    [self addSubview:_regButton];
    
    _nameTextField = [[UITextField alloc] init];
    _nameTextField.placeholder = @"Ваше имя";
    _nameTextField.borderStyle = UITextBorderStyleRoundedRect;
    _nameTextField.returnKeyType = UIReturnKeyNext;
    _nameTextField.tag = 0;
    [self addSubview:_nameTextField];
    
    _emailTextField = [[UITextField alloc] init];
    _emailTextField.placeholder = @"Ваш E-mail";
    _emailTextField.borderStyle = UITextBorderStyleRoundedRect;
    _emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    _emailTextField.returnKeyType = UIReturnKeyNext;
    _emailTextField.tag = 1;
    [self addSubview:_emailTextField];
    
    _passTextField = [[UITextField alloc] init];
    _passTextField.placeholder = @"Ваш пароль";
    _passTextField.borderStyle = UITextBorderStyleRoundedRect;
    _passTextField.secureTextEntry = YES;
    _passTextField.returnKeyType = UIReturnKeyNext;
    _passTextField.tag = 2;
    [self addSubview:_passTextField];
    
    _confirmPassTextField = [[UITextField alloc] init];
    _confirmPassTextField.placeholder = @"Повторите пароль";
    _confirmPassTextField.borderStyle = UITextBorderStyleRoundedRect;
    _confirmPassTextField.secureTextEntry = YES;
    _confirmPassTextField.returnKeyType = UIReturnKeyDone;
    _confirmPassTextField.tag = 3;
    [self addSubview:_confirmPassTextField];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _indicatorView.hidesWhenStopped = YES;
    _indicatorView.color = [UIColor blueColor];
    [self addSubview:_indicatorView];
    
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    CGFloat width = self.bounds.size.width;
    //CGFloat height = self.bounds.size.height;
    CGFloat heightElem = 40.0f;
    
    self.indicatorView.center = self.center;
    
    self.regLabel.frame = CGRectMake((width - 200.0f)/2, 30.0f, 200.0f, heightElem);
    
    self.infoLabel.frame = CGRectMake(offset,
                                      self.regLabel.bounds.size.height + self.regLabel.frame.origin.y + offset,
                                      width - offset*2,
                                      60.0f);
    
    self.nameTextField.frame = CGRectMake(offset*3,
                                          self.infoLabel.bounds.size.height + self.infoLabel.frame.origin.y + offset,
                                          width - offset*6,
                                          heightElem);
    
    self.emailTextField.frame = CGRectMake(self.nameTextField.frame.origin.x,
                                           self.nameTextField.frame.origin.y + self.nameTextField.bounds.size.height + offset,
                                           self.nameTextField.bounds.size.width,
                                           heightElem);
    
    self.passTextField.frame = CGRectMake(self.nameTextField.frame.origin.x,
                                          self.emailTextField.frame.origin.y + self.emailTextField.bounds.size.height + offset,
                                          self.emailTextField.bounds.size.width,
                                          heightElem);
    
    self.confirmPassTextField.frame = CGRectMake(self.nameTextField.frame.origin.x,
                                                 self.passTextField.frame.origin.y + self.passTextField.bounds.size.height + offset,
                                                 self.passTextField.bounds.size.width,
                                                 heightElem);
    
    self.errorLabel.frame = CGRectMake(self.nameTextField.frame.origin.x,
                                       self.confirmPassTextField.frame.origin.y + self.confirmPassTextField.bounds.size.height,
                                       self.passTextField.bounds.size.width,
                                       heightElem);

    self.regButton.frame = CGRectMake(self.nameTextField.frame.origin.x,
                                      self.errorLabel.frame.origin.y + self.errorLabel.bounds.size.height + offset,
                                      self.nameTextField.bounds.size.width,
                                      45.0f);
    
    self.closeButton.frame = CGRectMake(width - 40.0f - offset, 30.0f, 40.0f, 40.0f);
    
}


@end
