//
//  RegistrationView.h
//  MDKP
//
//  Created by Андрей on 04.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationView : UIView

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property (nonatomic, strong) UILabel *regLabel;
@property (nonatomic, strong) UILabel *infoLabel;
@property (nonatomic, strong) UILabel *errorLabel;
@property (nonatomic, strong) UITextField *nameTextField;
@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *passTextField;
@property (nonatomic, strong) UITextField *confirmPassTextField;
@property (nonatomic, strong) UIButton *regButton;
@property (nonatomic, strong) UIButton *closeButton;

@end
