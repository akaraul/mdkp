//
//  RegistrationViewController.m
//  MDKP
//
//  Created by Андрей on 04.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "RegistrationViewController.h"
#import "RegistrationView.h"
#import "APIService.h"
#import <CommonCrypto/CommonDigest.h>

@interface RegistrationViewController () <UITextFieldDelegate>

@property (nonatomic, strong) RegistrationView *regView;
@property (nonatomic, strong) NSMutableDictionary *regInfo;

@end

@implementation RegistrationViewController

- (void)loadView {
    self.regView = [[RegistrationView alloc] init];
    self.view = self.regView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.regInfo = [NSMutableDictionary dictionary];
    
    self.regView.nameTextField.delegate = self;
    self.regView.emailTextField.delegate = self;
    self.regView.passTextField.delegate = self;
    self.regView.confirmPassTextField.delegate = self;
    
    [self.regView.closeButton addTarget:self action:@selector(closeReg:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.regView.regButton addTarget:self action:@selector(registrationStart:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (BOOL)validation {
    NSString *errors = @"";
    
    if ([self.regView.nameTextField.text isEqualToString:@""]) errors = [errors stringByAppendingString:@"\n- Не указано имя"];
//    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//    if ([self.regView.emailTextField.text isEqualToString:@""]) errors = [errors stringByAppendingString:@"\n- Не указан e-mail"];
//    else if (![emailTest evaluateWithObject:self.regView.emailTextField.text]) errors = [errors stringByAppendingString:@"\n- Неверный формат e-mail"];
    
    if ([self.regView.passTextField.text isEqualToString:@""]) errors = [errors stringByAppendingString:@"\n- Пароль не может быть пустым"];
    else if (self.regView.passTextField.text != self.regView.confirmPassTextField.text) errors = [errors stringByAppendingString:@"\n- Пароли не совпадают"];

    
    if (![errors isEqualToString:@""])
    {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Ошибка!"
                                     message:errors
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                   }];
        
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        return false;
    }
    return true;
}

- (void)registrationStart:(UIButton *)sender {
    if ([self validation]) {
        [self.regInfo setValue:self.regView.nameTextField.text forKey:@"name"];
        [self.regInfo setValue:self.regView.emailTextField.text forKey:@"login"];
        [self.regInfo setValue:[self md5:self.regView.passTextField.text] forKey:@"password"];
        [self.regView.indicatorView startAnimating];
        [[APIService sharedService] regUser:self.regInfo withComplition:^(BOOL success, NSError *error) {
            if (success) {
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Регистрация успешно завершена!"
                                             message:@"Теперь вы можете войти под своей учетной записью!"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:@"Ok"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                            [self dismissViewControllerAnimated:YES completion:nil];
                                           }];
                
                [alert addAction:okButton];
                [self.regView.indicatorView stopAnimating];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }];
    }
}

- (void)closeReg:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (NSString *)md5:(NSString *)password {
    const char *ptr = [password UTF8String];
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(ptr, (CC_LONG)strlen(ptr), md5Buffer);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType==UIReturnKeyNext) {
        UIView *next = [[textField superview] viewWithTag:textField.tag+1];
        [next becomeFirstResponder];
    } else if (textField.returnKeyType==UIReturnKeyDone) {
        [textField resignFirstResponder];
    }
    return YES;
}

@end
