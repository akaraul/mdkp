//
//  ChangePasswordViewController.m
//  MDKP
//
//  Created by Андрей on 11.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "ChangePasswordView.h"
#import "ChangePasswordModel.h"

@interface ChangePasswordViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) ChangePasswordView *changePassView;
@property (nonatomic, strong) ChangePasswordModel *model;

@end

@implementation ChangePasswordViewController

- (void)loadView {
    self.changePassView = [[ChangePasswordView alloc] init];
    self.view = self.changePassView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[ChangePasswordModel alloc] init];
    
    self.navigationItem.title = @"Изменить пароль";
    
    self.changePassView.passTable.delegate = self;
    self.changePassView.passTable.dataSource = self;
    
    self.changePassView.oldPassTextField.delegate = self;
    self.changePassView.passTextFieldNew.delegate = self;
    
    [self.changePassView.saveChangesButton addTarget:self action:@selector(changePass:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)changePass:(UIButton *)sender {
    [self.changePassView.indicatorView startAnimating];
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Пароль успешно изменен!"
                                 message:@"При следующем входе используйте новый пароль!"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    if ([self.model checkCurrentPassword:self.changePassView.oldPassTextField.text]) {
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
        [userInfo setValue:[@(self.userId) stringValue] forKey:@"userId"];
        [userInfo setValue:self.changePassView.passTextFieldNew.text forKey:@"password"];
        [self.model changePassword:userInfo withCompletion:^{
        }];
    }
    else {
        [alert setTitle:@"Ошибка!"];
        [alert setMessage:@"Текущий пароль введен неверно!"];
    }
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    
    [alert addAction:okButton];
    [self.changePassView.indicatorView stopAnimating];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType==UIReturnKeyNext) {
        UIView *next = [self.changePassView viewWithTag:textField.tag+1];
        [next becomeFirstResponder];
    } else if (textField.returnKeyType==UIReturnKeyDone) {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma UITableviewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) return 2;
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PassChange"];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    if ([indexPath section]==0) {
        switch ([indexPath row]) {
            case 0:
                cell.textLabel.text = @"Текущий пароль";
                [cell.contentView addSubview:self.changePassView.oldPassTextField];
                break;
            case 1:
                cell.textLabel.text = @"Новый пароль";
                [cell.contentView addSubview:self.changePassView.passTextFieldNew];
                break;
                
        }
    }
    else {
        cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell.contentView addSubview:self.changePassView.saveChangesButton];
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 15;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}


@end
