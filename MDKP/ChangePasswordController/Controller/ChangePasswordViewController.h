//
//  ChangePasswordViewController.h
//  MDKP
//
//  Created by Андрей on 11.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController

@property (nonatomic, assign) NSInteger userId;

@end
