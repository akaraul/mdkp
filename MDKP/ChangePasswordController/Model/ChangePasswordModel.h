//
//  ChangePasswordModel.h
//  MDKP
//
//  Created by Андрей on 24.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChangePasswordModel : NSObject

- (void)changePassword:(NSDictionary *)userInfo withCompletion:(void (^)())completion;

- (BOOL)checkCurrentPassword:(NSString *)password;

@end
