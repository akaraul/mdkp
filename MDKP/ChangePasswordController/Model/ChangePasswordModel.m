//
//  ChangePasswordModel.m
//  MDKP
//
//  Created by Андрей on 24.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "ChangePasswordModel.h"
#import "APIService.h"
#import "AppDelegate.h"
#import <CommonCrypto/CommonDigest.h>
#import "USERINFO+CoreDataClass.h"

@interface ChangePasswordModel ()

@property (nonatomic, strong) NSPersistentContainer *persistentContainer;

@end

@implementation ChangePasswordModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}

- (void)changePassword:(NSDictionary *)userInfo withCompletion:(void (^)())completion {
    [userInfo setValue:[self md5:[userInfo valueForKey:@"password"]] forKey:@"password"];
    [[APIService sharedService] changeUserPassword:userInfo withComplition:^(BOOL success, NSError *error) {
        if (success) {
            NSManagedObjectContext *context = self.persistentContainer.viewContext;
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"USERINFO"];
            USERINFO *update=[[context executeFetchRequest:fetchRequest error:nil] lastObject];
            [update setValue:[userInfo valueForKey:@"password"] forKey:@"password"];
            [context save:nil];
            completion();
        }
    }];
}

- (BOOL)checkCurrentPassword:(NSString *)password {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"USERINFO"];
    NSArray *res = [context executeFetchRequest:request error:nil];
    NSString *currentPassword = [[res firstObject] valueForKey:@"password"];
    if ([currentPassword isEqual:[self md5:password]]) return YES;
    return NO;
}

- (NSString *)md5:(NSString *)password {
    const char *ptr = [password UTF8String];
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(ptr, (CC_LONG)strlen(ptr), md5Buffer);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}


@end
