//
//  ChangePasswordView.m
//  MDKP
//
//  Created by Андрей on 11.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "ChangePasswordView.h"

@implementation ChangePasswordView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
   UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    self.backgroundColor = [UIColor whiteColor];
    
    _saveChangesButton = [[UIButton alloc] init];
    _saveChangesButton.backgroundColor = colorGreen;
    _saveChangesButton.layer.cornerRadius = 10.0f;
    [_saveChangesButton setTitle:@"Изменить пароль" forState:UIControlStateNormal];
    
    _oldPassTextField = [[UITextField alloc] init];
    _oldPassTextField.tag = 0;
    _oldPassTextField.returnKeyType = UIReturnKeyNext;
    _oldPassTextField.secureTextEntry = YES;
    
    _passTextFieldNew = [[UITextField alloc] init];
    _passTextFieldNew.tag = 1;
    _passTextFieldNew.returnKeyType = UIReturnKeyDone;
    _passTextFieldNew.secureTextEntry = YES;
    
    _passTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _passTable.scrollEnabled = NO;
    [self addSubview:_passTable];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _indicatorView.hidesWhenStopped = YES;
    _indicatorView.color = [UIColor blueColor];
    [self addSubview:_indicatorView];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat width = self.bounds.size.width;
    
    self.passTable.frame = self.bounds;
    self.indicatorView.center = self.center;
    
    CGRect sizeTextField = CGRectMake(160, 0, width - 160, 50);
    self.saveChangesButton.frame = CGRectMake(10, 0, width - 20, 50);
    self.oldPassTextField.frame = sizeTextField;
    self.passTextFieldNew.frame = sizeTextField;
}


@end
