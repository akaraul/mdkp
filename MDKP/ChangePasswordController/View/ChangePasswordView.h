//
//  ChangePasswordView.h
//  MDKP
//
//  Created by Андрей on 11.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordView : UIView

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property (nonatomic, strong) UITableView *passTable;
@property (nonatomic, strong) UITextField *oldPassTextField;
@property (nonatomic, strong) UITextField *passTextFieldNew;
@property (nonatomic,strong) UIButton *saveChangesButton;

@end
