//
//  UserInfoModel.m
//  MDKP
//
//  Created by Андрей on 10.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "UserInfoModel.h"
#import "UIKit/UIKit.h"
#import "APIService.h"

@interface UserInfoModel ()

@property (nonatomic, strong) NSDictionary *userInfo;
@property (nonatomic, strong) NSMutableArray *adsInfo;
@property (nonatomic, strong) NSDictionary *adInfo;
@property (nonatomic, strong) NSArray *genderValue;

@end

@implementation UserInfoModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _userInfo = [NSDictionary dictionary];
        _adsInfo = [NSMutableArray array];
        _adInfo = [NSDictionary dictionary];
        _genderValue = [NSArray arrayWithObjects:@"Не указано",@"Мужской",@"Женский", nil];
    }
    return self;
}

- (void)getAdsInfo:(NSDictionary *)adsInfo {
    for (NSDictionary * ad in adsInfo) {
        [self.adsInfo addObject:ad];
    }
}

- (NSInteger)getAdsCount {
    return self.adsInfo.count;
}

- (NSString *)getAdsNameByIndexPath:(NSIndexPath *)indexPath {
    return [[self.adsInfo objectAtIndex:[indexPath item]] valueForKey:@"name"];
}

- (NSInteger)getAdIdByIndexPath:(NSIndexPath *)indexPath {
    return [[[self.adsInfo objectAtIndex:[indexPath item]] valueForKey:@"adsId"] integerValue];
}

- (NSString *)getAdsImagePathByIndexPath:(NSIndexPath *)indexPath {
    return [NSString stringWithFormat:@"https://easyhomeapp.000webhostapp.com%@",[[self.adsInfo objectAtIndex:[indexPath item]] valueForKey:@"photo"]];
}

- (NSDictionary *)getAdInfoDictionary {
    return self.adInfo;
}

- (NSString *)getGenderTitleByNumber:(NSInteger)number {
    return [self.genderValue objectAtIndex:number];
}

- (void)loadAdInfo:(NSInteger)adId withCompletion:(void (^)())completion {
    [[APIService sharedService] getAdById:adId withComplition:^(BOOL success, NSData *data, NSError *error) {
        if (success) {
            _adInfo = [NSJSONSerialization JSONObjectWithData:data
                                                      options:0
                                                        error:&error];
            _adInfo = [_adInfo objectForKey:@"response"];
            if (completion) {
                completion();
            }
        }
    }];
}

@end
