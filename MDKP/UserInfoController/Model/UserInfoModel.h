//
//  UserInfoModel.h
//  MDKP
//
//  Created by Андрей on 10.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfoModel : NSObject

- (NSDictionary *)getAdInfoDictionary;

- (void)getAdsInfo:(NSDictionary *)adsInfo;
- (NSInteger)getAdsCount;
- (NSString *)getAdsNameByIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)getAdIdByIndexPath:(NSIndexPath *)indexPath;
- (NSString *)getAdsImagePathByIndexPath:(NSIndexPath *)indexPath;

- (NSString *)getGenderTitleByNumber:(NSInteger)number;

- (void)loadAdInfo:(NSInteger)adId withCompletion:(void (^)())completion;

@end
