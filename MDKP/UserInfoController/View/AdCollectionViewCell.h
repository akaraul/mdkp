//
//  AdCollectionViewCell.h
//  MDKP
//
//  Created by Андрей on 22.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UILabel *name;

@end
