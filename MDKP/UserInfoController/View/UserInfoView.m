//
//  UserInfoView.m
//  MDKP
//
//  Created by Андрей on 02.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "UserInfoView.h"

@implementation UserInfoView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    //UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    self.backgroundColor = [UIColor whiteColor];
    
    _flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [_flowLayout  setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    _flowLayout.minimumLineSpacing = 15;
    
    _ads = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_flowLayout];
    _ads.backgroundColor = [UIColor whiteColor];
    [_ads setShowsHorizontalScrollIndicator:NO];
    [self addSubview:_ads];
    
    _userAvatar = [[UIImageView alloc] init];
    _userAvatar.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_userAvatar];
    
    _fullName = [[UILabel alloc] init];
    [_fullName setFont:[UIFont boldSystemFontOfSize:30]];
    [self addSubview:_fullName];
    
    _cityName = [[UILabel alloc] init];
    [self addSubview:_cityName];
    
    _aboutUser = [[UILabel alloc] init];
    _aboutUser.numberOfLines = 0;
    [self addSubview:_aboutUser];
    
//    _review = [[UIButton alloc] init];
//    _review.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    [_review setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [_review setTitle:@"Всего 47 отзывов" forState:UIControlStateNormal];
//    [self addSubview:_review];
    
    _moreInfo = [[UIView alloc] init];
    
    _moreInfoLabel = [[UILabel alloc] init];
    _moreInfoLabel.text = @"Дополнительная информация";
    [_moreInfoLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [_moreInfo addSubview:_moreInfoLabel];
    
    _genderLabel = [[UILabel alloc] init];
    _genderLabel.text = @"Пол: ";
    [_moreInfo addSubview:_genderLabel];
    
    _birthdayLabel = [[UILabel alloc] init];
    _birthdayLabel.text = @"Дата рождения: ";
    [_moreInfo addSubview:_birthdayLabel];
    
    _educationLabel = [[UILabel alloc] init];
    _educationLabel.text = @"Образование: ";
    [_moreInfo addSubview:_educationLabel];
    [self addSubview:_moreInfo];
    
    _countHomeLabel = [[UILabel alloc] init];
    [_countHomeLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [self addSubview:_countHomeLabel];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _indicatorView.hidesWhenStopped = YES;
    _indicatorView.color = [UIColor blueColor];
    _indicatorView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    [self addSubview:_indicatorView];
    
    [self addTopLayerToTheView:self.moreInfo];
    //[self addTopLayerToTheView:self.review];
    [self addTopLayerToTheView:self.countHomeLabel];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGFloat heightLabel= 40.0f;
    
    self.indicatorView.center = self.center;
    
    self.userAvatar.frame = CGRectMake(0, 0, width, height/2.4f);
    
    self.fullName.frame = CGRectMake(offset*2,
                                      self.userAvatar.bounds.size.height + self.userAvatar.frame.origin.y + offset,
                                     width - offset*4,
                                      heightLabel);
    
    self.cityName.frame = CGRectMake(offset*2,
                                        self.fullName.bounds.size.height + self.fullName.frame.origin.y + offset,
                                        self.fullName.bounds.size.width,
                                        20.0f);
    
    self.aboutUser.frame = CGRectMake(offset*2,
                                      self.cityName.bounds.size.height + self.cityName.frame.origin.y + offset,
                                      self.fullName.bounds.size.width,
                                      [self getLabelHeight:self.aboutUser]);
    
    self.moreInfo.frame = CGRectMake(offset*2,
                                     self.aboutUser.bounds.size.height + self.aboutUser.frame.origin.y + offset,
                                     width - offset*4,
                                     130.0f);
    
    self.moreInfoLabel.frame = CGRectMake(0, 0, self.moreInfo.bounds.size.width, heightLabel);
    
    self.genderLabel.frame = CGRectMake(0, self.moreInfoLabel.bounds.size.height + self.moreInfoLabel.frame.origin.y, self.moreInfo.bounds.size.width, 30);
    
    self.birthdayLabel.frame = CGRectMake(0, self.genderLabel.bounds.size.height + self.genderLabel.frame.origin.y, self.moreInfo.bounds.size.width, 30);
    
    self.educationLabel.frame = CGRectMake(0, self.birthdayLabel.bounds.size.height + self.birthdayLabel.frame.origin.y, self.moreInfo.bounds.size.width, 30);
    
//    self.review.frame = CGRectMake(offset*2,
//                                   self.moreInfo.bounds.size.height + self.moreInfo.frame.origin.y + offset,
//                                   self.moreInfo.bounds.size.width,
//                                   50.0f);
    
    self.countHomeLabel.frame = CGRectMake(offset*2,
                                           self.moreInfo.bounds.size.height + self.moreInfo.frame.origin.y+ offset,
                                           self.moreInfo.bounds.size.width,
                                           heightLabel);
    
    self.flowLayout.itemSize = CGSizeMake(width*0.7, height/3);
    self.ads.frame = CGRectMake(offset,
                                self.countHomeLabel.bounds.size.height + self.countHomeLabel.frame.origin.y + offset,
                                width-offset*2,
                                height/3);

    
    self.contentInset = UIEdgeInsetsMake(self.topOffset,0, 20.0f + self.ads.bounds.size.height ,0);
    self.contentSize = CGSizeMake(self.bounds.size.width, self.ads.frame.origin.y);
}

- (void)addTopLayerToTheView:(UIView *)view {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(0.0, 0.0, screenRect.size.width - 40.0f, 1.0);
    topBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                  alpha:1.0f].CGColor;
    [view.layer addSublayer:topBorder];
}

- (CGFloat)getLabelHeight:(UILabel*)label {
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    return size.height;
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end
