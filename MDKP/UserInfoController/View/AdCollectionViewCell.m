//
//  AdCollectionViewCell.m
//  MDKP
//
//  Created by Андрей on 22.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "AdCollectionViewCell.h"

@implementation AdCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        _imageView = [[UIImageView alloc] init];
        [self addSubview:_imageView];
        
        _name = [[UILabel alloc] init];
        [_name setFont:[UIFont boldSystemFontOfSize:18]];
        [self addSubview:_name];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    
    self.imageView.frame = CGRectMake(0, 0, width,  height-20);
    self.name.frame = CGRectMake(0, height-20, width, 20);
}

@end
