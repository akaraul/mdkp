//
//  UserInfoView.h
//  MDKP
//
//  Created by Андрей on 02.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface UserInfoView : UIScrollView
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;
@property(nonatomic,strong) UICollectionView *ads;
@property(nonatomic,strong) UICollectionViewFlowLayout *flowLayout;

@property(nonatomic,strong) UIImageView *userAvatar;

@property (nonatomic, strong) UILabel *fullName;
@property (nonatomic, strong) UILabel *cityName;
@property (nonatomic, strong) UILabel *aboutUser;

@property (nonatomic, strong) UIButton *review;

@property (nonatomic, strong) UIView *moreInfo;
@property (nonatomic, strong) UILabel *moreInfoLabel;
@property (nonatomic, strong) UILabel *genderLabel;
@property (nonatomic, strong) UILabel *birthdayLabel;
@property (nonatomic, strong) UILabel *educationLabel;

@property (nonatomic, strong) UILabel *countHomeLabel;

@property (nonatomic, assign) CGFloat topOffset;

@end
