//
//  UserInfoViewController.h
//  MDKP
//
//  Created by Андрей on 02.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInfoViewController : UIViewController

@property (nonatomic, strong) NSDictionary *userInfo;

@end
