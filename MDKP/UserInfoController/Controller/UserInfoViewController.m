//
//  UserInfoViewController.m
//  MDKP
//
//  Created by Андрей on 02.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "UserInfoViewController.h"
#import "UserInfoView.h"
#import "AdCollectionViewCell.h"
#import "DetailViewController.h"
#import "UserInfoModel.h"
#import "APIService.h"
#import <UIImageView+UIActivityIndicatorForSDWebImage.h>

NSString* const AdViewControllerCellRuseIdentifier = @"AdViewControllerCellRuseIdentifier";

@interface UserInfoViewController () <UICollectionViewDataSource,UICollectionViewDelegate>

@property (nonatomic,strong) UserInfoView *userInfoView;
@property (nonatomic, strong) UserInfoModel *model;

@end

@implementation UserInfoViewController

- (void)loadView {
    self.userInfoView = [[UserInfoView alloc] init];
    self.view = self.userInfoView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[UserInfoModel alloc] init];

    [self setValuesForFields];
    
    self.userInfoView.ads.dataSource = self;
    self.userInfoView.ads.delegate = self;
    [self.userInfoView.ads registerClass:[AdCollectionViewCell class] forCellWithReuseIdentifier:AdViewControllerCellRuseIdentifier];

}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.userInfoView.topOffset = self.topLayoutGuide.length;
}

- (void)setValuesForFields {
    NSString *avatarPath = [NSString stringWithFormat:@"https://easyhomeapp.000webhostapp.com%@",[self.userInfo valueForKeyPath:@"userInfo.avatar_path"]];
    [SDImageCache sharedImageCache].shouldCacheImagesInMemory = NO;
    [self.userInfoView.userAvatar setImageWithURL:[NSURL URLWithString:avatarPath] placeholderImage:nil options:SDWebImageCacheMemoryOnly usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", [self.userInfo valueForKeyPath:@"userInfo.name"], [self.userInfo valueForKeyPath:@"userInfo.surname"]];
    self.navigationItem.title = fullName;
    self.userInfoView.fullName.text = fullName;
    NSString *about = [self.userInfo valueForKeyPath:@"userInfo.about"];
    if (!about) about = @"Пользовтель не указал информацию о себе";
    self.userInfoView.aboutUser.text = about;
    self.userInfoView.cityName.text = [self.userInfo valueForKeyPath:@"userInfo.city"];
    self.userInfoView.genderLabel.text = [self.userInfoView.genderLabel.text stringByAppendingString:[self.model getGenderTitleByNumber:[[self.userInfo valueForKeyPath:@"userInfo.gender"] integerValue]]];
    self.userInfoView.birthdayLabel.text = [self.userInfoView.birthdayLabel.text stringByAppendingString:[self.userInfo valueForKeyPath:@"userInfo.birthday"]];
    self.userInfoView.educationLabel.text = [self.userInfoView.educationLabel.text stringByAppendingString:[self.userInfo valueForKeyPath:@"userInfo.education"]];
    [self.model getAdsInfo:[self.userInfo valueForKey:@"ads"]];
    self.userInfoView.countHomeLabel.text= [NSString stringWithFormat:@"Количество объявлений: %ld",(long)[self.model getAdsCount]];
}

#pragma UICollectionViewDelegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.model getAdsCount];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    AdCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:AdViewControllerCellRuseIdentifier forIndexPath:indexPath];
    //cell.imageView.image = [UIImage imageNamed:@"userAvatarIcon"];
    [cell.imageView setImageWithURL:[NSURL URLWithString:[self.model getAdsImagePathByIndexPath:indexPath]] placeholderImage:nil options:SDWebImageCacheMemoryOnly usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    cell.name.text = [self.model getAdsNameByIndexPath:indexPath];
    cell.tag = [self.model getAdIdByIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    [self.userInfoView.indicatorView startAnimating];
    [self.model loadAdInfo:[collectionView cellForItemAtIndexPath:indexPath].tag withCompletion:^{
        DetailViewController *detailController = [[DetailViewController alloc] init];
        detailController.adInfo = [self.model getAdInfoDictionary];
        [self.navigationController pushViewController:detailController animated:YES];
        [self.userInfoView.indicatorView stopAnimating];
    }];

}



@end
