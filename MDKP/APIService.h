//
//  APIService.h
//  MDKP
//
//  Created by Андрей on 05.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^APIServiceCompletion) (BOOL success, NSError *error);
typedef void (^APIServiceDataCompletion) (BOOL success, NSData *data, NSError *error);

@interface APIService : NSObject

+ (instancetype)sharedService;

- (void)searchResult:(NSDictionary *)options withComplition:(APIServiceDataCompletion)completion;

- (void)getUserInfoById:(NSInteger)userId withComplition:(APIServiceDataCompletion)completion;

- (void)getOwnerContact:(NSInteger)userId withComplition:(APIServiceDataCompletion)completion;

- (void)getAdById:(NSInteger)adId withComplition:(APIServiceDataCompletion)completion;

- (void)getAllComfortsWithComplition:(APIServiceDataCompletion)completion;

- (void)getAllReviews:(NSInteger)adId withComplition:(APIServiceDataCompletion)completion;

- (void)addAdvert:(NSDictionary *)adInfo images:(NSArray *)images withComplition:(APIServiceCompletion)completion;

- (void)addAdvertReview:(NSDictionary *)options withComplition:(APIServiceCompletion)completion;

- (void)changeAdActive:(NSInteger)adId active:(NSString *)active withComplition:(APIServiceDataCompletion)completion;

- (void)deleteAd:(NSInteger)adId withComplition:(APIServiceDataCompletion)completion;

- (void)updateUserInfo:(NSDictionary *)userInfo withComplition:(APIServiceCompletion)completion;

- (void)changeUserPassword:(NSDictionary *)userInfo withComplition:(APIServiceCompletion)completion;

- (void)updateUserAvatar:(NSData *)image byId:(NSInteger)userId withComplition:(APIServiceCompletion)completion;

- (void)regUser:(NSDictionary *)userInfo withComplition:(APIServiceCompletion)completion;

- (void)loginUser:(NSDictionary *)userInfo withComplition:(APIServiceCompletion)completion;

@end
