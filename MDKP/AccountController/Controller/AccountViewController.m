//
//  AccountViewController.m
//  MDKP
//
//  Created by Андрей on 03.03.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "AccountViewController.h"
#import "AccountView.h"
#import "AppDelegate.h"
#import "ChangePasswordViewController.h"
#import "AccountModel.h"

@interface AccountViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource, UITextViewDelegate>

@property (nonatomic,strong) AccountView *accountView;
@property (nonatomic,strong) AccountModel *model;
@property (nonatomic,strong) NSMutableDictionary *userInfo;
@property (nonatomic, assign) BOOL isAvatarChanged;

@end

@implementation AccountViewController

- (void)loadView {
    self.accountView = [[AccountView alloc] init];
    self.view = self.accountView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isAvatarChanged = NO;
    self.model = [[AccountModel alloc] init];
    self.userInfo = [[self.model getUserInfo] mutableCopy];
    [self setValuesForFields];
    
    self.accountView.nameTextField.delegate = self;
    self.accountView.surNameTextField.delegate = self;
    self.accountView.genderTextField.delegate =self;
    self.accountView.birthdayTextField.delegate =self;
    self.accountView.cityTextField.delegate =self;
    self.accountView.educationTextField.delegate =self;
    self.accountView.phoneTextField.delegate =self;
    self.accountView.about.delegate = self;
    
    self.accountView.genderPicker.delegate = self;
    self.accountView.genderPicker.dataSource = self;
    
    self.navigationItem.title = @"Аккаунт";
    self.accountView.userInfo.delegate = self;
    self.accountView.userInfo.dataSource = self;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menuIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(openMenu:)];
    
    [self.accountView.saveChangesButton addTarget:self action:@selector(saveChanges:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.accountView.changeAvatarButton addTarget:self action:@selector(changeAvatar:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.accountView.datePicker addTarget:self action:@selector(dateValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [gestureRecognizer setCancelsTouchesInView:NO];
    [self.accountView addGestureRecognizer:gestureRecognizer];
}

- (void)dateValueChanged:(UIDatePicker *)sender {
    self.accountView.birthdayTextField.text = [self.accountView.dateFormatter stringFromDate:sender.date];
}

- (void)setValuesForFields {
    NSLog(@"%@",[self.userInfo valueForKey:@"avatarImage"]);
        NSLog(@"%@",[self.userInfo valueForKey:@"phone"]);
    UIImage *avatar = [UIImage imageWithData:[self.userInfo valueForKey:@"avatarImage"]];
    if (avatar) self.accountView.userAvatar.image = avatar;
    
    self.accountView.nameTextField.text = [self.userInfo valueForKey:@"name"];
    self.accountView.emailTextField.text = [self.userInfo valueForKey:@"login"];
    if ([self.userInfo valueForKey:@"surname"] != [NSNull null]) self.accountView.surNameTextField.text = [self.userInfo valueForKey:@"surname"];
    if ([self.userInfo valueForKey:@"birthday"] != [NSNull null]) {
        [self.accountView.dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *birthday = [self.accountView.dateFormatter dateFromString:[self.userInfo valueForKey:@"birthday"]];
        [self.accountView.dateFormatter setDateFormat:@"dd.MM.yyyy"];
        self.accountView.birthdayTextField.text = [self.accountView.dateFormatter stringFromDate:birthday];
    }
    if ([self.userInfo valueForKey:@"about"] != [NSNull null]) {
        [self.accountView.placeholderTextView removeFromSuperview];
        self.accountView.about.text = [self.userInfo valueForKey:@"about"];
    }
    if ([self.userInfo valueForKey:@"gender"] != [NSNull null]) self.accountView.genderTextField.text =  [self.model getGenderTitleByNumber:[[self.userInfo valueForKey:@"gender"] integerValue]];
    if ([self.userInfo valueForKey:@"phone"] != [NSNull null]) self.accountView.phoneTextField.text = [self.userInfo valueForKey:@"phone"];
    if ([self.userInfo valueForKey:@"education"] != [NSNull null]) self.accountView.educationTextField.text = [self.userInfo valueForKey:@"education"];
    if ([self.userInfo valueForKey:@"city"] != [NSNull null]) self.accountView.cityTextField.text = [self.userInfo valueForKey:@"city"];
}

- (void)keyboardWasShown:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGRect keyboardRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    UIEdgeInsets contentInset = self.accountView.userInfo.contentInset;
    contentInset.bottom = keyboardRect.size.height + 7.0;
    self.accountView.userInfo.contentInset = contentInset;
}

- (void)keyboardWillBeHidden:(NSNotification*)notification {
    self.accountView.userInfo.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (void)changeAvatar:(UIBarButtonItem *)sender {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* takePhoto = [UIAlertAction
                                actionWithTitle:@"Сделать фото"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self photoFromCamera];
                                    
                                }];
    
    UIAlertAction* selectFromLibrary = [UIAlertAction
                                actionWithTitle:@"Выбрать из галереи"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self photoFromLibrary];
                                            
                                }];
    
    UIAlertAction* cancel = [UIAlertAction
                                        actionWithTitle:@"Отмена"
                                        style:UIAlertActionStyleCancel
                                        handler:nil];
    
    [alert addAction:takePhoto];
    [alert addAction:selectFromLibrary];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)photoFromCamera {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)photoFromLibrary {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.accountView.userAvatar.image = chosenImage;
    self.isAvatarChanged = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)saveChanges:(UIBarButtonItem *)sender {
    [self.userInfo setValue:self.accountView.nameTextField.text forKey:@"name"];
    [self.userInfo setValue:self.accountView.surNameTextField.text forKey:@"surname"];
    [self.userInfo setValue:self.accountView.about.text forKey:@"about"];
    
    NSDate *birthday = [self.accountView.dateFormatter dateFromString:self.accountView.birthdayTextField.text];
    [self.accountView.dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [self.userInfo setValue:[self.accountView.dateFormatter stringFromDate:birthday] forKey:@"birthday"];
    [self.accountView.dateFormatter setDateFormat:@"dd.MM.yyyy"];
    
    [self.userInfo setValue:[self.model getNumberValueForGender:self.accountView.genderTextField.text] forKey:@"gender"];
    [self.userInfo setValue:self.accountView.phoneTextField.text forKey:@"phone"];
    [self.userInfo setValue:self.accountView.educationTextField.text forKey:@"education"];
    [self.userInfo setValue:self.accountView.cityTextField.text forKey:@"city"];
    if (self.isAvatarChanged) {
        [self.model changeAvatar:UIImageJPEGRepresentation(self.accountView.userAvatar.image, 1) byId:[[self.userInfo valueForKey:@"userId"] integerValue]];
    }
    [self.userInfo removeObjectForKey:@"avatarImage"];
    [self.model updateUserProfile:self.userInfo withCompletion:^{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Личные данные обновлены!"
                                     message:@"Обновление прошло успешно!"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:nil];
        
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];

    }];
}

- (void)openMenu:(UIBarButtonItem *)sender {
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.accountView.topOffset = self.topLayoutGuide.length;
}

#pragma UIPickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.model getGenderCount];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.model getGenderTitleByNumber:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.accountView.genderTextField.text = [self.model getGenderTitleByNumber:row];
}


#pragma UITableviewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 7;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 1;
        case 1:
            return 1;
        case 2:
            return 4;
        case 3:
            return 2;
        case 4:
            return 2;
        case 5:
            return 1;
        case 6:
            return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    cell.textLabel.textColor = [UIColor lightGrayColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch ([indexPath section]) {
        case 0:
            [cell.contentView addSubview:self.accountView.userAvatar];
            break;
        case 1:
            [cell.contentView addSubview:self.accountView.about];
            break;
        case 2:
            switch ([indexPath row]) {
                case 0:
                    cell.textLabel.text = @"Имя";
                    [cell.contentView addSubview:self.accountView.nameTextField];
                    break;
                case 1:
                    cell.textLabel.text = @"Фамилия";
                    [cell.contentView addSubview:self.accountView.surNameTextField];
                    break;
                case 2:
                    cell.textLabel.text = @"Дата рождения";
                    [cell.contentView addSubview:self.accountView.birthdayTextField];
                    break;
                case 3:
                    cell.textLabel.text = @"Пол";
                    [cell.contentView addSubview:self.accountView.genderTextField];
                    break;
            }
            break;
        case 3:
            switch ([indexPath row]) {
                case 0:
                    cell.textLabel.text = @"E-mail";
                    [cell.contentView addSubview:self.accountView.emailTextField];
                    break;
                case 1:
                    cell.textLabel.text = @"Телефон";
                    [cell.contentView addSubview:self.accountView.phoneTextField];
                    break;
            }
            break;
        case 4:
            switch ([indexPath row]) {
                    case 0:
                    cell.textLabel.text = @"Образование";
                    [cell.contentView addSubview:self.accountView.educationTextField];
                    break;
                    case 1:
                    cell.textLabel.text = @"Родной город";
                    [cell.contentView addSubview:self.accountView.cityTextField];
                    break;
            }
            break;

        case 5:
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.text = @"Изменить пароль";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            break;
        case 6:
            cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
            [cell.contentView addSubview:self.accountView.saveChangesButton];
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath section] == 5) {
        ChangePasswordViewController *changePass = [[ChangePasswordViewController alloc] init];
        changePass.userId = [[self.userInfo valueForKey:@"userId"] integerValue];
        [self.navigationController pushViewController:changePass animated:YES];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 1:
            return @"О себе";
        case 2:
            return @"Личные данные";
        case 3:
            return @"Контакты";
        case 4:
            return @"Дополнительная информация";
    }
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 6) return 10;
    if (section == 0) return 0.001;
    return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath section] == 0) return self.accountView.userAvatar.frame.size.height;
    if ([indexPath section] == 1) return self.accountView.about.frame.size.height;
    if ([indexPath section] == 6) return 50;
    return 45;
}

#pragma UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType==UIReturnKeyNext) {
        UIView *next = [self.accountView viewWithTag:textField.tag+1];
        [next becomeFirstResponder];
    } else if (textField.returnKeyType==UIReturnKeyDone) {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma UITextViewDelegate
- (void) textViewDidChange:(UITextView *)theTextView {
    if(![self.accountView.about hasText]) {
        [self.accountView.about addSubview:self.accountView.placeholderTextView];
    } else if ([[self.accountView.about subviews] containsObject:self.accountView.placeholderTextView]) {
        [self.accountView.placeholderTextView removeFromSuperview];
    }
}

- (void)textViewDidEndEditing:(UITextView *)theTextView {
    if (![self.accountView.about hasText]) {
        [self.accountView.about addSubview:self.accountView.placeholderTextView];
    }
}


@end
