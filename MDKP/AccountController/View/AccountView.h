//
//  AccountView.h
//  MDKP
//
//  Created by Андрей on 03.03.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountView : UIView

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property(nonatomic,strong) UIButton *saveChangesButton;
@property(nonatomic,strong) UIButton *changeAvatarButton;
@property(nonatomic,strong) UIImageView *userAvatar;

@property(nonatomic,strong) UITableView *userInfo;

@property (nonatomic, strong) UITextField *nameTextField;
@property (nonatomic, strong) UITextField *surNameTextField;
@property (nonatomic, strong) UITextField *birthdayTextField;
@property (nonatomic, strong) UITextField *genderTextField;
@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *phoneTextField;
@property (nonatomic, strong) UITextField *educationTextField;
@property (nonatomic, strong) UITextField *cityTextField;
@property (nonatomic, strong) UITextView *about;
@property (nonatomic, strong) UILabel *placeholderTextView;
@property(nonatomic,strong) UIDatePicker *datePicker;
@property(nonatomic,strong) NSDateFormatter *dateFormatter;
@property (nonatomic,strong) UIPickerView *genderPicker;

@property (nonatomic, assign) CGFloat topOffset;

@end
