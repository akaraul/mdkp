//
//  AccountView.m
//  MDKP
//
//  Created by Андрей on 03.03.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "AccountView.h"

@implementation AccountView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    self.backgroundColor = [UIColor whiteColor];
    
    _saveChangesButton = [[UIButton alloc] init];
    _saveChangesButton.backgroundColor = colorGreen;
    _saveChangesButton.layer.cornerRadius = 10.0f;
    [_saveChangesButton setTitle:@"Сохранить изменения" forState:UIControlStateNormal];
    
    _changeAvatarButton = [[UIButton alloc] init];
    _changeAvatarButton.backgroundColor = [UIColor whiteColor];
    _changeAvatarButton.layer.cornerRadius = 30.0f;
    _changeAvatarButton.layer.borderWidth = 0.3f;
    [_changeAvatarButton setAlpha:0.72];
    [_changeAvatarButton setImage:[UIImage imageNamed:@"changeAvatarIcon"] forState:UIControlStateNormal];

    _userAvatar = [[UIImageView alloc] init];
    _userAvatar.image = [UIImage imageNamed:@"userAvatarIcon"];
    _userAvatar.contentMode = UIViewContentModeScaleAspectFit;
    _userAvatar.userInteractionEnabled = YES;
    [_userAvatar addSubview:_changeAvatarButton];
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Не указано" attributes:@{ NSForegroundColorAttributeName : [UIColor grayColor] }];
    
    _placeholderTextView = [[UILabel alloc] init];
    _placeholderTextView.text = @"Расскажите немного о себе";
    [_placeholderTextView setBackgroundColor:[UIColor clearColor]];
    [_placeholderTextView setTextColor:[UIColor lightGrayColor]];
    
    _about = [[UITextView alloc] init];
    _about.tag = 0;
    [_about setFont:[UIFont systemFontOfSize:16]];
    _about.returnKeyType = UIReturnKeyNext;
    [_about addSubview:_placeholderTextView];
    
    _nameTextField = [[UITextField alloc] init];
    _nameTextField.tag =1;
    _nameTextField.returnKeyType = UIReturnKeyNext;
    _nameTextField.attributedPlaceholder = str;
    
    _surNameTextField = [[UITextField alloc] init];
    _surNameTextField.tag = 2;
    _surNameTextField.returnKeyType = UIReturnKeyNext;
    _surNameTextField.attributedPlaceholder = str;
    
    _datePicker = [[UIDatePicker alloc] init];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"dd.MM.yyyy"];
    
    _birthdayTextField = [[UITextField alloc] init];
    _birthdayTextField.inputView = _datePicker;
    _birthdayTextField.tag = 3;
    _birthdayTextField.returnKeyType = UIReturnKeyNext;
    _birthdayTextField.attributedPlaceholder = str;

    
    _genderPicker = [[UIPickerView alloc] init];
    
    _genderTextField = [[UITextField alloc] init];
    _genderTextField.tag = 4;
    _genderTextField.inputView = _genderPicker;
    _genderTextField.returnKeyType = UIReturnKeyNext;
    _genderTextField.attributedPlaceholder = str;
    
    _emailTextField = [[UITextField alloc] init];
    _emailTextField.enabled = NO;
    
    _phoneTextField = [[UITextField alloc] init];
    _phoneTextField.keyboardType = UIKeyboardTypePhonePad;
    _phoneTextField.tag = 5;
    _phoneTextField.returnKeyType = UIReturnKeyNext;
    _phoneTextField.attributedPlaceholder = str;
    
    _educationTextField = [[UITextField alloc] init];
    _educationTextField.tag = 6;
    _educationTextField.returnKeyType = UIReturnKeyNext;
    _educationTextField.attributedPlaceholder = str;
    
    _cityTextField = [[UITextField alloc] init];
    _cityTextField.tag = 8;
    _cityTextField.returnKeyType = UIReturnKeyDone;
    _cityTextField.attributedPlaceholder = str;
    
    _userInfo = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    [self addSubview:_userInfo];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _indicatorView.hidesWhenStopped = YES;
    _indicatorView.color = [UIColor blueColor];
    _indicatorView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    [self addSubview:_indicatorView];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGRect sizeTextField = CGRectMake(160, 0, width - 160, 45);
    
    self.indicatorView.center = self.center;
    
    self.userInfo.frame = CGRectMake(0,
                                     0,
                                     width,
                                     height);
    
    self.userAvatar.frame = CGRectMake(0, 0, width, height/2.4f);
    self.changeAvatarButton.frame = CGRectMake(self.userAvatar.bounds.size.width - 65.0f,
                                               self.userAvatar.bounds.size.height - 65.0f,
                                               60.0f,
                                               60.0f);
    
    self.nameTextField.frame = sizeTextField;
    self.surNameTextField.frame = sizeTextField;
    self.birthdayTextField.frame = sizeTextField;
    self.genderTextField.frame = sizeTextField;
    self.emailTextField.frame = sizeTextField;
    self.phoneTextField.frame = sizeTextField;
    self.saveChangesButton.frame = CGRectMake(offset, 0, width - offset*2, 50);
    self.educationTextField.frame = sizeTextField;
    self.cityTextField.frame = sizeTextField;
    self.about.frame = CGRectMake(offset, 0, width, 80);
    self.placeholderTextView.frame = CGRectMake(0, 0, width, 30);
    
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end
