//
//  AccountModel.m
//  MDKP
//
//  Created by Андрей on 10.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "AccountModel.h"
#import "APIService.h"
#import "CoreData/CoreData.h"
#import "AppDelegate.h"
#import "USERINFO+CoreDataClass.h"

@interface AccountModel ()

@property (nonatomic, strong) NSPersistentContainer *persistentContainer;
@property (nonatomic, strong) NSArray *genderValue;

@end

@implementation AccountModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
        _genderValue = [NSArray arrayWithObjects:@"Не указано",@"Мужской",@"Женский", nil];
    }
    return self;
}

-(NSInteger)getGenderCount {
    return self.genderValue.count;
}

-(NSString *)getGenderTitleByNumber:(NSInteger)number {
    return [self.genderValue objectAtIndex:number];
}

-(NSString *)getNumberValueForGender:(NSString *)stringValue {
    return [NSString stringWithFormat:@"%lu",(unsigned long)[self.genderValue indexOfObject:stringValue]];
}

- (NSDictionary *) getUserInfo {
    NSDictionary *res = [NSDictionary dictionary];
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"USERINFO"];
    
    NSArray *results = [context executeFetchRequest:request error:nil];
    NSArray *keys = [[[[results firstObject] entity] attributesByName] allKeys];
    res = [[results firstObject] dictionaryWithValuesForKeys:keys];
    return res;
}

- (void)updateUserProfile:(NSDictionary *)userInfo withCompletion:(void (^)())completion {
    [[APIService sharedService] updateUserInfo:userInfo withComplition:^(BOOL success, NSError *error) {
        if (success) {
            NSManagedObjectContext *context = self.persistentContainer.viewContext;
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"USERINFO"];
            USERINFO *update=[[context executeFetchRequest:fetchRequest error:nil] lastObject];
            for (id key in userInfo) {
                [update setValue:[userInfo valueForKey:key] forKey:key];
            }
            [context save:nil];
            if (completion) completion();
        }
    }];
}

- (void)changeAvatar:(NSData *)image byId:(NSInteger)userId {
    [[APIService sharedService] updateUserAvatar:image byId:userId withComplition:^(BOOL success, NSError *error) {
        if (success){
            NSManagedObjectContext *context = self.persistentContainer.viewContext;
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"USERINFO"];
            USERINFO *update=[[context executeFetchRequest:fetchRequest error:nil] lastObject];
            [update setValue:image forKey:@"avatarImage"];
            [context save:nil];
        }
    }];
}

@end
