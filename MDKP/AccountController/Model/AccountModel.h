//
//  AccountModel.h
//  MDKP
//
//  Created by Андрей on 10.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccountModel : NSObject

-(NSDictionary *)getUserInfo;

-(void)updateUserProfile:(NSDictionary *)userInfo withCompletion:(void (^)())completion;

-(void)changeAvatar:(NSData *)image byId:(NSInteger)userId;

-(NSInteger)getGenderCount;
-(NSString *)getGenderTitleByNumber:(NSInteger)number;
-(NSString *)getNumberValueForGender:(NSString *)stringValue;

@end
