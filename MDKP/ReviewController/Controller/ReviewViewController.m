//
//  ReviewViewController.m
//  MDKP
//
//  Created by Андрей on 30.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "ReviewViewController.h"
#import "DetailTableViewCell.h"
#import "ReviewView.h"
//#import "AddReviewViewController.h"

@interface ReviewViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) ReviewView *reviewView;

@end

@implementation ReviewViewController

- (void)loadView {
    self.reviewView = [[ReviewView alloc] init];
    self.view = self.reviewView;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.reviewView.reviewsTable.delegate = self;
    self.reviewView.reviewsTable.dataSource = self;
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reviewIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(addReview:)];
    
}

//- (void)addReview:(UIButton *)sender {
//    AddReviewViewController *addReviewController = [[AddReviewViewController alloc] init];
//    addReviewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    [self presentViewController:addReviewController animated:YES completion:nil];
//}

#pragma UITableviewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.allReviews.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Review"];
    if (!cell) {
        cell = [[DetailTableViewCell alloc] init];
    }
    [cell.cellContentView setItem:[self.allReviews objectAtIndex:[indexPath row]]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 170.0f;
}



@end
