//
//  ReviewView.m
//  MDKP
//
//  Created by Андрей on 30.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "ReviewView.h"

@implementation ReviewView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];

    _reviewsTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _reviewsTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_reviewsTable];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.reviewsTable.frame = self.bounds;
    
}

@end
