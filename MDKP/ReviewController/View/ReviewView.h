//
//  ReviewView.h
//  MDKP
//
//  Created by Андрей on 30.04.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewView : UIView

@property (nonatomic, strong) UITableView *reviewsTable;

@end
