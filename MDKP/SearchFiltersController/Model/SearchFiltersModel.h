//
//  SearchFiltersModel.h
//  MDKP
//
//  Created by Андрей on 26.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchFiltersModel : NSObject

- (NSString *)getTitleType:(NSArray *)values;

- (NSString *)getTitleComforts:(NSArray *)ids;

@end
