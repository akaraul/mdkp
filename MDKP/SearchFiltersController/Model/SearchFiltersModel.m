//
//  SearchFiltersModel.m
//  MDKP
//
//  Created by Андрей on 26.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "SearchFiltersModel.h"
#import "AppDelegate.h"

@interface SearchFiltersModel ()

@property (nonatomic, strong) NSPersistentContainer *persistentContainer;

@end

@implementation SearchFiltersModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}

- (NSString *)getTitleType:(NSArray *)values {
    NSString *result = @"";
    for (NSNumber *typeId in values) {
        int num = [typeId intValue];
        if (num == 1) result = [result stringByAppendingString:@"Жилье целиком, "];
        if (num == 2) result = [result stringByAppendingString:@"Отдельная комната, "];
        if (num == 3) result = [result stringByAppendingString:@"Общая комната, "];
    }
    if ([result  isEqual: @""]) result = @"Не выбрано";
    else result = [result substringToIndex:[result length] - 2];
    return result;
}

- (NSString *)getTitleComforts:(NSArray *)ids {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"COMFORT"];
    
    NSString *result = @"";
    for (id comfortId in ids) {
        [request setPredicate:[NSPredicate predicateWithFormat:@"comfortId == %@", comfortId]];
        NSArray *res = [context executeFetchRequest:request error:nil];
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%@, ",[[res firstObject] valueForKey:@"name"]]];
    }
    if ([result  isEqual: @""]) result = @"Не выбрано";
    else result = [result substringToIndex:[result length] - 2];
    return result;
}

@end
