//
//  SearchFiltersViewController.m
//  MDKP
//
//  Created by Андрей on 25.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "SearchFiltersViewController.h"
#import "SearchFiltersView.h"
#import "SearchFiltersModel.h"
#import "TypeHomeViewController.h"
#import "ComfortsViewController.h"

@interface SearchFiltersViewController () <UITableViewDelegate, UITableViewDataSource, ComfortsViewControllerDelegate, TypeHomeControllerDelegate>

@property (nonatomic,strong) SearchFiltersView *searchFiltersView;
@property (nonatomic,strong) SearchFiltersModel *model;
@property (nonatomic, strong) NSMutableDictionary *valueFilters;

@end

@implementation SearchFiltersViewController

- (void)loadView{
    self.searchFiltersView = [[SearchFiltersView alloc] init];
    self.view = self.searchFiltersView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[SearchFiltersModel alloc] init];
    self.valueFilters = [NSMutableDictionary dictionary];
    
    self.navigationItem.title = @"Фильтры";
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    
    self.searchFiltersView.filterTable.delegate = self;
    self.searchFiltersView.filterTable.dataSource = self;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"closeIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(closeView:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Сбросить" style:UIBarButtonItemStyleDone target:self action:@selector(resetFilters:)];
    
    [self.searchFiltersView.save addTarget:self action:@selector(saveParametrs:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.searchFiltersView.priceSlider addTarget:self action:@selector(rangeSliderValueDidChange:)forControlEvents:UIControlEventValueChanged];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.barStyle = UIBarStyleDefault;
    CGFloat toolbarHeight = 80;
    CGRect rootViewBounds = self.parentViewController.view.bounds;
    CGFloat rootViewHeight = CGRectGetHeight(rootViewBounds);
    CGFloat rootViewWidth = CGRectGetWidth(rootViewBounds);
    CGRect rectArea = CGRectMake(0, rootViewHeight - toolbarHeight, rootViewWidth, toolbarHeight);
    [toolbar setFrame:rectArea];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.searchFiltersView.save];
    [toolbar setItems:[NSArray arrayWithObjects:barButtonItem,nil]];
    [self.navigationController.view addSubview:toolbar];
}

- (void)resetFilters:(UIButton *)sender {
    [self.valueFilters removeAllObjects];
    self.searchFiltersView.stepRooms.value = 0;
    self.searchFiltersView.stepBeds.value = 0;
    self.searchFiltersView.stepBathrooms.value = 0;
    [self.searchFiltersView.priceSlider setLeftValue:0 rightValue:10000];
    [self rangeSliderValueDidChange:self.searchFiltersView.priceSlider];
    [self.searchFiltersView.filterTable reloadData];
}

- (void)saveParametrs:(UIButton *)sender {
    if (self.searchFiltersView.stepRooms.value != 0)
    [self.valueFilters setValue:[@(self.searchFiltersView.stepRooms.value) stringValue] forKey:@"rooms"];
    if (self.searchFiltersView.stepBeds.value != 0)
    [self.valueFilters setValue:[@(self.searchFiltersView.stepBeds.value) stringValue] forKey:@"beds"];
    if (self.searchFiltersView.stepBathrooms.value != 0)
    [self.valueFilters setValue:[@(self.searchFiltersView.stepBathrooms.value) stringValue] forKey:@"bathrooms"];
    [self.delegate searchParametrsDidSelected:self.valueFilters];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)closeView:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)rangeSliderValueDidChange:(MARKRangeSlider *)slider {
    self.searchFiltersView.priceLabel.text = [NSString stringWithFormat:@"₽%0.0f - ₽%0.0f", slider.leftValue, slider.rightValue];
    NSInteger min = slider.leftValue;
    NSInteger max = slider.rightValue;
    [self.valueFilters setValue:[@(min) stringValue] forKey:@"minPrice"];
    [self.valueFilters setValue:[@(max) stringValue] forKey:@"maxPrice"];
}

#pragma Select delegates

- (void)typeHomeDidSelected:(NSArray *)typeHome {
    [self.valueFilters setValue:typeHome forKey:@"type"];
    [self.searchFiltersView.filterTable reloadData];
}

- (void)comfortsDidSelected:(NSArray *)comforts {
    [self.valueFilters setValue:comforts forKey:@"comforts"];
    [self.searchFiltersView.filterTable reloadData];
}

#pragma UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Filter"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Filter"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch ([indexPath row]) {
        case 0:
            [cell.contentView addSubview:self.searchFiltersView.priceView];
            break;
        case 1:
            cell.detailTextLabel.text = [self.model getTitleType:[self.valueFilters valueForKey:@"type"]];
            cell.textLabel.text = @"Выберите тип";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            break;
        case 2:
            cell.detailTextLabel.text = [self.model getTitleComforts:[self.valueFilters objectForKey:@"comforts"]];
            cell.textLabel.text = @"Выберите удобства";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            break;
        case 3:
            cell.textLabel.text = @"Комнаты";
            [cell.contentView addSubview:self.searchFiltersView.stepRooms];
            break;
        case 4:
            cell.textLabel.text = @"Кровати";
            [cell.contentView addSubview:self.searchFiltersView.stepBeds];
            break;
        case 5:
            cell.textLabel.text = @"Ванные комнаты";
            [cell.contentView addSubview:self.searchFiltersView.stepBathrooms];
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([indexPath row] == 1) {
        TypeHomeViewController *typeController = [[TypeHomeViewController alloc] init];
        typeController.delegate = self;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:typeController];
        [self presentViewController:nav animated:YES completion:nil];
    }
    if ([indexPath row] == 2) {
        ComfortsViewController *comfortsController = [[ComfortsViewController alloc] init];
        comfortsController.delegate = self;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:comfortsController];
        [self presentViewController:nav animated:YES completion:nil];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([indexPath row]) {
        case 0:
            return self.searchFiltersView.priceView.frame.size.height;
    }
    return 60;
}


@end
