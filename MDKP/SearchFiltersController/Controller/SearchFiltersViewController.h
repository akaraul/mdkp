//
//  SearchFiltersViewController.h
//  MDKP
//
//  Created by Андрей on 25.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  SearchFiltersViewControllerDelegate;

@interface SearchFiltersViewController : UIViewController

@property (nonatomic, weak) id<SearchFiltersViewControllerDelegate> delegate;

@end

@protocol SearchFiltersViewControllerDelegate <NSObject>

- (void)searchParametrsDidSelected:(NSDictionary *)parametrs;

@end
