//
//  SearchFiltersView.h
//  MDKP
//
//  Created by Андрей on 25.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MARKRangeSlider.h"
#import <SAStepperControl.h>

@interface SearchFiltersView : UIView

@property (nonatomic, strong) UIButton *save;

@property (nonatomic, strong) UITableView *filterTable;

@property (nonatomic, strong) UIView *priceView;
@property (nonatomic, strong) UILabel *priceHeader;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) MARKRangeSlider *priceSlider;
@property (nonatomic, strong) SAStepperControl *stepRooms;
@property (nonatomic, strong) SAStepperControl *stepBeds;
@property (nonatomic, strong) SAStepperControl *stepBathrooms;

@end
