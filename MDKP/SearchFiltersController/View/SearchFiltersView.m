//
//  SearchFiltersView.m
//  MDKP
//
//  Created by Андрей on 25.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "SearchFiltersView.h"

@implementation SearchFiltersView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    UIImage *incrementImageFromFile = [UIImage imageNamed:@"plusIcon"];
    UIImage *incrementImage = [incrementImageFromFile imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *decrementImageFromFile = [UIImage imageNamed:@"minusIcon"];
    UIImage *decrementImage = [decrementImageFromFile imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIColor *colorGreen = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    self.backgroundColor = [UIColor whiteColor];
    
    _save = [[UIButton alloc] init];
    [_save setTitle:@"Применить" forState:UIControlStateNormal];
    _save.backgroundColor = colorGreen;
    _save.layer.cornerRadius = 10.0f;
    
    _priceView = [[UIView alloc] init];
    _priceView.backgroundColor = [UIColor whiteColor];
    
    _priceSlider = [[MARKRangeSlider alloc] initWithFrame:CGRectZero];
    [_priceSlider setMinValue:0 maxValue:10000];
    [_priceSlider setLeftValue:0 rightValue:10000];
    _priceSlider.minimumDistance = 1000;
    [_priceView addSubview:_priceSlider];
    
    _priceLabel = [[UILabel alloc] init];
    _priceLabel.text = @"₽0 - ₽10000";
    [_priceView addSubview:_priceLabel];
    
    _priceHeader = [[UILabel alloc] init];
    _priceHeader.text = @"Цена";
    [_priceHeader setFont:[UIFont boldSystemFontOfSize:20]];
    [_priceView addSubview:_priceHeader];
    
    _stepBeds = [[SAStepperControl alloc] init];
    _stepBeds.value = 0;
    _stepBeds.tintColor = [UIColor clearColor];
    _stepBeds.transform = CGAffineTransformMakeScale(1.3, 1.3);
    [_stepBeds setDecrementImage:decrementImage forState:UIControlStateNormal];
    [_stepBeds setIncrementImage:incrementImage forState:UIControlStateNormal];;
    
    _stepRooms = [[SAStepperControl alloc] init];
    _stepRooms.value = 0;
    _stepRooms.tintColor = [UIColor clearColor];
    _stepRooms.transform = CGAffineTransformMakeScale(1.3, 1.3);
    [_stepRooms setDecrementImage:decrementImage forState:UIControlStateNormal];
    [_stepRooms setIncrementImage:incrementImage forState:UIControlStateNormal];;
    
    _stepBathrooms = [[SAStepperControl alloc] init];
    _stepBathrooms.value = 0;
    _stepBathrooms.tintColor = [UIColor clearColor];
    _stepBathrooms.transform = CGAffineTransformMakeScale(1.3, 1.3);
    [_stepBathrooms setDecrementImage:decrementImage forState:UIControlStateNormal];
    [_stepBathrooms setIncrementImage:incrementImage forState:UIControlStateNormal];;

    
    _filterTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _filterTable.scrollEnabled = NO;
    [self addSubview:_filterTable];

    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat offset = 10;
    CGFloat width = self.bounds.size.width;
    CGFloat widthButton = width*0.8;
    
    self.save.frame = CGRectMake(width/2 - widthButton/2,
                                 15.0f,
                                 widthButton,
                                 50.0f);
    
    self.filterTable.frame = self.bounds;
    
    self.priceView.frame = CGRectMake(offset, 0, width - offset*2, 100);
    self.priceHeader.frame = CGRectMake(offset, 5, width - offset*2, 20);
    self.priceLabel.frame = CGRectMake(offset, 30, width - offset*2, 20);
    self.priceSlider.frame = CGRectMake(offset, 55, width - offset*3, 45);
    
    self.stepRooms.frame = CGRectMake(width - 140, 10, 80, 60);
    self.stepBathrooms.frame = self.stepRooms.frame;
    self.stepBeds.frame = self.stepRooms.frame;
    
}

@end
