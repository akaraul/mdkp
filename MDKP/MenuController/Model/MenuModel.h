//
//  MenuModel.h
//  MDKP
//
//  Created by Андрей on 11.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MenuModel : NSObject

- (void)deleteUserInfo;

- (NSString *)menuTitleForIndexPath:(NSIndexPath *)indexPath;
- (UIImage *)imageForIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)countElements;

- (NSString *)setUserName;
- (UIImage *)setUserAvatar;

@end
