//
//  MenuModel.m
//  MDKP
//
//  Created by Андрей on 11.05.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "MenuModel.h"
#import "AppDelegate.h"
#import "CoreData/CoreData.h"

@interface MenuModel ()

@property (nonatomic, strong) NSPersistentContainer *persistentContainer;
@property (strong,nonatomic) NSArray *menuItems;
@property (nonatomic, strong) UIImage *avatar;

@end

@implementation MenuModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
        _menuItems = [NSArray arrayWithObjects:@"Аккаунт",@"Поиск",@"Добавить объявление",@"Мои объявления",@"Выйти",nil];
    }
    return self;
}

- (NSInteger)countElements {
    return self.menuItems.count;
}

- (NSString *)menuTitleForIndexPath:(NSIndexPath *)indexPath {
    return [self.menuItems objectAtIndex:indexPath.row];
}

- (UIImage *)imageForIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            return [UIImage imageNamed:@"accountIcon"];
        case 1:
            return [UIImage imageNamed:@"searchIcon"];
        case 2:
            return [UIImage imageNamed:@"addIcon"];
        case 3:
            return [UIImage imageNamed:@"myAdsIcon"];
        case 4:
            return [UIImage imageNamed:@"exitIcon"];
    }
    return nil;
}

- (NSString *)setUserName {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"USERINFO"];
    request.propertiesToFetch = @[ @"name", @"surname" ];
    NSArray *res = [context executeFetchRequest:request error:nil];
    NSString *surname = [[res firstObject] valueForKey:@"surname"];
    if (!surname) surname = @"";
    self.avatar = [UIImage imageWithData:[[res firstObject] valueForKey:@"avatarImage"]];
    return [NSString stringWithFormat:@"%@ %@",[[res firstObject] valueForKey:@"name"],surname];
}

- (UIImage *)setUserAvatar {
    if (self.avatar) return self.avatar;
    return [UIImage imageNamed:@"noavatar"];
}

- (void) deleteUserInfo {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"USERINFO"];
    NSArray *res = [context executeFetchRequest:request error:nil];
    for (NSManagedObject *elem in res) {
        [context deleteObject:elem];
    }
    request = [NSFetchRequest fetchRequestWithEntityName:@"ADS"];
    res = [context executeFetchRequest:request error:nil];
    for (NSManagedObject *elem in res) {
        [context deleteObject:elem];
    }
    [context save:nil];
}

@end
