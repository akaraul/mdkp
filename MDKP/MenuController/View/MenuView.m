//
//  MenuView.m
//  MDKP
//
//  Created by Андрей on 06.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "MenuView.h"

@implementation MenuView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    
    _tableMenu = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableMenu.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableMenu.scrollEnabled = NO;
    [self addSubview:_tableMenu];
    
    _nameLabel = [[UILabel alloc] init];
    _nameLabel.text = @"Имя пользователя";
    [self addSubview:_nameLabel];
    
    _avatarView = [[UIImageView alloc] init];
    _avatarView.layer.cornerRadius = 30.0f;
    _avatarView.layer.masksToBounds = YES;
    [self addSubview:_avatarView];
    
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    
    self.avatarView.frame = CGRectMake(10, 20, 64, 64);
    self.nameLabel.frame = CGRectMake(84, 32, width - 40, 40);
    self.tableMenu.frame = CGRectMake(0, height/6, width, height*0.4f);
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end
