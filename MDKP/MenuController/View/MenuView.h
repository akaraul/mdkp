//
//  MenuView.h
//  MDKP
//
//  Created by Андрей on 06.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuView : UIView

@property (nonatomic,strong) UIImageView *avatarView;
@property (nonatomic,strong) UILabel *nameLabel;

@property (nonatomic,strong) UITableView *tableMenu;

@property (nonatomic, assign) CGFloat topOffset;

@end
