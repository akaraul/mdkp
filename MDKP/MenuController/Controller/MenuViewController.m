//
//  MenuViewController.m
//  MDKP
//
//  Created by Андрей on 06.01.17.
//  Copyright © 2017 Home. All rights reserved.
//

#import "MenuViewController.h"
#import "AccountViewController.h"
#import "MainViewController.h"
#import "LoginViewController.h"
#import "AddAdvertViewController.h"
#import "MyAdsViewController.h"
#import "AppDelegate.h"
#import "MenuView.h"
#import "MenuModel.h"

@interface MenuViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong,nonatomic) MenuView *menuView;
@property (strong,nonatomic) MenuModel *model;

@end

@implementation MenuViewController

- (void)loadView {
    self.menuView = [[MenuView alloc] init];
    self.view = self.menuView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[MenuModel alloc] init];
    
    self.menuView.tableMenu.dataSource = self;
    self.menuView.tableMenu.delegate = self;

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.menuView.nameLabel.text = [self.model setUserName];
    self.menuView.avatarView.image = [self.model setUserAvatar];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.menuView.topOffset = self.topLayoutGuide.length;
}


#pragma UITableviewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.model countElements];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MYCell"];
    }
    cell.imageView.image = [self.model imageForIndexPath:indexPath];
    cell.textLabel.text = [self.model menuTitleForIndexPath:indexPath];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIViewController *center;
    switch (indexPath.row) {
        case 0:
            center = [[AccountViewController alloc] init];
            break;
        case 1:
            center = [[MainViewController alloc] init];
            break;
        case 2:
            center = [[AddAdvertViewController alloc] init];
            break;
        case 3:
            center = [[MyAdsViewController alloc] init];
            break;
        case 4:
            center = [[LoginViewController alloc] init];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isLoggedIn"];
            [self.model deleteUserInfo];
            [self presentViewController:center animated:YES completion:nil];
            return;
    }
    UINavigationController *centerController = [[UINavigationController alloc] initWithRootViewController:center];
    centerController.navigationBar.barTintColor = [UIColor colorWithRed:78.0/255.0 green:214.0/255.0 blue:193.0/255.0 alpha:1];
    [centerController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    centerController.navigationBar.tintColor = [UIColor whiteColor];
    [app.drawerController setCenterViewController:centerController withCloseAnimation:YES completion:nil];
}

@end
